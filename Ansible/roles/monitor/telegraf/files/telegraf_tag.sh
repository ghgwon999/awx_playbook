#!/bin/bash
ENV=/etc/default/telegraf

Location1="AWS"
Location2="IDC"
DMI_Patten="amazon Amazon Intel Xen invalid"
Types="VM BM EC2 RDS"
Products="PROD DEV STAG"
Zones="DMZ INT DB DEV"
APPs="Apache nginx Tomcat JBOSS ORACLE Mysql Redis MongoDB BATCH SVC Infra"
PJs="OMEGA ETP Stella PLX AMS NDC LCC TAVI PARAGON TNA BENEPIA ETL COMMON DBMS"

### set ${Location} ###
function Location() {
for ID in $DMI_Patten
do
        DMI=$(dmesg | grep "DMI" | grep $ID | wc -l)
        if [[ $DMI = 1 ]]
        then
                if [[ $ID = Amazon ]]
                then
                        echo Location=$Location1 >> $ENV
                        echo Type=EC2 >> $ENV
                else
                        echo Location=$Location2 >> $ENV
                        if [[ $ID = Xen ]]
                        then
                                echo Type=VM >> $ENV
                        else
                                echo Type=BM >> $ENV
                        fi

                fi
        fi
done
}

### set ${Product} ###
function Product() {
Status=0
for ID in $Products
do
        if [[ $ID = $Product ]]
        then
                Status=1
        fi
done
if [[ $Status = 1 ]]
then
        echo Product=$Product >> $ENV
else
        echo Product=Other >> $ENV
fi
}

### set ${APP} ###
function APP() {
Status=0
for ID in $APPs
do
        if [[ $ID = $APP ]]
        then
                Status=1
        fi
done
if [[ $Status = 1 ]]
then
	echo APP=$APP >> $ENV
else
	echo APP=Other >> $ENV
fi
}

### set ${Project} ###
function Project() {
Status=0
for ID in $PJs
do
        if [[ $ID = $Project ]]
        then
		Status=1
	fi
done
if [[ $Status = 1 ]]
then
	echo Project=$Project >> $ENV
else
        echo Project=Other >> $ENV
fi
}


USAGE="usage:`basename $0` [-S PRIVIA | TORUVIS | TOURVIS_BIZ] [-P PROD | DEV | STAG] [-A Apache | Tomcat | more] [-J Stella | NDC | LCC | DBMS | more] [-help ]"

while getopts "S:P:A:J:h:" opt;
do
        case $opt in
                S) Service=$OPTARG # Select Service 
		### set ${Service} ###
		if [[ $Service = PRIVIA ]]
		then
			IP=$(ip a | grep 172.16 | awk '{print $2}' | cut -f 1 -d '/' | head -1)
		        echo Service=PRIVIA >> $ENV
			echo IP=$IP >> $ENV

		        Zone=$(echo $IP | awk -F . '{print $1"."$2"."$3"."}')
		        if [[ $Zone = 172.16.1. ]]
		        then
		                echo Zone=DMZ >> $ENV
		        else
                		if [[ $Zone = 172.16.0. ]]
		                then
                		        echo Zone=INT >> $ENV
		                else
                		        if [[ $Zone = 172.16.2. ]]
		                        then
                	        	        echo Zone=DEV >> $ENV
	                        	else
        		                        echo Zone=Other >> $ENV
		                        fi
                		fi
        		fi
		else
			if [[ $Service = TOURVIS ]]
			then
				IP=$(ip a | grep 172.16 | awk '{print $2}' | cut -f 1 -d '/' | head -1)
		        	echo Service=TOURVIS >> $ENV
				echo IP=$IP >> $ENV

			        Zone=$(echo $IP | awk -F . '{print $1"."$2"."$3"."}')
			        if [[ $Zone = 172.16.100. ]]
		        	then
                			echo Zone=DMZ >> $ENV
			        else
        	        		if [[ $Zone = 172.16.101. ]]
			                then
                			        echo Zone=INT >> $ENV
		                	else
                        			if [[ $Zone = 172.16.102. ]]
		                	        then
        			                        echo Zone=DB >> $ENV
                	        		else
							if [[ $Zone = 172.16.103. ]]
	        			                then
        	                			        echo Zone=DEV >> $ENV
				                	else
						        	if [[ $Zone = 172.16.200. ]]
							        then
					                		echo Zone=DMZ >> $ENV
							        else
							                if [[ $Zone = 172.16.201. ]]
								        then
					        			       echo Zone=INT >> $ENV
						        	        else
						                        	if [[ $Zone = 172.16.202. ]]
						                	        then
				                        		        	echo Zone=DB >> $ENV
					                	        	else
        		                        					if [[ $Zone = 172.16.203. ]]
						        	                        then
        	                	        					        echo Zone=DEV >> $ENV
					                        		        else
                        	        				        		echo Zone=Other >> $ENV
											fi
										fi
									fi
								fi
							fi
						fi
		                	fi
        			fi
			else
				if [[ $Service = TOURVIS_BIZ ]]
				then
					IP=$(ip a | grep 10. | awk '{print $2}' | cut -f 1 -d '/' | head -1)
				        echo Service=TOURVIS_BIZ >> $ENV
					echo IP=$IP >> $ENV
	
				        Zone=$(echo $IP | awk -F . '{print $1"."$2"."$3"."}')
				        if [[ $Zone = 10.20.1. ]]
				        then
			        	        echo Zone=DMZ >> $ENV
				        else
				                if [[ $Zone = 10.20.2. ]]
				                then
				                        echo Zone=INT >> $ENV
			        	        else
			                	        if [[ $Zone = 10.120.1. ]]
			                        	then
			                                	echo Zone=DB>> $ENV
				                        else
								if [[ $Zone = 10.20.3. ]]
					                        then
			        		                        echo Zone=DEV>> $ENV
				        	       	        else
			                        		        echo Zone=Other >> $ENV
				                        	fi
				                        fi
				                fi
        				fi
				else
					echo "Service Input ERROR"
				fi
			fi
		fi
			Location;;
                P) Product=$OPTARG # Select Product
			Product ;;
                A) APP=$OPTARG # Select APPlication
			APP ;;
                J) Project=$OPTARG # select Project
			Project ;;
		h) echo $USAGE ;;
        esac
done
