#!/bin/bash
PLAYBOOK_PATH="/etc/ansible/playbook/valnerability_yaml"
INVENTORY_PATH="/etc/ansible/inventory"
CATEGORY_1=(
  "U-01.yml"
  "U-02.yml"
  "U-03.yml"
  "U-04.yml"
  "U-44.yml"
  "U-45.yml"
  "U-46.yml"
  "U-47.yml"
  "U-48.yml"
  "U-49.yml"
  "U-50.yml"
  "U-51.yml"
  "U-52.yml"
  "U-53.yml"
)
CATEGORY_2=(
  "U-05.yml"
  "U-06.yml"
  "U-07.yml"
  "U-08.yml"
  "U-09.yml"
  "U-10.yml"
  "U-11.yml"
  "U-12.yml"
  "U-13.yml"
  "U-14.yml"
  "U-15.yml"
  "U-16.yml"
  "U-17.yml"
  "U-55.yml"
  "U-57.yml"
  "U-58.yml"
  "U-59.yml"
)
CATEGORY_3=(
  "U-19.yml"
  "U-20.yml"
  "U-21.yml"
  "U-22.yml"
  "U-23.yml"
  "U-26.yml"
  "U-28.yml"
  "U-29.yml"
  "U-30.yml"
  "U-31.yml"
  "U-32.yml"
  "U-33.yml"
  "U-34.yml"
  "U-35_U-41.yml"
  "U-36_U-40.yml"
  "U-60.yml"
  "U-61_U-64.yml"
  "U-65.yml"
  "U-66.yml"
  "U-67.yml"
  "U-68.yml"
  "U-69.yml"
  "U-70.yml"
  "U-71.yml"
  "U-72.yml"
)

CHECK_MODE=""
if [[ "$3" == "--check" ]]; then
  CHECK_MODE="--check"
fi

run_playbooks() {
  local inventory="$1"
  local category_name="$2"
  shift 2
  local playbooks=("$@")

  echo "Running $category_name Playbooks with inventory $inventory..."

  for playbook in "${playbooks[@]}"; do
    if [ -f "$PLAYBOOK_PATH/$playbook" ]; then
      echo "Executing: $PLAYBOOK_PATH/$playbook with inventory $inventory $CHECK_MODE"
      ansible-playbook -i "$INVENTORY_PATH/$inventory" "$PLAYBOOK_PATH/$playbook" $CHECK_MODE
      if [ $? -ne 0 ]; then
        echo "Error: Playbook $playbook failed. Exiting."
        exit 1
      fi
    else
      echo "Warning: Playbook $playbook not found. Skipping."
    fi
  done

  echo "$category_name Playbooks completed."
}

if [[ -z "$1" || -z "$2" ]]; then
  echo "Usage: $0 {1|2|3|all} {privia_host_dev|privia_host_prod|privia_host_stg} [--check]"
  exit 1
fi

case "$1" in
  "1")
    run_playbooks "$2" "Category 1" "${CATEGORY_1[@]}"
    ;;
  "2")
    run_playbooks "$2" "Category 2" "${CATEGORY_2[@]}"
    ;;
  "3")
    run_playbooks "$2" "Category 3" "${CATEGORY_3[@]}"
    ;;
  "all")
    run_playbooks "$2" "Category 1" "${CATEGORY_1[@]}"
    run_playbooks "$2" "Category 2" "${CATEGORY_2[@]}"
    run_playbooks "$2" "Category 3" "${CATEGORY_3[@]}"
    ;;
  *)
    echo "Usage: $0 {1|2|3|all} {privia_host_dev|privia_host_prod|privia_host_stg} [--check]"
    exit 1
    ;;
esac

