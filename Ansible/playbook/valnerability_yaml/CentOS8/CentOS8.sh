#!/bin/bash

PLAYBOOK_DIR="/etc/ansible/playbook/valnerability_yaml/CentOS8"
INVENTORY_FILE="privia_host"

for playbook in $PLAYBOOK_DIR/U-*.yml; do
  if [ -f "$playbook" ]; then
    echo "Running playbook: $playbook"
    ansible-playbook -i "$INVENTORY_FILE" "$playbook"
  fi
done

