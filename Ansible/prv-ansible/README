You have an empty repository
To get started you will need to run these commands in your terminal.

# A. Configure Git for the first time
```bash
  git config --global user.name "[Bitbucket User Name]"
  git config --global user.email "[Jira E-mail]"
```

----------------------------------------------------------------------------------------------
# B. Working with your repository
### B.1. I just want to clone this repository
If you want to simply clone this empty repository then run this command in your terminal.
```bash
  git clone https://[Bitbucket User Name]@bitbucket.org/tidesquare/prv-ansible.git
```

### B.2. My code is ready to be pushed
If you already have code ready to be pushed to this repository then run this in your terminal.
```bash
  DatE=`date '+%Y%m%d-%H%M'`
  cd /etc/ansible
  git init
  git add --all
  git commit -m "${DatE}"
  git remote add origin https://[Bitbucket User Name]@bitbucket.org/tidesquare/prv-ansible.git
  git push -u origin master
  git config credential.helper store
```

### B.3. My code is already tracked by Git
If your code is already tracked by Git then set this repository as your "origin" to push to.
```bash
  cd /etc/ansible
  git remote set-url origin https://[Bitbucket User Name]@bitbucket.org/tidesquare/prv-ansible.git
  git push -u origin --all
  git push origin --tags
```
