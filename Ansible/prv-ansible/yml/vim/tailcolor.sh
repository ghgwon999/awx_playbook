# color
#black	\x1b[30m
#red	\x1b[31m
#green	\x1b[32m
#yellow	\x1b[33m
#blue	\x1b[34m
#purple	\x1b[35m
#cyan	\x1b[36m
#white	\x1b[37m

# command
sed \
    -e "s/\(SUCCESSED\)/\x1b[32m\1\x1b[0m/" \
    -e "s/\(FAILED\)/\x1b[31m\1\x1b[0m/" \
    -e "s/\( 200 \)/\x1b[34m\1\x1b[0m/" \
