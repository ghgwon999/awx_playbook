#!/bin/bash
find /root/itam -name "itam_*_*.txt" -exec perl -pi -e 's/\[u\x27//g' {} \;
find /root/itam -name "itam_*_*.txt" -exec perl -pi -e 's/\x27\,\ u\x27/|/g' {} \;
find /root/itam -name "itam_*_*.txt" -exec perl -pi -e 's/\x27\]//g' {} \;
find /root/itam -name "itam_*_*.txt" -exec perl -pi -e 's/\[\]/None/g' {} \;

find /root/itam -name "itam_*_*.txt" -exec mv -t /root/itam_result/ {} \;

python /etc/ansible/yml/itam/merge_into_csv.py
