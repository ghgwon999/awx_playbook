#!/bin/bash
unset LANG
cd ~
OFHN=`hostname`
OFDAY=`date +"%y%m%d%H%M"`
OFRND=`expr ${RANDOM} % 1000`

if [ -f ~/itam_${OFHN}_*_*.txt ];then
   rm -f ~/itam_${OFHN}_*_*.txt
fi


# system info check
tab=`echo -e "\t"`
if [ -f ~/itamiplist ];then
if [ `ifconfig | grep "encap:Ethernet" | wc -l` -ge 1 ];then
   TT=`ifconfig | grep "encap:Ethernet" | awk '{print $1}'`
   for a in $TT
   do
      TA=`ifconfig $a | grep 'inet ' | awk '{print $2}' | awk -F: '{print $2}'`
      if [ -n "${TA}" ];then
#         cat ~/itamiplist | grep ${TA} | awk '{print $3, $4, $5, $6, $7}' >> ~/sysinfo
         cat ~/itamiplist | awk "/${TA}\t/" | grep -v -E "return" | awk '{print $3, $4, $5, $6, $7}' >> ~/sysinfo
         echo "${TA}" >> ~/sysinfo3
      fi
   done
elif [ `ifconfig | grep flags | wc -l` -ge 1 ];then
   TT=`ifconfig | grep flags | grep -v ^lo |  awk -F: '{print $1}'`
   for a in $TT
   do
      TA=`ifconfig $a | grep 'inet ' | awk '{print $2}'`
      if [ -n "${TA}" ];then
#         cat ~/itamiplist | grep ${TA} | awk '{print $3, $4, $5, $6, $7}' >> ~/sysinfo
         cat ~/itamiplist | awk "/${TA}\t/" | grep -v -E "return" | awk '{print $3, $4, $5, $6, $7}' >> ~/sysinfo
         echo "${TA}" >> ~/sysinfo3
      fi
   done
fi
fi

if [ -f ~/sysinfo ];then
cat ~/sysinfo | sort | uniq > ~/sysinfo2
fi

if [ -f ~/sysinfo2 ];then
   find ~/ -type f -name "sysinfo2" -exec perl -pi -e 's/$/\ \|/' {} \;
   find ~/ -type f -name "sysinfo2" -exec perl -pi -e 's/\:/\//g' {} \;

   if [ `cat ~/sysinfo2 | wc -l` -eq 1 ];then
      TB1=`cat ~/sysinfo2 | awk '{print $2}'`
      TB2=`cat ~/sysinfo2 | awk '{print $3}'`
      TB3=`cat ~/sysinfo2 | awk '{print $1}'`
      TB4=`cat ~/sysinfo2 | awk '{print $4, $5}'`

      echo "Service Name :" ${TB4} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
      echo "HOSTNAME :" ${OFHN} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
      echo "Remark :" ${TB3} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
      echo "Machine Type :" ${TB2} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
      echo "Location :" ${TB1} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
   elif [ `cat ~/sysinfo2 | grep xen | wc -l` -eq 1 ];then
      cat ~/sysinfo2 | grep xen >> ~/sysinfo4
      TB1=`cat ~/sysinfo4 | awk '{print $2}'`
      TB2=`cat ~/sysinfo4 | awk '{print $3}'`
      TB3=`cat ~/sysinfo4 | awk '{print $1}'`
      TB4=`cat ~/sysinfo4 | awk '{print $4, $5}'`

      echo "Service Name :" ${TB4} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
      echo "HOSTNAME :" ${OFHN} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
      echo "Remark :" ${TB3} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
      echo "Machine Type :" ${TB2} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
      echo "Location :" ${TB1} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
   else
      TD=`cat ~/sysinfo2 | wc -l`
      TE=`cat ~/sysinfo3`
      echo "Service Name : RECHECK" >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
      echo "HOSTNAME :" ${OFHN} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
      echo "Remark : RECHECK" ${TD}_${TE} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
      echo "Machine Type : RECHECK" >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
      echo "Location : RECHECK" >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
   fi
fi

echo "~/sysinfo ~/sysinfo2 ~/sysinfo3 ~/sysinfo4" >> ~/itamrmlist


# interface check
if [ `ifconfig | grep "encap:Ethernet" | wc -l` -ge 1 ];then
   TT=`ifconfig | grep "encap:Ethernet" | awk '{print $1}'`
   for a in $TT
   do
      TA=`ifconfig $a | grep 'inet ' | awk '{print $2}' | awk -F: '{print $2}'`
      if [ -n "${TA}" ];then
         echo "${a}-${TA}" >> ~/ifinfo
      fi
   done
elif [ `ifconfig | grep flags | wc -l` -ge 1 ];then
   TT=`ifconfig | grep flags | grep -v ^lo |  awk -F: '{print $1}'`
   for a in $TT
   do
      TA=`ifconfig $a | grep 'inet ' | awk '{print $2}'`
      if [ -n "${TA}" ];then
         echo "${a}-${TA}" >> ~/ifinfo
      fi
   done
fi

if [ -f ~/ifinfo ];then
   find ~/ -type f -name "ifinfo" -exec perl -pi -e 's/$/\ \|/' {} \;
   find ~/ -type f -name "ifinfo" -exec perl -pi -e 's/\:/\//g' {} \;
fi
TB=`cat ~/ifinfo`
echo "Interface :" ${TB} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt

echo "~/ifinfo" >> ~/itamrmlist


# Account
cat /etc/passwd | grep sh$ | awk -F: '{print $1}' | grep root >> AC
cat /etc/passwd | grep sh$ | awk -F: '{print $1}' | grep -v root | sort >> AC1
cat AC1 >> AC
find ~/ -type f -name "AC" -exec perl -pi -e 's/$/\ \|/' {} \;
AC=`cat ~/AC`
echo "Account :" $AC  >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt

echo "~/AC" >> ~/itamrmlist


# Apache ServerName Check
if [ `ps -ef | grep -v -E "grep|root" | grep httpd | wc -l` -ge 2 ];then
    find /etc/httpd/conf.d/* -type f -name "*.conf" | xargs grep -v ^'#' | grep -E "ServerName|ServerAlias" | awk '{print $3}'| sort | uniq >> asncinfo
fi
if [ -f ~/asncinfo ];then
   find ~/ -type f -name "asncinfo" -exec perl -pi -e 's/$//g' {} \;
   find ~/ -type f -name "asncinfo" -exec perl -pi -e 's/$/\ \|/' {} \;
   ASN=`cat ~/asncinfo`
   echo "Apache ServerName :" ${ASN}  >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
else
   echo "Apache ServerName : N/A" >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
fi

echo "~/asncinfo" >> ~/itamrmlist


# JkMount Point Check (Worker.properties"
if [ `ps -ef | grep -v -E "grep|root" | grep httpd | wc -l` -ge 2 ];then
   JP1=`find /etc/httpd/conf.d/* -type f -name "*.conf" | xargs grep JkMount | grep -v '#' | awk '{print$4}' | sort| uniq`
   JP2_1=`find /etc/httpd/ -type f -name "*.conf" | grep -v ori | xargs grep ServerRoot | grep -v '#' | awk -F\" '{print $2}'`
   JP2_2=`find /etc/httpd/ -type f -name "*.conf" | grep -v ori | xargs grep JkWorkersFile | grep -v '#' | awk -F\: '{print $2}' | awk '{print $2}'`
   JP2=${JP2_1}/${JP2_2}

   if [ ! -z ${JP2_2} ];then
      for JM in $JP1
      do
         if [ `grep worker.${JM}.balance_workers ${JP2} | grep -v '#' | wc -l` -eq 1 ];then
            grep worker.${JM}.balance_workers ${JP2} | grep -v '#' | awk -F\= '{print $2}' >> ~/JP
            if [ -f ~/JP ];then
              find ~/ -type f -name "JP" -exec perl -pi -e 's/,/\ /g' {} \;
            fi
         elif [ `grep worker.${JM}.balance_workers ${JP2} | grep -v '#' | wc -l` -eq 0 ];then
            if [ `grep worker.${JM}.type\=ajp ${JP2} | grep -v '#' | wc -l` -eq 1 ];then
               echo "${JM}" >> ~/JP
            fi
         fi
      done

      for i in `cat ~/JP`
      do
         echo ${i} >> ~/JR
      done

      for a in `cat ~/JR | sort | uniq`
      do
         JM1_1=`cat ${JP2} | grep worker.${a}.host | grep -v '#' | awk -F\= '{print $2}'`
         JM1_2=`cat ${JP2} | grep worker.${a}.port | grep -v '#' | awk -F\= '{print $2}'`
         if [ -z ${JM1_1} ];then
            echo "${a} localhost ${JM1_2} |" >> JP1
         else
            echo "${a} ${JM1_1} ${JM1_2} |" >> JP1
         fi
      done
   fi
   if [ -f ~/JP1 ];then
      JE=`cat ~/JP1`
      echo "JkMount Config :" ${JE} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
   else
      echo "JkMount Config : N/A" ${JE} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
   fi
else
   echo "JkMount Point : N/A" ${JE} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
fi

echo "~/JP ~/JR ~/JP1" >> ~/itamrmlist

# WAS Check
   if [ -d /sdb1/servers ];then
      cd /sdb1/servers
      find . -maxdepth 1 -type d | awk -F/ '{print $2}' >> ~/jbinfo
      cd ~
   elif [ -d /app/servers ];then
      cd /app/servers
      find . -maxdepth 1 -type d | awk -F/ '{print $2}' >> ~/jbinfo
      cd ~
   fi
   if [ -f ~/jbinfo ];then
      JB=`cat ~/jbinfo | grep -v tmp`
   fi
   if [ `ps -ef | grep -v grep | grep -E "jdk|java" | grep ^jboss | wc -l` -ge 1 ];then
      for a in $JB
      do
         if [ `ps -ef | grep -v grep | grep java | grep -E "jdk|env.properties" | grep ${a} | wc -l` -ge 1 ];then
            echo "${a}" >> ~/jbinfo2
         fi
      done
      JB2=Jboss
   elif [ `ps -ef | grep -v grep | grep -E "jdk|java" | grep ^tomcat | wc -l` -ge 1 ];then
      for a in $JB
      do
         if [ `ps -ef | grep -v grep | grep java | grep -E "jdk|catalina|Bootstrap" | grep ${a} | wc -l` -ge 1 ];then
            echo "${a}" >> ~/jbinfo2
         fi
      done
      JB2=tomcat
   elif [ `ps -ef | grep -v grep | grep -E "jdk|java" | grep -v -E "^jboss|^tomcat" | wc -l` -ge 1 ];then
      JB2=unknow
      echo "RECHECK" >> ~/jbinfo2
      ps -ef | grep -v grep | grep -E "jdk|java" | grep -v -E "^jboss|^tomcat" >> ~/jbinfo2
   fi

if [ -f ~/jbinfo2 ];then
   find ~/ -type f -name "jbinfo2" -exec perl -pi -e 's/$/\ \|/' {} \;
   find ~/ -type f -name "jbinfo2" -exec perl -pi -e 's/:/-/g' {} \;
   JB1=`cat ~/jbinfo2 | sort`
   echo "WAS Instance Type :" ${JB2} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
   echo "WAS Instance Name :" ${JB1} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
else
   echo "WAS Instance Type : N/A" >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
   echo "WAS Instance Name : N/A" >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
fi

echo "~/jbinfo ~/jbinfo2" >> ~/itamrmlist


# Jennifer Instance Name Check
echo "Jennifer Name : aaaaaa" >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt

# CPU Check
cat /proc/cpuinfo | grep "model name" | awk -F:\  '{print $2}' | awk -F\ @\  '{print $1, $2}' | awk -F\)\  '{print $3}' | sort | uniq >> CC
find ~/ -type f -name "CC" -exec perl -pi -e 's/CPU\ //g' {} \;
find ~/ -type f -name "CC" -exec perl -pi -e 's/\ v/v/g' {} \;
CC1=`cat ~/CC`
echo "CPU Model :" ${CC1} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt

CC2=`grep ^processor /proc/cpuinfo | wc -l`
echo "Physical CPU per M/B :" ${CC2} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt

CC3=`grep 'cpu cores' /proc/cpuinfo | tail -1 | awk -F:\  '{print $2}'`
echo "Physical Core per CPU :" ${CC3} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt


# Memory Check
MC1=`cat /proc/meminfo | grep MemTotal | awk '{print $2}'`
MC2=`expr \( ${MC1} / 1024 \) / 1024`
MC3=${MC2}" GB"
if [ `echo ${MC3} | awk '{print $1}'` -eq 0 ];then
   MC4=`expr ${MC1} / 1024`
   MC5=${MC4}" MB"
   echo "Memory Size :" ${MC5} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
else
   echo "Memory Size :" ${MC3} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
fi

echo "~/CC" >> ~/itamrmlist


# Disk Check
fdisk -l | egrep 'Disk.*bytes' | awk '{ sub(/,/,""); sum +=$5;print $2" "$3" "$4" "$5" "$6 } END { print "total:" sum/1024/1024/1024 " GB" }' >> ~/DC

find ~/ -type f -name "DC" -exec perl -pi -e 's/$/\ \|/' {} \;

DC1=`cat ~/DC | tail -1 | awk -F: '{print $2}'`
DC2=`find ~/ -type f -name "DC" -exec perl -pi -e 's/:/->/g' {} \; ; cat ~/DC | grep -v 'total->'`

echo "DISK Total Size :" ${DC1} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt
echo "DISK Detail Size :" ${DC2} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt

echo "~/DC" >> ~/itamrmlist


# Check DATE
echo "Check DATE :" ${OFDAY} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt



find ~/ -type f -name "itam_${OFHN}_${OFDAY}*" -exec perl -pi -e 's/,,//g' {} \;
find ~/ -type f -name "itam_${OFHN}_${OFDAY}*" -exec perl -pi -e 's/,//g' {} \;
find ~/ -type f -name "itam_${OFHN}_${OFDAY}*" -exec perl -pi -e 's/\ \|$//g' {} \;
//g' {} \;ype f -name "itam_${OFHN}_${OFDAY}*" -exec perl -pi -e 's/
/g' {} \;type f -name "itam_${OFHN}_${OFDAY}*" -exec perl -pi -e 's/$/


TF=`cat ~/sysinfo3`
echo "CHECK POINT : " ${TF} >> ~/itam_${OFHN}_${OFDAY}_${OFRND}.txt

ITAM_RM_LIST=`cat ~/itamrmlist`

for irl in ${ITAM_RM_LIST}
do
   if [ -f ${irl} ];then
      rm -f ${irl}
   fi
done

#rm -f ~/JP ~/JR ~/JP1 ~/AC ~/AC1 ~/ifinfo ~/asncinfo ~/jbinfo ~/jbinfo2 ~/CC ~/DC ~/sysinfo ~/sysinfo2 ~/sysinfo3 ~/sysinfo4
