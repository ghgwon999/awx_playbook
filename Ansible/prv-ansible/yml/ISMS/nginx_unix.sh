#!/bin/sh

LANG=C
export LANG

CREATE_FILE_RESULT=`hostname`"_"`date +%Y%m%d`"_Nginx.log"


echo ""
echo "################# Nginx 진단 스크립트를 실행하겠습니다 ###################"
echo ""
echo ""

alias ls=ls
alias grep=/bin/grep

echo '[Nginx 서비스 확인]'
echo ''
ps -ef | grep -v grep | grep nginx | grep -v grep
echo ''

echo '[Nginx nginx.conf 경로]'

if [ `ps -ef | grep -v grep | grep nginx | grep -v grep | wc -l` -ge 1 ]
   then
      apa=`ps -ef | grep -v grep | grep nginx | grep -v grep | awk 'BEGIN{ OFS="\n"} {i=1; while(i<=NF) {print $i; i++}}' | grep '\/nginx' | grep "^/" | sed 's/.bin\/nginx//' | sort -u`
      for apd in $apa
      do
         if [ -f $apd/conf/nginx.conf ]
            then
               echo $apd/conf/nginx.conf
            else
               if [ -f $apd/nginx.conf ]
                  then
                     echo $apd/conf/nginx.conf
               fi
         fi
      done
   else
      echo 'nginx가 동작하지 않습니다.'
      exit
fi

echo ''

echo "※ 사용중인 nginx.conf 파일 경로를 입력하십시오. "

while true
do 
  echo "    (ex. /usr/local/nginx/conf/nginx.conf) : " 
  read apachep
  if [ `echo $apachep | grep "nginx\.conf" | wc -l` -eq 1 ]
    then
      if [ -f $apachep ]
         then 
            if [ `echo $apachep | grep '/conf/nginx\.conf' | wc -l` -eq 1 ]
               then
                  apache=`echo $apachep | sed 's/\/conf\/nginx\.conf//'`
                  conf=$apache/conf
              else
                  apache=`echo $apachep | sed 's/\/nginx\.conf//'`
                  conf=$apache
            fi
            break
         else
            echo "   입력하신 경로가 존재하지 않습니다. 다시 입력하여 주십시오."
            echo " "
       fi
    else
        echo "   잘못 입력하셨습니다. 다시 입력하여 주십시오."
        echo " "
  fi
done

echo " "

#echo $apache
#echo $conf
echo "****************************** Start **********************************" 
echo "****************************** Start **********************************"
echo "  ※  Launching Time: `date`                                                                      " >> $CREATE_FILE_RESULT 2>&1
echo "  ※  Result File: $CREATE_FILE_RESULT                                                            " >> $CREATE_FILE_RESULT 2>&1
echo "  ※  Hostname: `hostname`                                                                        " >> $CREATE_FILE_RESULT 2>&1
ipadd=`ifconfig -a | grep "inet " | awk -F":" '{i=1; while(i<=NF) {print $i; i++}}' | awk 'BEGIN{ OFS="\n"} {i=1; while(i<=NF) {print $i; i++}}' | egrep "^[1-9]" | egrep -v "^127|^255|255$"`
echo "  ※  ip address: `echo $ipadd`                                                                        " >> $CREATE_FILE_RESULT 2>&1
echo "********************************************************************************************************" >> $CREATE_FILE_RESULT 2>&1
uname -a                            >>  $CREATE_FILE_RESULT 2>&1
echo "********************************************************************************************************" >>  $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1
echo "###################################### Script Launching Time ################################################"
date
echo " "
echo " " >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
ps -ef |grep nginx |grep -v grep >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo '[nginx.conf 파일]' >> $CREATE_FILE_RESULT 2>&1
cat $conf/nginx.conf |grep 'User ' |grep -v '#' >> $CREATE_FILE_RESULT 2>&1
cat $conf/nginx.conf |grep 'Group ' |grep -v '#' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
if [ `ps -ef |grep nginx |grep -v grep| wc -l` -eq 0 ]
    then 
        result1='취약'
fi
if [ `ps -ef | grep nginx | grep -v grep | grep -v root | wc -l` -eq 0 ]
        then
                            result1='취약'
    elif [ `ps -ef |grep nginx | grep -v grep | grep root | wc -l` -ge 3 ]
        then
            result1='취약'
    else
        result1='양호'

        fuser=`ps -ef |grep nginx | grep -v grep | grep -v root |awk '{print  $1}' | head -1`
        luser=`ps -ef |grep nginx | grep -v grep | grep -v root |awk '{print  $1}' | tail -1`
        echo '[Apache /etc/passwd]' >> $CREATE_FILE_RESULT 2>&1

        fuserbin=`cat /etc/passwd | grep $fuser | awk -F':' '{print  $7}' |awk -F'/' '{print $3}'|head -1`
        luserbin=`cat /etc/passwd | grep $luser | awk -F':' '{print  $7}' |awk -F'/' '{print $3}'|head -1`

        if [ $fuser = $luser ]
            then        
            cat /etc/passwd |grep $fuser >> $CREATE_FILE_RESULT 2>&1
        else
            cat /etc/passwd |grep $fuser    >> $CREATE_FILE_RESULT 2>&1        
            cat /etc/passwd |grep $luser >> $CREATE_FILE_RESULT 2>&1
        fi
fi

echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1




echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' 설정파일 설정'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
cat $conf/nginx.conf |grep -i "^Include " |grep -v '#' >> $CREATE_FILE_RESULT 2>&1
confinc=`cat $conf/nginx.conf |grep -i "^Include " |grep -v '#' |awk '{print $2}'`
result3='양호'
echo '' >> $CREATE_FILE_RESULT 2>&1
for conlist in $confinc
    do
        if [ `echo $conlist | grep "^/" | wc -l` -eq 0 ]
            then
                conpath=$apache/$conlist
            else
                conpath=$conlist
        fi
        if [ -f $conpath ]
            then
                ls -alL $conpath >> $CREATE_FILE_RESULT 2>&1
                if [ `ls -alL $conpath | grep '\.conf*'| grep -v '....------'|wc -l` -gt 0 ]
                    then
                        result3='취약'
                fi
            else
                ls -alL $conpath >> $CREATE_FILE_RESULT 2>&1
        fi
done

echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo '[설정파일]' >> $CREATE_FILE_RESULT 2>&1

ls -alLR $conf >> $CREATE_FILE_RESULT 2>&1
echo '=================================================== 설정파일' >> $CREATE_FILE_RESULT 2>&1
cat $conf/nginx.conf >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
ls -al $apache >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
ls -al $apache/logs >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo '=================================================== 설정파일' >> $CREATE_FILE_RESULT 2>&1

if [ `ls -alLR $conf | grep '\.conf*'| grep -v '....------'|wc -l` -gt 0 ] 
    then
        result3='취약'
fi

echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1


echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1




echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' 보안 패치 적용'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1

if [ -f $apache/sbin/nginx ]
    then
        nginx=$apache/sbin/nginx
    else
        if [ -f $apache/sbin/nginx ]
            then
                nginx=$apache/sbin/nginx
            else
                if [ `ps -ef | grep -v grep | grep nginx | grep -v grep | awk 'BEGIN{ OFS="\n"} {i=1; while(i<=NF) {print $i; i++}}' | grep '\/nginx' | grep "^/" | sort -u | wc -l` -eq 1 ]
                    then
                        nginx=`ps -ef | grep -v grep | grep nginx | grep -v grep | awk 'BEGIN{ OFS="\n"} {i=1; while(i<=NF) {print $i; i++}}' | grep '\/nginx' | grep "^/" | sort -u`
                    else
                        nginx=`ps -ef | grep -v grep | grep nginx | grep " 1 " | awk 'BEGIN{ OFS="\n"} {i=1; while(i<=NF) {print $i; i++}}' | grep '\/nginx' | grep "^/"`
                fi
        fi
fi

ver=`$nginx -v |head -1|awk -F' ' '{print $3}'` >> $CREATE_FILE_RESULT 2>&1
ver1=`echo $ver |awk -F'/' '{print $2}'|awk -F'.' '{print $1}'`
ver2=`echo $ver |awk -F'/' '{print $2}'|awk -F'.' '{print $2}'`
ver3=`echo $ver |awk -F'/' '{print $2}'|awk -F'.' '{print $3}'` 

$nginx -v >> $CREATE_FILE_RESULT 2>&1

echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo '****************************** End ***********************************' 
#end script