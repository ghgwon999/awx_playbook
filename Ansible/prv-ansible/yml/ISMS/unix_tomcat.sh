#!/bin/sh

LANG=C
export LANG

BUILD_VER=1.0.4
LAST_UPDATE=2015.02.10
CREATE_FILE_RESULT=`hostname`"_before_ini_".txt


echo ""
echo "################# Tomcat 진단 스크립트를 실행하겠습니다 ###################"
echo ""
echo ""

alias ls=ls
alias grep=/bin/grep

echo '[현재 Tomcat 설치 경로]'
#ps -ef |grep -i  tomcat|awk -F'home' '{print $2}'|awk '{print $1}'|grep =|tail -1
ps -ef |grep -i tomcat |awk -F'home' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'
echo ''
#echo '[구동 중인 conf파일 경로]' 
#ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / 


#echo '[구동 중인 conf파일 경로]' >> $CREATE_FILE_RESULT 2>&1
#ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / >> $CREATE_FILE_RESULT 2>&1
#echo '' >> $CREATE_FILE_RESULT 2>&1
#echo '' >> $CREATE_FILE_RESULT 2>&1
#/svc/was/tomcat6.0.20/Svr1
#/svc/was/tomcat6.0.20/Svr2
echo ''
echo '' >> $CREATE_FILE_RESULT 2>&1
echo ''
echo "※ 위 내용을 확인하시고 Tomcat 설치 디렉터리를 입력하십시오. "
while true
do 
   echo -n "    (ex. /usr/local/tomcat/) : " 
   read tomcat
   if [ $tomcat ]
      then
         if [ -d $tomcat ]
            then 
               break
            else
               echo "   입력하신 디렉터리가 존재하지 않습니다. 다시 입력하여 주십시오."
               echo " "
         fi
    else
         echo "   잘못 입력하셨습니다. 다시 입력하여 주십시오."
         echo " "
   fi
done

#echo '[현재 Tomcat 설치 경로]' >> $CREATE_FILE_RESULT 2>&1
#ps -ef |grep -i tomcat|awk -F'home' '{print $2}'|awk '{print $1}'|grep =|tail -1 >> $CREATE_FILE_RESULT 2>&1
#ps -ef |grep -i tomcat|awk -F'home' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}' >> $CREATE_FILE_RESULT 2>&1
echo ''
echo '[ 구동중인 Tomat 디렉토리 ]' >> $CREATE_FILE_RESULT 2>&1


#Dcatalina.Base
bcount=`ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / |wc -l`


#Dcatalina.Base 경로
if [ $bcount -gt 5 ]
    then
    echo 'Dcatalina.Base가 너무 많음 수동진단하시기 바랍니다.'
    bcount=6
else
    if [ $bcount -eq 5 ]
        then
        ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / >> $CREATE_FILE_RESULT 2>&1
        base1=`ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / |grep -n ''|grep 1:|awk -F':' '{print $2}'`
        base2=`ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / |grep -n ''|grep 2:|awk -F':' '{print $2}'`
        base3=`ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / |grep -n ''|grep 3:|awk -F':' '{print $2}'`
        base4=`ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / |grep -n ''|grep 4:|awk -F':' '{print $2}'`
        base5=`ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / |grep -n ''|grep 5:|awk -F':' '{print $2}'`
    fi
    if [ $bcount -eq 4 ]
        then
        ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / >> $CREATE_FILE_RESULT 2>&1
        base1=`ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / |grep -n ''|grep 1:|awk -F':' '{print $2}'`
        base2=`ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / |grep -n ''|grep 2:|awk -F':' '{print $2}'`
        base3=`ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / |grep -n ''|grep 3:|awk -F':' '{print $2}'`
        base4=`ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / |grep -n ''|grep 4:|awk -F':' '{print $2}'`
    fi
    if [ $bcount -eq 3 ]
        then
        ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / >> $CREATE_FILE_RESULT 2>&1
        base1=`ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / |grep -n ''|grep 1:|awk -F':' '{print $2}'`
        base2=`ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / |grep -n ''|grep 2:|awk -F':' '{print $2}'`
        base3=`ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / |grep -n ''|grep 3:|awk -F':' '{print $2}'`
    fi
    if [ $bcount -eq 2 ]
        then
        ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / >> $CREATE_FILE_RESULT 2>&1
        base1=`ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / |grep -n ''|grep 1:|awk -F':' '{print $2}'`
        base2=`ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / |grep -n ''|grep 2:|awk -F':' '{print $2}'`
    fi
    if [ $bcount -eq 1 ]
        then
        ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / >> $CREATE_FILE_RESULT 2>&1
        base1=`ps -ef |grep -i tomcat |awk -F'base' '{print $2}'|awk '{print $1}'|awk -F'=' '{print $2}'|grep / |grep -n ''|grep 1:|awk -F':' '{print $2}'`
    #else
    #    echo 'Dcatalina.Base가 없음' >> $CREATE_FILE_RESULT 2>&1
    fi

fi



echo " "
conf=$tomcat
conf1=$base1
conf2=$base2
conf3=$base3
conf4=$base4
conf5=$base5
confall=`echo $conf1 $conf2 $conf3 $conf4 $conf5`


echo "" >> $CREATE_FILE_RESULT 2>&1
echo "" >> $CREATE_FILE_RESULT 2>&1
echo "================================= Start Time ==============================" >> $CREATE_FILE_RESULT 2>&1
date >> $CREATE_FILE_RESULT 2>&1
echo "" $CREATE_FILE_RESULT 2>&1


echo "INFO_CHKSTART"  >> $CREATE_FILE_RESULT 2>&1
echo >> $CREATE_FILE_RESULT 2>&1

echo "###################################   Linux Security Check Ver $BUILD_VER ($LAST_UPDATE)   ######################################"
echo "###################################   Linux Security Check Ver $BUILD_VER ($LAST_UPDATE)   ######################################" >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1
echo "############################################ Start Time ################################################"
date
date +%y%m > dtmp
echo " "
echo "############################################ Start Time ################################################" >> $CREATE_FILE_RESULT 2>&1
date >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1

echo "=================================== System Information Query Start ====================================="
echo "=================================== System Information Query Start =====================================" >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1

echo "#######################################   Kernel Information   #########################################"
echo "#######################################   Kernel Information   #########################################" >> $CREATE_FILE_RESULT 2>&1
uname -a >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1

echo "#########################################   IP Information   ###########################################"
echo "#########################################   IP Information   ###########################################" >> $CREATE_FILE_RESULT 2>&1
ifconfig -a >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1

echo "#########################################   Network Status   ###########################################"
echo "#########################################   Network Status   ###########################################" >> $CREATE_FILE_RESULT 2>&1
netstat -an | egrep -i "LISTEN|ESTABLISHED" >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1

echo "#######################################   Routing Information   ########################################"
echo "#######################################   Routing Information   ########################################" >> $CREATE_FILE_RESULT 2>&1
netstat -rn >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1

echo "##########################################   Process Status   ##########################################"
echo "##########################################   Process Status   ##########################################" >> $CREATE_FILE_RESULT 2>&1
ps -ef >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1

echo "##########################################   User Env   ################################################"
echo "##########################################   User Env   ################################################" >> $CREATE_FILE_RESULT 2>&1
env >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1

echo " " >> $CREATE_FILE_RESULT 2>&1

echo "##########################################  lsof -i -P  ################################################"
echo "##########################################  lsof -i -P  ################################################" >> $CREATE_FILE_RESULT 2>&1
lsof -i -P >> $CREATE_FILE_RESULT 2>&1
lsof -i -P > `hostname`_lsof.txt
echo " " >> $CREATE_FILE_RESULT 2>&1

echo " " >> $CREATE_FILE_RESULT 2>&1

echo "=================================== System Information Query End ======================================="
echo "=================================== System Information Query End =======================================" >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1


echo >> $CREATE_FILE_RESULT 2>&1
echo "INFO_CHKEND"  >> $CREATE_FILE_RESULT 2>&1

echo '' >> $CREATE_FILE_RESULT 2>&1 
echo '' >> $CREATE_FILE_RESULT 2>&1 
echo "****************************** Start **********************************" 
echo '============================== 1.1 =============================='
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' 1.1 데몬 관리'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo 'Tomcat의 데몬이 root 계정 외의 WAS 전용 계정으로 구동중이면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1
ps -ef |grep -i tomcat |grep -v "Tomcat_script" | grep -v "grep" >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

if [ `ps -ef | grep -i tomcat | grep -v "Tomcat_script" | grep -v "grep" | grep -v "root" | wc -l` -eq 0 ]
    then
            result2_1='■ 취약'
else
    result2_1='■ 양호'
    fuser=`ps -ef |grep -i tomcat | grep -v "Tomcat_script" | grep -v "grep" | awk '{print  $1}' | head -1`
    luser=`ps -ef |grep -i tomcat | grep -v "Tomcat_script" | grep -v "grep" | awk '{print  $1}' | tail -1`
    echo '[Tomcat /etc/passwd]' >> $CREATE_FILE_RESULT 2>&1

    fuserbin=`cat /etc/passwd | grep $fuser | awk -F':' '{print  $7}' |awk -F'/' '{print $3}'|head -1`
    luserbin=`cat /etc/passwd | grep $luser | awk -F':' '{print  $7}' |awk -F'/' '{print $3}'|head -1`
    if [ $fuser = $luser ]
        then        
        cat /etc/passwd |grep $fuser >> $CREATE_FILE_RESULT 2>&1

    else
        cat /etc/passwd |grep $fuser    >> $CREATE_FILE_RESULT 2>&1        
        cat /etc/passwd |grep $luser >> $CREATE_FILE_RESULT 2>&1
    fi
fi

echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo '※ Tomcat의 데몬을 root 계정 외의 WAS 전용 계정으로 변경하여 구동 권고' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo $result2_1  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo '============================== 1.2 =============================='
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' 1.2 관리서버 디렉토리 권한 설정'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo '관리 서버 홈디렉토리 퍼미션이 750이하이면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1


echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo '[ 관리서버 ]' >> $CREATE_FILE_RESULT 2>&1
echo '경로(버전 6.x, 7.x) : '$tomcat'/webapps' >> $CREATE_FILE_RESULT 2>&1
ls -al $tomcat/webapps |grep manager >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo '경로(버전 4.x, 5.x) : '$tomcat'/server/webapps' >> $CREATE_FILE_RESULT 2>&1
ls -al $tomcat/server/webapps |grep manager >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

if [ `ls -al $tomcat/webapps |grep manager|grep -v '.r...-.---'|wc -l ` -eq 0 ]
    then
    sresult2_2='■ 양호'
else
    sresult2_2='■ 취약'
fi

echo '※ 관리 서버 홈디렉토리 퍼미션이 750이하로 설정 권고' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo $sresult2_2 >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1


echo '============================== 1.3 =============================='
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' 1.3 설정파일 권한 설정'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo 'Tomcat conf 디렉터리 내부의 설정파일 퍼미션이 600 또는 700이하이면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1

cresult2_32='■ 양호'

if [ $bcount -gt 5 ]
    then
    echo 'Dcatalina.Base가 너무 많음 수동진단하시기 바랍니다.'
    bcount=6
else
    for folder in $confall
        do
            if [ -d $folder ]
                then
                    echo '[ Tomcat 구동 디렉토리 ]' $folder/conf/ >> $CREATE_FILE_RESULT 2>&1
                    ls -al $folder/conf/|egrep ".xml|.properties|.policy"|grep -v 'd.........' >> $CREATE_FILE_RESULT 2>&1
                    if [ `ls -al $folder/conf/|egrep ".xml|.properties|.policy"|grep -v 'd.........'|grep -v '....------'|wc -l` -gt 0 ]
                        then
                            cresult2_32='■ 취약'
                    fi
                    echo '' >> $CREATE_FILE_RESULT 2>&1
            fi
    done
fi

echo '※ Tomcat conf 디렉터리 내부의 설정파일 퍼미션을 600 또는 700이하로 설정 권고' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo $cresult2_32 >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1


echo '============================== 1.4 =============================='
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' 1.4 로그 디렉토리/파일 권한 관리'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo '로그 디렉토리 퍼미션 750 이하, 로그 파일 퍼미션 640이하 이면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1


result2_55='양호'

if [ $bcount -gt 5 ]
    then
    echo 'Dcatalina.Base가 너무 많음 수동진단하시기 바랍니다.'
    bcount=6
else
    for folder in $confall
        do
            if [ -d $folder ]
                then
                    log=`cat $folder/conf/server.xml|grep -v '!--'|grep -n ' '|grep 'valves.AccessLog'|awk -F'"' '{print $4}'`
                    if [ `ls -al $folder|grep $log|grep -v '.....-.---'|wc -l` -gt 0 ]
                    then
                            result2_55='취약'
                    fi
                    echo '' >> $CREATE_FILE_RESULT 2>&1
            fi
    done
fi



result2_5='양호'

if [ $bcount -gt 5 ]
    then
    echo 'Dcatalina.Base가 너무 많음 수동진단하시기 바랍니다.'
    bcount=6
else
    for folder in $confall
        do
            if [ -d $folder ]
                then
                    log=`cat $folder/conf/server.xml|grep -v '!--'|grep -n ' '|grep 'valves.AccessLog'|awk -F'"' '{print $4}'`
                    echo '| Log 디렉토리 |' $folder/$log >> $CREATE_FILE_RESULT 2>&1
                    ls -al $folder/$log >> $CREATE_FILE_RESULT 2>&1
                    if [ `ls -al $folder/$log|grep -v 'd.........' |grep -v 'total'|grep -v '...-.-----'|wc -l` -gt 0 ]
                    then
                            result2_5='취약'
                    fi
                    echo '' >> $CREATE_FILE_RESULT 2>&1
            fi
    done
fi

if [ \( $result2_55 == "취약" \) -o \( $result2_5 == "취약" \) ]
    then
        result2_555='■ 취약'
    else
        result2_555='■ 양호'
fi



echo '※ 로그파일 퍼미션을 640이하, 디렉토리 750이하로 설정을 권고함.'>> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo $result2_555 >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1


echo '============================== 1.5 =============================='
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' 1.5 로그 포맷 설정 '  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo '로그 포맷 설정이  combined로 되어 있으면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1


cresult1_5='■ 양호'

if [ $bcount -gt 5 ]
    then
    echo 'Dcatalina.Base가 너무 많음 수동진단하시기 바랍니다.'
    bcount=6
else
    for folder in $confall
        do
            if [ -d $folder ]
                then
                    echo '[ Tomcat 구동 디렉토리 ]' $folder/conf/server.xml >> $CREATE_FILE_RESULT 2>&1
                    cat $folder/conf/server.xml|grep -v 'Note'|grep -v '#' |grep 'pattern=' |awk -F' ' '{print $1}' >> $CREATE_FILE_RESULT 2>&1
                    if [ `cat $folder/conf/server.xml|grep -v 'Note'| grep -v '#' |grep 'pattern='|awk -F'"' '{print $2}'` != 'combined' ]
                        then
                            cresult1_5='■ 취약'
                    fi
                    echo '' >> $CREATE_FILE_RESULT 2>&1
            fi
    done
fi

echo '※ 로그 포맷을 combined로 설정 권고' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo $cresult1_5 >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1


echo '============================== 1.6 =============================='

echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' 1.6 HTTP Method 제한'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo 'PUT, DELETE, TRACE Method 제한 설정이 되어 있으면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1

cresult1_7='■ 양호'

if [ $bcount -gt 5 ]
    then
    echo 'Dcatalina.Base가 너무 많음 수동진단하시기 바랍니다.'
    bcount=6
else
    for folder in $confall
        do
            if [ -d $folder ]
                then
                    echo '[ Tomcat 구동 디렉토리 ]' $folder/conf/web.xml >> $CREATE_FILE_RESULT 2>&1
                    cat $folder/conf/web.xml|grep -v '!--'|grep -n ''|grep -i forbidden  >> $CREATE_FILE_RESULT 2>&1
                    cat $folder/conf/web.xml|grep -v '!--'|grep -n ''|grep -i method >> $CREATE_FILE_RESULT 2>&1
                    if [ `cat $folder/conf/web.xml|grep -v '!--'|grep -n ''|grep -i method | egrep -i "put|delete|trace"|wc -l` -lt 3 ]
                        then
                            cresult1_7='■ 취약'
                    fi
                    echo '' >> $CREATE_FILE_RESULT 2>&1
            fi
    done
fi

echo '※ web.xml 파일에서 PUT, DELETE, TRACE Method 설정을 제한' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo $cresult1_7 >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1


echo '============================== 1.7 =============================='
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' 1.7 디렉토리 검색 기능 제거'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo 'web.xml내부의 listing설정이 false로 설정되어있으면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1


cresult1_8='■ 양호'

if [ $bcount -gt 5 ]
    then
    echo 'Dcatalina.Base가 너무 많음 수동진단하시기 바랍니다.'
    bcount=6
else
    for folder in $confall
        do
            if [ -d $folder ]
                then
                    echo '[ Tomcat 구동 디렉토리 ]' $folder/conf/web.xml >> $CREATE_FILE_RESULT 2>&1
                    mcount=`cat $folder/conf/web.xml|grep -v '!--'|grep -n ' '|grep listing|awk -F':' '{print $1}'`
                    mcount=$((mcount+1))
                    cat $folder/conf/web.xml|grep -v '!--'|grep -n ' '|grep listing|awk -F':' '{print $2}' >> $CREATE_FILE_RESULT 2>&1
                    cat $folder/conf/web.xml|grep -v '!--'|grep -n ' '|grep $mcount|head -1|awk -F':' '{print $2}' >> $CREATE_FILE_RESULT 2>&1
                    if [ `cat $folder/conf/web.xml|grep -v '!--'|grep -n ' '|grep $mcount|head -1|awk -F':' '{print $2}'|awk -F'>' '{print $2}'|awk -F'<' '{print $1}'` == 'true' ]
                        then
                            cresult1_8='■ 취약'
                    fi
                    echo '' >> $CREATE_FILE_RESULT 2>&1
            fi
    done
fi

echo '※ 디렉토리 구조 및 주요 설정파일의 내용을 노출 시킬 수 있는 listing 설정 값 false로 변경 권고' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo $cresult1_8 >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo '============================== 1.8 =============================='
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' 1.8 Session Timeout 설정'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo 'Session Timeout 60분 이내로 변경 (Default = 30) ' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1

cresult1_9='■ 양호'

if [ $bcount -gt 5 ]
    then
    echo 'Dcatalina.Base가 너무 많음 수동진단하시기 바랍니다.'
    bcount=6
else
    for folder in $confall
        do
            if [ -d $folder ]
                then
                    echo '[ Tomcat 구동 디렉토리 ]' $folder/conf/web.xml >> $CREATE_FILE_RESULT 2>&1
                    cat $folder/conf/web.xml|grep -v '!--'|grep -n ' '|grep session-timeout|awk -F':' '{print $2}' >> $CREATE_FILE_RESULT 2>&1
                    if [ `cat $folder/conf/web.xml|grep -v '!--'|grep session-timeout|egrep -o "[0-9]+"` -gt 60 ]
                        then
                            cresult1_9='■ 취약'
                    fi
                    echo '' >> $CREATE_FILE_RESULT 2>&1
            fi
    done
fi

echo '※ Session Timeout 60분 이내로 변경 권고' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo $cresult1_9 >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1


echo '============================== 2.1 =============================='
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' 2.1 불필요한 Examples 디렉토리 삭제'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo '운영상 불필요한 example 디렉토리가 제거 되어 있으면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1

result3_1='■ 양호'

if [ $bcount -gt 5 ]
    then
    echo 'Dcatalina.Base가 너무 많음 수동진단하시기 바랍니다.'
    bcount=6
else
    for folder in $confall
        do
            if [ -d $folder ]
                then
                    echo '[ Tomcat 구동 디렉토리 ]' $folder/webapps/ >> $CREATE_FILE_RESULT 2>&1
                    ls -al $folder/webapps/|grep 'd.........' >> $CREATE_FILE_RESULT 2>&1
                    if [ `ls $folder/webapps|grep -w examples|wc -l` -ge 1 ]
                        then
                            result3_1='■ 취약'
                    else
                        echo $folder/webapps/examples/ '디렉토리 없음' >> $CREATE_FILE_RESULT 2>&1
                    fi
                    echo '' >> $CREATE_FILE_RESULT 2>&1
            fi
    done
fi

echo '※ example 디렉토리는 불필요시 삭제 권고' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo $result3_1 >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo '============================== 2.2 =============================='
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' 2.2 프로세스 관리기능 삭제'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1
echo '불필요한 프로세스 관리 디렉토리가 삭제 되어 있으면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1

result3_2='■ 양호'

if [ $bcount -gt 5 ]
    then
    echo 'Dcatalina.Base가 너무 많음 수동진단하시기 바랍니다.'
    bcount=6
else
    for folder in $confall
        do
            if [ -d $folder ]
                then
                    echo '[ Tomcat 구동 디렉토리 ]' $folder >> $CREATE_FILE_RESULT 2>&1
                    find $folder -name catalina-manager.jar
                    if [ `find $folder -name catalina-manager.jar|wc -l` -ge 1 ]
                        then
                            result3_2='■ 취약'
                    else
                        echo $folder/'catalina-manager.jar 파일 없음' >> $CREATE_FILE_RESULT 2>&1
                    fi
                    echo '' >> $CREATE_FILE_RESULT 2>&1
            fi
    done
fi

echo '※ Tomcat 설치시 관리자 프로세스 관리 기능이 웹상에서 가능하므로 catalina-manager.jar 파일 삭제' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo  $result3_2 >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo '============================== 3.1 =============================='
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' 3.1 보안 패치 적용'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '주기적으로 최신 패치 적용 작업이 진행되고 있으면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1
echo '[ 구동중인 Tomcat Version ]'  >> $CREATE_FILE_RESULT 2>&1
sh $folder/bin/version.sh |grep version|awk -F':' '{print $2}'|awk -F'/' '{print $2}' >> $CREATE_FILE_RESULT 2>&1

ver=`sh $tomcat/bin/version.sh |grep version|awk -F':' '{print $2}'|awk -F'/' '{print $2}'`
ver1=`sh $tomcat/bin/version.sh |grep version|awk -F':' '{print $2}'|awk -F'/' '{print $2}'|awk -F'.' '{print $1}'`
ver2=`sh $tomcat/bin/version.sh |grep version|awk -F':' '{print $2}'|awk -F'/' '{print $2}'|awk -F'.' '{print $2}'`
ver3=`sh $tomcat/bin/version.sh |grep version|awk -F':' '{print $2}'|awk -F'/' '{print $2}'|awk -F'.' '{print $3}'`

if [ $ver1 -eq 7 ]
    then
    if [ $ver3 -ge 35 ]
        then 
        result4_11='■ 양호'
    else
        result4_11='■ 취약'
    fi            
elif [ $ver1 -eq 4 ]
    then
    result4_11='■ 취약'

elif [ $ver1 -eq 5 ]
    then
    if [ $ver2 -ge 5 ]
        then
        if [ $ver3 -ge 35 ]
            then
            result4_11='■ 양호'
        else
            result4_11='■ 취약'
        fi
    else
        result4_11='■ 취약'
    fi
else
    if [ $ver1 -eq 6 ]
        then
        if [ $ver3 -ge 36 ]
                then
                result4_11='■ 양호'
            else
                result4_11='■ 취약'
        fi
    else
        echo '버전 정보를 찾을 수 없음' >> $CREATE_FILE_RESULT 2>&1
        result4_11='■ 취약'
    fi
fi

echo '' >> $CREATE_FILE_RESULT 2>&1
echo '[최신 버전]' >> $CREATE_FILE_RESULT 2>&1
echo 'Tomcat 7.0.x    7.0.35' >> $CREATE_FILE_RESULT 2>&1
echo 'Tomcat 6.0.x    6.0.36' >> $CREATE_FILE_RESULT 2>&1
echo 'Tomcat 5.5.x    5.5.35' >> $CREATE_FILE_RESULT 2>&1
echo 'Tomcat 5.5버전 미만은 기술지원 완료로 인해 패치 지원 안함' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo '※ 영향도 평가 이후 최신 패치 수행 권고' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1


echo $result4_11  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo '============================== 4.1 =============================='
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' 4.1 관리자 콘솔 접근통제'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo '불필요한 경우, Tomcat 관리자 콘솔 사용 금지되어 있고, 필요한 경우 Default Port 변경해서 사용하는 경우 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1
echo '1) Tomcat 관리자 콘솔 확인'  >> $CREATE_FILE_RESULT 2>&1

cresult4_1_1='양호'

if [ $bcount -gt 5 ]
    then
    echo 'Dcatalina.Base가 너무 많음 수동진단하시기 바랍니다.'
    bcount=6
else
    for folder in $confall
        do
            if [ -d $folder ]
                then
                    echo '[ Tomcat 구동 디렉토리 ]' $folder >> $CREATE_FILE_RESULT 2>&1
                    ls -al $folder/webapps |grep -w manager >> $CREATE_FILE_RESULT 2>&1
                    ls -al $folder/webapps |grep -w admin >> $CREATE_FILE_RESULT 2>&1
                    #find $folder -name admin.xml >> $CREATE_FILE_RESULT 2>&1
                    #find $folder -name manager.xml >> $CREATE_FILE_RESULT 2>&1
                    if [ \( `ls -al $folder/webapps |grep -w manager |wc -l` -ge 1 \) -o \( `ls -al $folder/webapps |grep -w admin |wc -l` -ge 1 \) ]
                        then
                            cresult4_1_1='취약'
                    else
                        echo 'admin.xml, manager.xml 파일 없음' >> $CREATE_FILE_RESULT 2>&1
                    fi
                    echo '' >> $CREATE_FILE_RESULT 2>&1
            fi
    done
fi

if [ $cresult4_1_1 == "취약" ]
then
echo '2) Default 포트(8080) 확인'  >> $CREATE_FILE_RESULT 2>&1

cresult4_1_2='양호'

if [ $bcount -gt 5 ]
    then
    echo 'Dcatalina.Base가 너무 많음 수동진단하시기 바랍니다.'
    bcount=6
else
    for folder in $confall
        do
            if [ -d $folder ]
                then
                    echo '[ Tomcat 구동 디렉토리 ]' $folder/conf/server.xml >> $CREATE_FILE_RESULT 2>&1
                    cat $folder/conf/server.xml |grep 'Connector port'|grep -v '#'|awk -F" " '{print $2}' >> $CREATE_FILE_RESULT 2>&1
                    #if [ `cat $folder/conf/server.xml |grep 'Connector port'|awk '{print $2}'|awk -F'"' '{print $2}'` == '8080' ]
                    if [ `cat $folder/conf/server.xml |grep 'Connector port'|grep -v '#'|awk '{print $2}'|grep 8080|wc -l` -ge 1 ]
                        then
                            cresult4_1_2='취약'
                    fi
                    echo '' >> $CREATE_FILE_RESULT 2>&1
            fi
    done
fi
fi

if [ $cresult4_1_1 == "취약" ]
    then
        if [ $cresult4_1_2 == "취약" ]
            then
                result44_1='■ 취약'
        else
            result44_1='■ 양호'
        fi
else
    result44_1='■ 양호'
fi

echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo '[수동 방법]' >> $CREATE_FILE_RESULT 2>&1    
echo '(예 1) telnet 127.0.0.1 8080 으로 접속 후 GET http://127.0.0.1/admin/ HTTP/1.0를 요청 시 200 OK(Tomcat관련 웹페이지)가 나올 경우 취약' >> $CREATE_FILE_RESULT 2>&1    
echo '(예 2) telnet 127.0.0.1 8080 으로 접속 후 GET http://127.0.0.1/manager/ HTTP/1.0를 요청 시 200 OK(Tomcat관련 웹페이지)가 나올 경우 취약' >> $CREATE_FILE_RESULT 2>&1    
echo ' ' >> $CREATE_FILE_RESULT 2>&1    
echo '※ 불필요한 경우, Tomcat 관리자 콘솔 사용 금지하고, 필요한 경우 Default Port 변경 권고' >> $CREATE_FILE_RESULT 2>&1    
echo ' ' >> $CREATE_FILE_RESULT 2>&1    

echo $result44_1  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo '============================== 4.2 =============================='
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' 4.2 관리자 default 계정명 변경'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo '관리자 콘솔 default 값으로 제공된 계정명이 변경 되어 있으면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1

if [ $bcount -gt 5 ]
    then
    echo 'Dcatalina.Base가 너무 많음 수동진단하시기 바랍니다.'
    bcount=6
else
    for folder in $confall
        do
            if [ -d $folder ]
                then
                    echo '[ Tomcat 구동 디렉토리 ]' $folder/conf/tomcat-users.xml >> $CREATE_FILE_RESULT 2>&1
                    awk '/tomcat-users/,/\/tomcat-users/' $folder/conf/tomcat-users.xml >> $CREATE_FILE_RESULT 2>&1
                    echo '' >> $CREATE_FILE_RESULT 2>&1
            fi
    done
fi

echo '※ 관리자 콘솔 default 계정명을 유추 힘든 계정으로 변경 권고' >> $CREATE_FILE_RESULT 2>&1    
echo ' ' >> $CREATE_FILE_RESULT 2>&1    

echo '■ 수동'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo '============================== 4.3 =============================='
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' 4.3 관리자 패스워드 암호정책'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo '패스워드 보안 정책에 맞게 사용중이면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1

if [ $bcount -gt 5 ]
    then
    echo 'Dcatalina.Base가 너무 많음 수동진단하시기 바랍니다.'
    bcount=6
else
    for folder in $confall
        do
            if [ -d $folder ]
                then
                    echo '[ Tomcat 구동 디렉토리 ]' $folder/conf/tomcat-users.xml >> $CREATE_FILE_RESULT 2>&1
                    awk '/tomcat-users/,/\/tomcat-users/' $folder/conf/tomcat-users.xml >> $CREATE_FILE_RESULT 2>&1
                    echo '' >> $CREATE_FILE_RESULT 2>&1
                    echo '' >> $CREATE_FILE_RESULT 2>&1
            fi
    done
fi

echo '※ 패스워드 보안 정책에 맞게 영문/숫자 조합 8자리 이상의 길이로 구성 권고' >> $CREATE_FILE_RESULT 2>&1    
echo ' ' >> $CREATE_FILE_RESULT 2>&1    

echo '■ 수동'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo '============================== 4.4 =============================='
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' 4.4 tomcat-users.xml 파일 권한 설정'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo '패스워드 파일 (tomcat-users.xml) 퍼미션이 600 또는 700이면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1

result4_4='■ 양호'

if [ $bcount -gt 5 ]
    then
    echo 'Dcatalina.Base가 너무 많음 수동진단하시기 바랍니다.'
    bcount=6
else
    for folder in $confall
        do
            if [ -d $folder ]
                then
                    echo '[ Tomcat 구동 디렉토리 ]' $folder/conf/tomcat-users.xml >> $CREATE_FILE_RESULT 2>&1
                    ls -al $folder/conf|grep tomcat-users.xml >> $CREATE_FILE_RESULT 2>&1
                    if [ `ls -al $folder/conf|grep tomcat-users.xml|grep -v '....------'|wc -l` -ge 1 ]
                        then
                            result4_4='■ 취약'
                    fi
                    echo '' >> $CREATE_FILE_RESULT 2>&1
                    echo '' >> $CREATE_FILE_RESULT 2>&1
            fi
    done
fi


echo '※ 패스워드 파일(tomcat-users.xml) 퍼미션 600 또는 700으로 변경 권고' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1

echo $result4_4  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo '[참고]' >> $CREATE_FILE_RESULT 2>&1
echo '[ web.xml ]' >> $CREATE_FILE_RESULT 2>&1
cat $folder/conf/web.xml >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo '[ server.xml ]' >> $CREATE_FILE_RESULT 2>&1
cat $folder/conf/server.xml >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo '****************************** End ***********************************' 
#end script

_HOSTNAME=`hostname`
CREATE_FILE_RESULT_END="TOMCAT_"${_HOSTNAME}"_"`date +%m%d`"-"`date +%H%M`.txt
echo > $CREATE_FILE_RESULT_END

CREATE_FILE_RESULT=`hostname`"_before_ini_".txt

mv $CREATE_FILE_RESULT $CREATE_FILE_RESULT_END

rm -Rf dtmp
rm -Rf `hostname`_result_temp2.txt
rm -Rf `hostname`_lsof.txt

echo "☞ 진단작업이 완료되었습니다. 수고하셨습니다!"
