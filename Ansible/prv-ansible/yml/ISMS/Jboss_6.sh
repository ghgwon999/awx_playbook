#!/bin/sh

LANG=C
export LANG

BUILD_VER=1.0.4
LAST_UPDATE=2015.02.10
CREATE_FILE_RESULT=`hostname`"_before_ini_".txt


echo ""
echo "################# JBoss 진단 스크립트를 실행하겠습니다 ###################"
echo ""
echo ""

alias ls=ls
alias grep=/bin/grep

echo '[현재 JBoss 설치 경로]'
ps -ef |grep jboss
echo ''
echo ''
echo '' >> $CREATE_FILE_RESULT 2>&1
echo ''
echo "※ 위 내용을 확인하시고 JBoss 설치 디렉터리를 입력하십시오. "
while true
do 
   echo -n "    (ex. /usr/local/JBoss/) : " 
   read JBoss
   if [ $JBoss ]
      then
         if [ -d $JBoss ]
            then 
               break
            else
               echo "   입력하신 디렉터리가 존재하지 않습니다. 다시 입력하여 주십시오."
               echo " "
         fi
    else
         echo "   잘못 입력하셨습니다. 다시 입력하여 주십시오."
         echo " "
   fi
done

echo "※ 위 내용을 확인하시고 JBoss 서버 디렉터리를 입력하십시오. "
while true
do 
   echo -n "    (ex. /usr/local/JBoss/server/default) : " 
   read Server
   if [ $Server ]
      then
         if [ -d $Server ]
            then 
               break
            else
               echo "   입력하신 디렉터리가 존재하지 않습니다. 다시 입력하여 주십시오."
               echo " "
         fi
    else
         echo "   잘못 입력하셨습니다. 다시 입력하여 주십시오."
         echo " "
   fi
done

echo ''
echo '[ 구동중인 JBoss 디렉토리 ]' >> $CREATE_FILE_RESULT 2>&1
echo $JBoss                        >> $CREATE_FILE_RESULT 2>&1
echo ''
echo '[ 구동중인 JBoss 관리서버 디렉토리 ]' >> $CREATE_FILE_RESULT 2>&1
echo $Server                        >> $CREATE_FILE_RESULT 2>&1


echo " "
if [ `ps -ef | grep $Server | grep -i 'standalone' | wc -l` -eq 0 ]
then
	conf=$Server/../../conf
else
	conf=$Server/configuration
fi

serdir=`find $Server -name conf |awk -F'/' '{print $(NF-1)}'`

echo "" >> $CREATE_FILE_RESULT 2>&1
echo "" >> $CREATE_FILE_RESULT 2>&1
echo "================================= Start Time ==============================" >> $CREATE_FILE_RESULT 2>&1
date >> $CREATE_FILE_RESULT 2>&1
echo "" $CREATE_FILE_RESULT 2>&1


echo "INFO_CHKSTART"  >> $CREATE_FILE_RESULT 2>&1
echo >> $CREATE_FILE_RESULT 2>&1

echo "###################################   Linux Security Check Ver $BUILD_VER ($LAST_UPDATE)   ######################################"
echo "###################################   Linux Security Check Ver $BUILD_VER ($LAST_UPDATE)   ######################################" >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1
echo "############################################ Start Time ################################################"
date
date +%y%m > dtmp
echo " "
echo "############################################ Start Time ################################################" >> $CREATE_FILE_RESULT 2>&1
date >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1

echo "=================================== System Information Query Start ====================================="
echo "=================================== System Information Query Start =====================================" >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1

echo "#######################################   Kernel Information   #########################################"
echo "#######################################   Kernel Information   #########################################" >> $CREATE_FILE_RESULT 2>&1
uname -a >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1

echo "#########################################   IP Information   ###########################################"
echo "#########################################   IP Information   ###########################################" >> $CREATE_FILE_RESULT 2>&1
ifconfig -a >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1

echo "#########################################   Network Status   ###########################################"
echo "#########################################   Network Status   ###########################################" >> $CREATE_FILE_RESULT 2>&1
netstat -an | egrep -i "LISTEN|ESTABLISHED" >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1

echo "#######################################   Routing Information   ########################################"
echo "#######################################   Routing Information   ########################################" >> $CREATE_FILE_RESULT 2>&1
netstat -rn >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1

echo "##########################################   Process Status   ##########################################"
echo "##########################################   Process Status   ##########################################" >> $CREATE_FILE_RESULT 2>&1
ps -ef >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1

echo "##########################################   User Env   ################################################"
echo "##########################################   User Env   ################################################" >> $CREATE_FILE_RESULT 2>&1
env >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1

echo " " >> $CREATE_FILE_RESULT 2>&1

echo "##########################################  lsof -i -P  ################################################"
echo "##########################################  lsof -i -P  ################################################" >> $CREATE_FILE_RESULT 2>&1
lsof -i -P >> $CREATE_FILE_RESULT 2>&1
lsof -i -P > `hostname`_lsof.txt
echo " " >> $CREATE_FILE_RESULT 2>&1

echo " " >> $CREATE_FILE_RESULT 2>&1

echo "=================================== System Information Query End ======================================="
echo "=================================== System Information Query End =======================================" >> $CREATE_FILE_RESULT 2>&1
echo " " >> $CREATE_FILE_RESULT 2>&1


echo >> $CREATE_FILE_RESULT 2>&1
echo "INFO_CHKEND"  >> $CREATE_FILE_RESULT 2>&1

echo "****************************** Start **********************************" 
echo '============================== 1.1 =============================='
echo 'A-01 START'>> $CREATE_FILE_RESULT 2>&1
echo ' 1.1 데몬 관리'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo 'JBoss의 데몬이 root 계정 외의 WAS 전용 계정으로 구동중이면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1
ps -ef |grep -i jboss | grep -v 'grep' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

if [ `ps -ef |grep -i jboss | grep -v "grep" | grep -v "root" | wc -l` -eq 0 ]
then
        result1_1='취약'
else
	result1_1='양호'
fi
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo '※ JBoss의 데몬을 root 계정 외의 WAS 전용 계정으로 변경하여 구동 권고' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo 'A-01 진단결과 : '$result1_1 >> $CREATE_FILE_RESULT 2>&1
echo 'A-01 END' >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo '============================== 1.2 =============================='
echo 'A-02 START' >> $CREATE_FILE_RESULT 2>&1
echo ' 1.2 관리서버 디렉토리 권한 설정'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo '관리 서버 홈디렉토리 퍼미션이 750이하이면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo '[ 관리서버 ]' >> $CREATE_FILE_RESULT 2>&1
echo '경로(버전 6.x) : '$Server  >> $CREATE_FILE_RESULT 2>&1
ls -al $Server/../ >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

if [ `ls -al $Server/../ |grep -v '.r...-.---' | wc -l` -eq 0 ]
	then
	sresult1_2='양호'
else
	sresult1_2='취약'
fi

echo '※ 관리 서버 홈디렉토리 퍼미션이 750이하로 설정 권고' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo 'A-02 진단결과 : '$sresult1_2 >> $CREATE_FILE_RESULT 2>&1
echo 'A-02 END' >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1


echo '============================== 1.3 =============================='
echo 'A-03 START' >> $CREATE_FILE_RESULT 2>&1
echo ' 1.3 설정파일 권한 설정'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo 'JBoss conf 디렉터리 내부의 설정파일 퍼미션이 600 또는 700이하이면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1
ls -al $conf >> $CREATE_FILE_RESULT 2>&1
if [ `ls -al $conf |grep -v '^-...------' |wc -l` -eq 0 ]
	then
	cresult1_3='양호'
else
	cresult1_3='취약'
fi
echo '※ JBoss configuration 디렉터리 내부의 설정파일 퍼미션을 600 또는 700이하로 설정 권고' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo 'A-03 진단결과 : '$cresult1_3 >> $CREATE_FILE_RESULT 2>&1
echo 'A-03 END'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1


echo '============================== 1.4 =============================='
echo 'A-04 START' >> $CREATE_FILE_RESULT 2>&1
echo ' 1.4 로그 디렉토리/파일 권한 관리'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo '로그 디렉토리 퍼미션 750 이하, 로그 파일 퍼미션 640이하 이면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1
ls -al $Server >> $CREATE_FILE_RESULT 2>&1
if [ `ls -al $Server | grep log | grep -v '^d....-.---' |wc -l` -eq 0 ]
	then
	result1_4='양호'
else
	result1_4='취약'
fi
echo '※ 로그파일 퍼미션을 640이하, 디렉토리 750이하로 설정을 권고함.'>> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo 'A-04 진단결과 : '$result1_4 >> $CREATE_FILE_RESULT 2>&1
echo 'A-04 END' >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1


echo '============================== 1.5 =============================='
echo 'A-05 START' >> $CREATE_FILE_RESULT 2>&1
echo ' 1.5 로그 포맷 설정 '  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo '로그 포맷 설정이  combined로 되어 있으면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1

if [ `ps -ef | grep $Server | grep -i 'standalone' | wc -l` -eq 0 ]
then
	echo [domain.xml]   >> $CREATE_FILE_RESULT 2>&1
	cat $conf/domain.xml |grep pattern >> $CREATE_FILE_RESULT 2>&1
else
	echo [standalone.xml]   >> $CREATE_FILE_RESULT 2>&1
	cat $conf/standalone.xml |grep pattern >> $CREATE_FILE_RESULT 2>&1
	echo ' ' >> $CREATE_FILE_RESULT 2>&1
	echo [standalone-ha.xml]   >> $CREATE_FILE_RESULT 2>&1
	cat $conf/standalone-ha.xml |grep pattern >> $CREATE_FILE_RESULT 2>&1

fi
echo ' ' >> $CREATE_FILE_RESULT 2>&1
cresult1_5='수동'
echo '※ 로그 포맷을 combined로 설정 권고' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo 'A-05 진단결과 : '$cresult1_5 >> $CREATE_FILE_RESULT 2>&1
echo 'A-05 END' >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1


echo '============================== 1.6 =============================='
echo 'A-06' >> $CREATE_FILE_RESULT 2>&1
echo ' 1.6 로그 저장주기'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo '법에 정해진 최소 로그저장기간 적용 시 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1
cresult1_6='수동'
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo 'A-06 진단결과 : '$cresult1_6 >> $CREATE_FILE_RESULT 2>&1
echo 'A-06 END' >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1


echo '============================== 1.7 =============================='
echo 'A-07' >> $CREATE_FILE_RESULT 2>&1
echo ' 1.7 디렉토리 검색 기능 제거'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo 'web.xml내부의 listing설정이 false로 설정되어있으면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1
if [ `ls -al $conf |grep standalone.xml |wc -l` -eq 0 ]
then
	echo '[ '$conf'/domain.xml]'  >> $CREATE_FILE_RESULT 2>&1	
	if [ `cat $conf/domain.xml | grep listings |wc -l` -eq 0 ]
	then
		echo 'Directory listing 설정이 없습니다.' >> $CREATE_FILE_RESULT 2>&1
		cresult1_6='양호'
	else
		cat $conf/domain.xml | grep listings >> $CREATE_FILE_RESULT 2>&1
		cresult1_6='수동'
	fi
else
	echo '[ '$conf'/standalone.xml]'  >> $CREATE_FILE_RESULT 2>&1
	if [ `cat $conf/standalone.xml | grep listings |wc -l` -eq 0 ]
	then
		echo 'Directory listing 설정이 없습니다.' >> $CREATE_FILE_RESULT 2>&1
		cresult1_6='양호'
	else
		cat $conf/standalone.xml | grep listings >> $CREATE_FILE_RESULT 2>&1
		cresult1_6='수동'
	fi
fi

echo '※ 디렉토리 구조 및 주요 설정파일의 내용을 노출 시킬 수 있는 listing 설정 값 false로 변경 권고' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo 'A-07 진단결과 : '$cresult1_6 >> $CREATE_FILE_RESULT 2>&1
echo 'A-07 END' >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo '============================== 1.8 =============================='
echo 'A-08' >> $CREATE_FILE_RESULT 2>&1
echo ' 1.8 데이터소스의 패스워드 암호화'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo 'DB접속 패스워드를 암호화하여 설정 ' >> $CREATE_FILE_RESULT 2>&1
echo '(standalone.xml 또는 domain.xml에서 암호화설정 존재)' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1
if [ `ls -al $conf |grep standalone.xml |wc -l` -eq 0 ]
then
	echo '[ '$conf'/domain.xml]'  >> $CREATE_FILE_RESULT 2>&1
	cat $conf/domain.xml | grep password >> $CREATE_FILE_RESULT 2>&1
else
	echo '[ '$conf'/standalone.xml]'  >> $CREATE_FILE_RESULT 2>&1
	cat $conf/standalone.xml | grep password >> $CREATE_FILE_RESULT 2>&1
fi
echo ''  >> $CREATE_FILE_RESULT 2>&1
echo 'A-08 진단결과 : 수동' >> $CREATE_FILE_RESULT 2>&1
echo 'A-08 END' >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1


echo '============================== 1.9 =============================='
echo 'A-09' >> $CREATE_FILE_RESULT 2>&1
echo ' 1.9 Session Timeout 설정'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo 'Session Timeout 60분 이내로 변경 (Default = 30) ' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1

result1_9='수동'
find $Server -name web.xml >> web_list 2>&1
if [ `cat web_list |wc -l` -eq 0 ]
then
	echo 'web.xml 을 찾을 수 없습니다.' >> $CREATE_FILE_RESULT 2>&1
else
	cat `cat web_list` |grep 'session-timeout' >> $CREATE_FILE_RESULT 2>&1
fi
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '※ Session Timeout 60분 이내로 변경 권고' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo 'A-09 진단결과 : '$result1_9 >> $CREATE_FILE_RESULT 2>&1
echo 'A-09 END' >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1


echo '============================== 2.1 =============================='
echo 'A-10' >> $CREATE_FILE_RESULT 2>&1
echo ' 2.1 보안 패치 적용'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo '주기적으로 보안 패치 적용' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1
$JBoss/bin/standalone.sh -version >> $CREATE_FILE_RESULT 2>&1
result1_10='수동'
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo '※ 영향도 평가 이후 최신 패치 수행 권고' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo 'A-10 진단결과 : '$result1_10 >> $CREATE_FILE_RESULT 2>&1
echo 'A-10 END' >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo '============================== 3.1 =============================='
echo 'A-11' >> $CREATE_FILE_RESULT 2>&1
echo ' 4.1 관리자 콘솔 접근통제'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo '불필요한 경우, JBoss 관리자 콘솔 사용 금지되어 있고, 필요한 경우 Default Port 변경해서 사용하는 경우 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1
echo '[JBoss 관리자 콘솔]'  >> $CREATE_FILE_RESULT 2>&1
if [ `ls -al $conf |grep 'standalone.xml' |wc -l` -eq 0 ]
then
	cat $conf/host.xml |grep 'http-interface security-realm' >> $CREATE_FILE_RESULT 2>&1
	cat $conf/host.xml |grep 'management-http' |grep 'port' >> $CREATE_FILE_RESULT 2>&1
else
	cat $conf/standalone.xml |grep 'http-interface security-realm' >> $CREATE_FILE_RESULT 2>&1
	cat $conf/standalone.xml |grep 'management-http' |grep 'port' >> $CREATE_FILE_RESULT 2>&1
fi
echo ' ' >> $CREATE_FILE_RESULT 2>&1	
echo '※ 불필요한 경우, JBoss 관리자 콘솔 사용 금지하고, 필요한 경우 Default Port 변경 권고' >> $CREATE_FILE_RESULT 2>&1	
echo ' ' >> $CREATE_FILE_RESULT 2>&1	
echo 'A-11 진단결과 : 수동'  >> $CREATE_FILE_RESULT 2>&1
echo 'A-11 END'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo '============================== 4.2 =============================='
echo 'A-12' >> $CREATE_FILE_RESULT 2>&1
echo ' 4.2 관리자 default 계정명 변경'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo '관리자 콘솔 default 값으로 제공된 계정명이 변경 되어 있으면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1
echo '[application-users.properties]'   >> $CREATE_FILE_RESULT 2>&1
cat $conf/application-users.properties >> $CREATE_FILE_RESULT 2>&1
echo ''>> $CREATE_FILE_RESULT 2>&1
echo '[application-roles.properties]'   >> $CREATE_FILE_RESULT 2>&1
cat $conf/application-roles.properties >> $CREATE_FILE_RESULT 2>&1
echo ''>> $CREATE_FILE_RESULT 2>&1
echo '※ 관리자 콘솔 default 계정명을 유추 힘든 계정으로 변경 권고' >> $CREATE_FILE_RESULT 2>&1	
echo ' ' >> $CREATE_FILE_RESULT 2>&1	
echo 'A-12 진단결과 : 수동'  >> $CREATE_FILE_RESULT 2>&1
echo 'A-12 END'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo '============================== 4.3 =============================='
echo 'A-13' >> $CREATE_FILE_RESULT 2>&1
echo ' 4.3 관리자 패스워드 암호정책'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo '패스워드 보안 정책에 맞게 사용중이면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1
echo '※ 패스워드 보안 정책에 맞게 영문/숫자 조합 8자리 이상의 길이로 구성 권고' >> $CREATE_FILE_RESULT 2>&1	
echo ' ' >> $CREATE_FILE_RESULT 2>&1	
echo 'A-13 진단결과 : 수동'  >> $CREATE_FILE_RESULT 2>&1
echo 'A-13 END'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo '============================== 4.4 =============================='
echo 'A-14' >> $CREATE_FILE_RESULT 2>&1
echo ' 4.4 properties 파일 권한 설정'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo '■ 기준' >> $CREATE_FILE_RESULT 2>&1
echo '패스워드 파일 (application-users.properties) 퍼미션이 600 또는 700이면 양호' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo '■ 현황' >> $CREATE_FILE_RESULT 2>&1 
ls -al $conf |grep application >> $CREATE_FILE_RESULT 2>&1
if [ `ls -al $conf |grep application |grep -v '...------' | wc -l` -eq 0 ]
then
	result1_14='양호'
else
	result1_14='취약'
fi
echo '※ 패스워드 파일(application-users.properties) 퍼미션 600 또는 700으로 변경 권고' >> $CREATE_FILE_RESULT 2>&1
echo '' >> $CREATE_FILE_RESULT 2>&1
echo 'A-14 진단결과 : '$result1_14  >> $CREATE_FILE_RESULT 2>&1
echo 'A-14 END'  >> $CREATE_FILE_RESULT 2>&1
echo '===================================================================' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1
echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo '[참고]' >> $CREATE_FILE_RESULT 2>&1
if [ `ls -al $conf |grep standalone.xml |wc -l` -eq 0 ]
then
	echo '[ '$conf'/domain.xml]'  >> $CREATE_FILE_RESULT 2>&1
	cat $conf/domain.xml >> $CREATE_FILE_RESULT 2>&1
else
	echo '[ '$conf'/standalone.xml]'  >> $CREATE_FILE_RESULT 2>&1
	cat $conf/standalone.xml >> $CREATE_FILE_RESULT 2>&1
fi
echo ' ' >> $CREATE_FILE_RESULT 2>&1

echo '****************************** End ***********************************' 
#end script

_HOSTNAME=`hostname`
CREATE_FILE_RESULT_END="JBoss_"${_HOSTNAME}"_"`date +%m%d`"-"`date +%H%M`.txt
echo > $CREATE_FILE_RESULT_END

CREATE_FILE_RESULT=`hostname`"_before_ini_".txt

mv $CREATE_FILE_RESULT $CREATE_FILE_RESULT_END

rm -rf server_dir
rm -Rf dtmp
rm -Rf `hostname`_result_temp2.txt
rm -Rf `hostname`_lsof.txt

echo "☞ 진단작업이 완료되었습니다. 수고하셨습니다!"
