resource "aws_eks_cluster" "eks-cluster" {
  name     = var.eks_cluster_config.name
  role_arn = var.eks_cluster_config.cluster_role
  version  = var.eks_cluster_config.cluster_version

  vpc_config {
    subnet_ids              = var.eks_cluster_config.subnets
    endpoint_private_access = var.eks_cluster_config.endpoint_private_access
    endpoint_public_access  = var.eks_cluster_config.endpoint_public_access
    security_group_ids      = var.eks_cluster_config.sg_ids
  }
  kubernetes_network_config {
    service_ipv4_cidr = var.eks_cluster_config.service_ipv4_cidr
  }
  tags = var.eks_cluster_config.tags
  access_config {
    authentication_mode                         = "API_AND_CONFIG_MAP"
    bootstrap_cluster_creator_admin_permissions = true
  }

}
