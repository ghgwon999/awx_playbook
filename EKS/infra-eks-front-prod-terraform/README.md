# EKS_Terraform

## 모듈 변수 구성
- 각 모듈의 구성의 변수는 map(object({})) 타입으로 구성

## metrics 서버 배포
```bash
cd ./env/main/manifest
kubectl apply -f metrics-server.yml
```

## Karpenter 설치
```bash
#Karpenter
CLUSTER_NAME=eks-front-dev
NODE_POOL=front-dev-node-group-np
NODE_GROUP=eks_front_dev_node_group_private
helm upgrade --install karpenter oci://public.ecr.aws/karpenter/karpenter --version 0.37.0 --namespace kube-system --create-namespace \
--set "serviceAccount.annotations.eks\.amazonaws\.com/role-arn=arn:aws:iam::751732153713:role/eks_front_dev_irsa_karpenter_controller" \
--set settings.clusterName=$CLUSTER_NAME \
--set settings.interruptionQueue=$CLUSTER_NAME \
--set settings.featureGates.drift=false \
--set settings.featureGates.spotToSpotConsolidation=true \
--set dnsPolicy=Default \
--set "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[0].weight=100" \
--set "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[0].preference.matchExpressions[0].key=karpenter.sh/nodepool" \
--set "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[0].preference.matchExpressions[0].operator=In" \
--set "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[0].preference.matchExpressions[0].values[0]=$NODE_POOL" \
--set "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[1].weight=1" \
--set "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[1].preference.matchExpressions[0].key=eks.amazonaws.com/nodegroup" \
--set "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[1].preference.matchExpressions[0].operator=In" \
--set "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[1].preference.matchExpressions[0].values[0]=$NODE_GROUP" \
--set "affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution=null"

kubectl apply -f ./env/main/manifest/PrivateNodePool.yml


```

## AWS LoadBalancer Controller 설치

```bash
#LoadBalacner Controller
CLUSTER_NAME=eks-front-dev
NODE_POOL=front-dev-node-group-np
helm repo add eks https://aws.github.io/eks-charts
helm upgrade --install aws-load-balancer-controller eks/aws-load-balancer-controller --namespace kube-system --create-namespace \
  --set clusterName=$$CLUSTER_NAME \
  --set "serviceAccount.annotations.eks\.amazonaws\.com/role-arn=arn:aws:iam::751732153713:role/eks_front_dev_irsa_aws_load_balancer_controller" \
  --set "affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].key=karpenter.sh/nodepool" \
  --set "affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].operator=In" \
  --set "affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].values[0]=$NODE_POOL" \
  --set "affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].matchExpressions[0].key=app.kubernetes.io/name"\
  --set "affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].matchExpressions[0].operator=In" \
  --set "affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].matchExpressions[0].values[0]=aws-load-balancer-controller" \
  --set "affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].topologyKey=kubernetes.io/hostname"

```


## ArgoCD 설치
```bash
helm repo add argo-cd https://argoproj.github.io/argo-helm
helm upgrade --install argo-cd argo-cd/argo-cd --namespace argo-cd --create-namespace -f argo-custom-values.yaml
kubectl -n argo-cd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d

#argocd cli install
# Download the binary
curl -sSL -o argocd-linux-amd64 https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
sudo install -m 555 argocd-linux-amd64 /usr/local/bin/argocd
rm argocd-linux-amd64
argocd login dev-argo-cd.priviatravel.com:3000 --username admin --password el2E3xR1J8rTLL5r --insecure
argocd account update-password

argocd repo add https://Git주소 --username <계정> --password 로그인키등록

```

## ArgoCD Image Updater 설치
```bash
#argocd image updater
 
helm repo add argo https://argoproj.github.io/argo-helm
helm upgrade --install argocd-image-updater argo/argocd-image-updater  --namespace argo-cd --create-namespace -f argocd-image-updater-custom-value.yml
#configmap/argocd-cm 내 사용자 정의 추가
#		data:
#		 # …
# accounts.image-updater: apiKey
#configmap/argocd-rbac-cm 권한 추가
#data.plicy.csv: |
#p, role:image-updater, applications, get, */*, allow
#p, role:image-updater, applications, update, */*, allow
#g, image-updater, role:image-updater


#argocd 액세스 토큰 생성
argocd account generate-token --account image-updater --id image-updater
kubectl create secret generic argocd-image-updater-secret \
	  --from-literal argocd.token=$YOUR_TOKEN --dry-run -o yaml |
kubectl -n argo-cd apply -f -
#ArgoCD Application Annotations 구성
#argocd-image-updater.argoproj.io/image-list = test-app=751732153713.dkr.ecr.ap-northeast-2.amazonaws.com/infra-team-tools
#argocd-image-updater.argoproj.io/test-app.helm.image-name=deployment.image.name <Helm 변수>
#argocd-image-updater.argoproj.io/test-app.helm.image-tag=deployment.image.tag <Helm 변수>
#argocd-image-updater.argoproj.io/test-app.update-strategy=newest-build
```
## FluentBit Install
```bash
helm upgrade --install aws-for-fluent-bit --namespace kube-system eks/aws-for-fluent-bit
```


## Kubernetes Dashboard Install
```bash
kubectl apply -f kubernetes-dashboard-token.yml -n kubernetes-dashboard

kubectl get secret dashboard-user -n kubernetes-dashboard -o jsonpath={".data.token"} | base64 -d
#해당 값으로 configmap/kubernetes-dashboard-proxy 수정

kubectl apply -f kubernetes-dashboard-proxy.yml -n kubernetes-dashboard

```