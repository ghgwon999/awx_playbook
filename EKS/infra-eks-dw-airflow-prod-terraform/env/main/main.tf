module "vpc" {
  source   = "../../module/vpc"
  for_each = merge(var.vpc_cidr,local.THUM_VPC)
  vpc_config=each.value
}

module "nat_gw" {
  source        = "../../module/nat"
  for_each = merge(var.nat_gw,local.THUM_NAT_GW)
  nat_config=each.value
  depends_on    = [module.vpc, module.subnets]
}

module "subnets" {
  source = "../../module/subnets"
  for_each = merge(var.subnets,local.THUM_SUBNETS)
  subnet_config=each.value
  depends_on = [module.vpc]
}
output "subnets" {
  value = flatten([for subnet_info in values(module.subnets) : subnet_info.subnet_id])
}
module "route_tables" {
  source     = "../../module/route_table"
  for_each = merge(var.route_tables,local.THUM_ROUTE_TABLE)
  route_table_config = each.value
  depends_on = [module.vpc, module.subnets]
}

module "vgw_route" {
  source     = "../../module/vgw_route_propagation"
  for_each = merge(var.vgw_route_config,local.THUM_VGW_ROUTE_ENABLE)
  vgw_route_config = each.value
  depends_on = [module.route_tables]
}
module "iam_role" {
  source             = "../../module/iam_role"
  for_each = merge(var.iam_roles,local.THUM_IAM_ROLE)
  iam_role_config = each.value
  depends_on = [module.iam_policy]
}
output iam_role {
  value = flatten([for iam_roles in module.iam_role : iam_roles.iam_role])
}
module "iam_policy" {
  source = "../../module/iam_policy"
  for_each = merge(var.iam_policies,local.THUM_IAM_POLICY)
  iam_policy_config = each.value
}
output iam_policy {
  value = flatten([for iam_roles in module.iam_role : iam_roles.iam_role])
}
module "security_groups" {
  source = "../../module/security_groups"
  for_each = merge(var.security_group,local.THUM_SECURITY_GROUPS)
  sg_config = each.value
  depends_on = [module.vpc]
}
output eks_node_sg_id {
  value = module.security_groups["thum_eks_node_sg"].id
}
module "k8s_karpenter" {
  source = "../../module/karpenter"
  for_each = merge(var.k8s_karpenter,local.THUM_KARPENTER)
  karpenter_config = each.value
}

module "eks_cluster" {
  source            = "../../module/eks_cluster"
  for_each = merge(var.eks_cluster,local.THUM_EKS_CLUSTER)
  eks_cluster_config = each.value
  depends_on = [
    module.iam_role,
    module.security_groups,
    module.subnets
  ]
}
module "iam_oidc" {
  source = "../../module/iam_oidc"
  for_each = merge(var.iam_oidc,local.THUM_IAM_OIDC)
  iam_oidc_config = each.value
  depends_on = [
  module.eks_cluster]
}
output eks_oidc {
  value = module.eks_cluster["eks_thum_prod"].cluster_oidc
}
module "iam_irsa" {
  source = "../../module/iam_role"
  for_each = merge(var.iam_roles,local.THUM_IAM_ROLE_IRSA)
  iam_role_config = each.value
  depends_on = [
  module.eks_cluster,module.iam_oidc]
}
output iam_irsa {
  value = flatten([for iam_roles in module.iam_irsa : iam_roles.iam_role])
}
module "launch_template" {
  source = "../../module/launch_template"
  for_each = merge(var.launch_template,local.THUM_LAUNCH_TEMPLATES)
  launch_template_config = each.value
  depends_on = [
  module.eks_cluster,module.security_groups
  ]
}
# module "eks_node_group" {
#   source = "../../module/eks_node_groups"
#   for_each = merge(var.eks_node_group,local.THUM_EKS_NODE_GROUP)
#   eks_node_group_config = each.value
#   depends_on = [module.launch_template, module.iam_role,module.subnets,module.eks_cluster]
# }
module "helm_release" {
  source = "../../module/helm"
  for_each = merge(var.helm_release,local.THUM_HELM)
  helm_release_config = each.value
  depends_on = [module.k8s_karpenter]
}
