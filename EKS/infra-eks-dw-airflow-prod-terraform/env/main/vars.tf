variable "thum_name_tag" {
  default = "thum_eks_prod"
}
variable "vpc_cidr" {
  description = "VPC CIDR BLOCK : x.x.x.x/x"
  type = map(object({
    vpc_cidr=string
    tags = any
  }))
  default = {}
}
variable "vgw_route_config" {
  type = map(object({
    vpn_gateway_id=string
    route_table_id=string
  }))
  default = {}
}
variable "nat_gw" {
  type = map(object({
    public_subnet=string
    tags=any
  }))
  default = {}
}
variable "subnets" {
  type=map(object({
    vpc_id=any
    subnet_cidr=any
    subnet_az=any
    is_public=bool
    tags=any
  }))
  default = {}
}
variable "route_tables" {
  type = map(object({
    vpc_id = string
    route = any
    tags = any
    subnets = any
  }))
  default = {}
}
variable "iam_roles"{
  type=map(object({
    name=any
    tags=any
    assume_role_policy = any
    mgd_policies = set(string)
  }))
  default = {}
  }
variable "iam_policies" {
  type = map(object({
    name = string
    policy = any
    description = string
    tags = any
  }))
  default = {}
}
variable "security_group" {
  type = map(object({
    name = string
    description = string
    vpc_id = string
    ingress = any
    egress = any
    tags = any
  }))
  default = {}
}
variable "launch_template" {
  type = map(object({
    name = string
    image_id = string
    instance_type = string
    vpc_security_group_ids = any
    user_data = any
    update_default_version = bool
  }))
  default = {}
}
variable "eks_cluster" {
  type = map(object({
    name = string
    subnets = any
    tags = any
    service_ipv4_cidr = string
    cluster_role = string
    cluster_version = number
    sg_ids= any
    node_group_role = string
    admin_role = string
    endpoint_private_access = bool
    endpoint_public_access = bool
    upload_kubeconfig = string
  }))
  default = {}
}
variable "thum_eks_cluster_addons" {

  type = map(object({
    cluster_name =string
    addon_name = string
    /*
      check addon version
      aws eks describe-addon-versions --kubernetes-version 1.29 --addon-name vpc-cni \ --query 'addons[].addonVersions[].{Version: addonVersion, Defaultversion: compatibilities[0].defaultVersion}' --output table
    */
    addon_version = string
    /*
      NONE = Amazon EKS는 값을 변경하지 않습니다. 업데이트가 실패할 수 있습니다
      OVERWRITE =  Amazon EKS는 변경된 값을 다시 Amazon EKS 기본값으로 덮어씁니다
      PRESERVE = Amazon EKS가 값을 보존합니다. 이 옵션을 선택하는 경우 프로덕션 클러스터에서 추가 기능을 업데이트하기 전에 비프로덕션 클러스터에서 필드 및 값 변경 사항을 테스트하는 것이 좋습니다
    */
    resolve_conflicts_on_create = string
    resolve_conflicts_on_update = string
  }))
  default = {}
}
variable "eks_node_group" {
  type = map(object({
    cluster_name = string
    node_group_name = string
    node_group_role_arn = string
    subnet_ids = any
    scaling_config = any
    launch_template = any
    tags = any
  }))
  default = {}
}
variable "ec2_instance" {
  type = map(object({
    ami = string
    instance_type = string
    iam_instance_profile = string
    vpc_security_group_ids = list(string)
    subnet_id = string
    user_data = any
    tags = any
  }))
  default = {}
}
variable "iam_oidc" {
  type = map(object({
    url = any
    client_id_list = any
    thumbprint_list = any
  }))
  default = {}
}
variable "helm_release" {
  type = map(object({
    chart = any
    name = string
    values = any
    upgrade_install=bool
    set = any
    create_namespace = bool
    version=any
  }))
  default = {}
}
variable "k8s_manifest" {
  type = map(object({
    manifest = any
  }))
  default = {}
}
variable "k8s_karpenter" {
  type = map(object({
    sqs = any
    event_rules = any
    instance_profile = string
  }))
  default = {}
}
#KARPENTER
locals {
  THUM_KARPENTER = {
    thum_karpenter = {
      sqs = {
        name = "eks_thum_prod"
        message_retention_seconds = 300
        sqs_managed_sse_enabled = true
        policy = data.aws_iam_policy_document.karpenter_sqs_policy.json
      }
      event_rules = {
        ScheduledChangeRule = {
          name          = "ScheduledChangeRuleForKarpenter"
          description   = "ScheduledChangeRuleForKarpenter"
          event_pattern = jsonencode(
            {
              "source" : ["aws.health"],
              "detail-type" : ["AWS Health Event"]
            })
        }
        SpotInterruptionRule = {
          name          = "SpotInterruptionRuleForKarpenter"
          description   = "SpotInterruptionRuleForKarpenter"
          event_pattern = jsonencode(
            {
              "source" : ["aws.ec2"],
              "detail-type" : ["EC2 Spot Instance Interruption Warning"]
            })
        }
        RebalanceRule = {
          name          = "RebalanceRuleForKarpenter"
          description   = "RebalanceRuleForKarpenter"
          event_pattern = jsonencode(
            {
              "source" : ["aws.ec2"],
              "detail-type" : ["EC2 Instance Rebalance Recommendation"]
            })
        }
        RebalanceRule = {
          name          = "InstanceStateChangeRuleForKarpenter"
          description   = "InstanceStateChangeRuleForKarpenter"
          event_pattern = jsonencode(
            {
              "source" : ["aws.ec2"],
              "detail-type" : ["EC2 Instance State-change Notification"]
            })
        }
      }
      instance_profile = module.iam_role["thum_node_group_role"].iam_role_name
    }
  }
}
#VPC CIDR
locals {
  THUM_VPC={
    thum_vpc_for_eks = {
      vpc_cidr = "10.80.0.0/16"
      tags= {
        Name  = "${var.thum_name_tag}_vpc",
      }
    }
  }
}
#NAT GW
locals {
  THUM_NAT_GW ={
    thum_nat_gw_a = {
      public_subnet = module.subnets["pub1"].subnet_id
      tags= {
        Name  = "${var.thum_name_tag}_nat_gw_a_zone",
      }
    }
    thum_nat_gw_c = {
      public_subnet = module.subnets["pub2"].subnet_id
      tags= {
        Name  = "${var.thum_name_tag}_nat_gw_c_zone",
      }
    }
  }
}
#SUBNETS
locals {
  THUM_SUBNETS= {
    pub1 = {
      vpc_id      = module.vpc["thum_vpc_for_eks"].vpc_id
      subnet_cidr = "10.80.0.0/24"
      subnet_az   = data.aws_availability_zones.available.names[0]
      is_public   = true
      tags= {
        Name  = "${var.thum_name_tag}_public_a",
      }
    }
    pub2 = {
      vpc_id      = module.vpc["thum_vpc_for_eks"].vpc_id
      subnet_cidr = "10.80.1.0/24"
      subnet_az   = data.aws_availability_zones.available.names[2]
      is_public   = true
      tags= {
        Name  = "${var.thum_name_tag}_public_c",
      }
    }
    pri1 = {
      vpc_id      = module.vpc["thum_vpc_for_eks"].vpc_id
      subnet_cidr = "10.80.2.0/24"
      subnet_az   = data.aws_availability_zones.available.names[0]
      is_public   = false
      tags= {
        Name  = "${var.thum_name_tag}_private_a",
      }
    }
    pri2 = {
      vpc_id      = module.vpc["thum_vpc_for_eks"].vpc_id
      subnet_cidr = "10.80.3.0/24"
      subnet_az   = data.aws_availability_zones.available.names[2]
      is_public   = false
      tags= {
        Name  = "${var.thum_name_tag}_private_c",
      }
    }
  }
}
#ROUTE TABLES
locals {
  THUM_ROUTE_TABLE = {
    thum_public_route_table = {
      vpc_id = module.vpc["thum_vpc_for_eks"].vpc_id
      tags= {
        Name  = "${var.thum_name_tag}_public_rt",
      }
      route =[
        {
          cidr_block = "0.0.0.0/0"
          gateway_id = module.vpc["thum_vpc_for_eks"].igw_id
        },
        #THUM VPC PEERING
        {
          cidr_block = "172.31.0.0/16"
          gateway_id = "pcx-095e7efa91ac01a41"
        },
        #HOT VPC PEERING
        {
          cidr_block = "10.5.0.0/16"
          gateway_id = "pcx-07783ca5aa58f6f15"
        }
      ,
        #COMM VPC PEERING
        {
          cidr_block = "10.10.0.0/16"
          gateway_id = "pcx-01950436ea4ad1d9b"
        }
      ]
      subnets = [module.subnets["pub1"].subnet_id,module.subnets["pub2"].subnet_id]
    }
    thum_private_route_table_a = {
      vpc_id = module.vpc["thum_vpc_for_eks"].vpc_id
      tags= {
        Name  = "${var.thum_name_tag}_private_a_zone_rt",
      }
      route =[
        {
          cidr_block = "0.0.0.0/0"
          gateway_id = module.nat_gw["thum_nat_gw_a"].nat_gw
        },
        #THUM VPC PEERING
        {
          cidr_block = "172.31.0.0/16"
          gateway_id = "pcx-095e7efa91ac01a41"
        },
        #HOT VPC PEERING
        {
          cidr_block = "10.5.0.0/16"
          gateway_id = "pcx-07783ca5aa58f6f15"
        }
      ,
        #COMM VPC PEERING
        {
          cidr_block = "10.10.0.0/16"
          gateway_id = "pcx-01950436ea4ad1d9b"
        }

      ]
      subnets = [module.subnets["pri1"].subnet_id,]
    }
    thum_private_route_table_c = {
      vpc_id = module.vpc["thum_vpc_for_eks"].vpc_id
      tags= {
        Name  = "${var.thum_name_tag}_private_c_zone_rt",
      }
      route =[
        {
          cidr_block = "0.0.0.0/0"
          gateway_id = module.nat_gw["thum_nat_gw_c"].nat_gw
        },
        #THUM VPC PEERING
        {
          cidr_block = "172.31.0.0/16"
          gateway_id = "pcx-095e7efa91ac01a41"
        },
        #HOT VPC PEERING
        {
          cidr_block = "10.5.0.0/16"
          gateway_id = "pcx-07783ca5aa58f6f15"
        }
      ,
        #COMM VPC PEERING
        {
          cidr_block = "10.10.0.0/16"
          gateway_id = "pcx-01950436ea4ad1d9b"
        }
      ]
      subnets = [module.subnets["pri2"].subnet_id]
    }
  }
}
#VGW ROUTE
locals {
  THUM_VGW_ROUTE_ENABLE = {
    thum_public_route_table_vgw={
      vpn_gateway_id="vgw-02aa2312f439af09a"
      route_table_id = module.route_tables["thum_public_route_table"].route_table_id
    }
    thum_private_route_table_a_vgw={
      vpn_gateway_id="vgw-02aa2312f439af09a"
      route_table_id=module.route_tables["thum_private_route_table_a"].route_table_id
    }
    thum_private_route_table_c_vgw={
      vpn_gateway_id="vgw-02aa2312f439af09a"
      route_table_id=module.route_tables["thum_private_route_table_c"].route_table_id
    }
  }
}


#IAM ROLES
locals {
  THUM_IAM_ROLE = {
    thum_cluster_role = {
      name               = "thum_cluster_role"
      tags= {
        Name  = "${var.thum_name_tag}_cluster_role",
      }
      assume_role_policy = data.aws_iam_policy_document.eks_cluster_role.json
      mgd_policies       = [
        "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy",
        "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
      ]
    }
    thum_node_group_role = {
      name               = "thum_node_group_role"
      tags= {
        Name  = "${var.thum_name_tag}_node_group_role",
      }
      assume_role_policy = data.aws_iam_policy_document.eks_node_group_role.json
      mgd_policies       = [
        "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore",
        "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy",
        "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy",
        "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly",
        "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy",
        "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess",
        module.iam_policy["thum_node_group_policy"].policy_arn
      ]
    }
  }
}
#IAM POLICY
locals {
  THUM_IAM_POLICY = {
    thum_node_group_policy = {
      name = "thum_node_group_policy"
      description = "ecr policy for node group"
      policy = templatefile("${path.root}/template/NodeECR_Policy.json",{} )
      tags= {
        Name  = "${var.thum_name_tag}_node_group_policy",
      }
    }
    thum_irsa_elb_controller_policy = {
      name = "thum_irsa_elb_controller_policy"
      description = "irsa for elb controller"
      policy = templatefile("${path.root}/template/AWS_LB_Controller_Policy.json", {})
      tags= {
        Name  = "${var.thum_name_tag}_irsa_elb_controller_policy",
      }
    }
    thum_irsa_karpenter_policy = {
      name = "thum_irsa_karpenter_policy"
      description = "irsa for karpenter controller"
      policy = templatefile("${path.root}/template/KarpenterControllerPolicy.json", { ClusterName = "eks_thum_prod" , KarpenterNodeGroupRoleName = "thum_node_group_role"})
      tags= {
        Name  = "${var.thum_name_tag}_irsa_karpenter_policy",
      }
    }
  }
}
#SECURIT GROUPS
locals {
  THUM_SECURITY_GROUPS = {
    thum_eks_node_sg = {
      name = "thum_eks_node_sg"
      description = "thum_eks_node_sg"
      vpc_id = module.vpc["thum_vpc_for_eks"].vpc_id
      ingress = {
        inbound_tcp = {
          cidr_ipv4   = "0.0.0.0/0"
          from_port   = 0
          ip_protocol = "tcp"
          to_port     = 65535
          description = "inbound_tcp"
        }
        inbound_dns = {
          cidr_ipv4   = "0.0.0.0/0"
          from_port   = 53
          ip_protocol = "udp"
          to_port     = 53
          description = "inbound_dns"
        }
      }
      egress = {
        outbound_any = {
          cidr_ipv4   = "0.0.0.0/0"
          from_port   = 0
          ip_protocol = "tcp"
          to_port     = 65535
          description = "outbound_any"
        }
        outbound_dns = {
          cidr_ipv4   = "0.0.0.0/0"
          from_port   = 0
          ip_protocol = "udp"
          to_port     = 65535
          description = "outbound_dns"
        }
      }
      tags= {
        Name  = "${var.thum_name_tag}_node_sg",
      }
    }
    thum_eks_cluster_sg = {
      name = "thum_eks_cluster_sg"
      description = "thum_eks_cluster_sg"
      vpc_id = module.vpc["thum_vpc_for_eks"].vpc_id
      ingress = {
        inbound_tcp = {
          cidr_ipv4   = "0.0.0.0/0"
          from_port   = 0
          ip_protocol = "tcp"
          to_port     = 65535
          description = "inbound_tcp"
        }
      }
      egress = {
        outbound_any = {
          cidr_ipv4   = "0.0.0.0/0"
          from_port   = 0
          ip_protocol = "tcp"
          to_port     = 65535
          description = "outbound_any"
        }
        outbound_dns = {
          cidr_ipv4   = "0.0.0.0/0"
          from_port   = 53
          ip_protocol = "udp"
          to_port     = 53
          description = "outbound_dns"
        }
      }
      tags= {
        Name  = "${var.thum_name_tag}_cluster_sg",
      }
    }
    thum_eks_ingress_sg = {
      name = "thum_eks_ingress_sg"
      description = "thum_eks_ingress_sg"
      vpc_id = module.vpc["thum_vpc_for_eks"].vpc_id
      ingress = {
        inbound_80 = {
          cidr_ipv4   = "0.0.0.0/0"
          from_port   = 80
          ip_protocol = "tcp"
          to_port     = 80
          description = "inbound_80"
        }
        inbound_443 = {
          cidr_ipv4   = "0.0.0.0/0"
          from_port   = 443
          ip_protocol = "tcp"
          to_port     = 443
          description = "inbound_443"
        }
      }
      egress = {
        outbound_any = {
          cidr_ipv4   = "0.0.0.0/0"
          from_port   = 0
          ip_protocol = "tcp"
          to_port     = 65535
          description = "outbound_any"
        }
      }
      tags= {
        Name  = "${var.thum_name_tag}_ingress_sg",
      }
    }
  }
}

#EKS CLUSTERS
locals {
  THUM_EKS_CLUSTER = {
    eks_thum_prod = {
      name = "eks_thum_prod"
      subnets = [
        module.subnets["pri1"].subnet_id,
        module.subnets["pri2"].subnet_id
      ]
      tags = {
        Name  = "${var.thum_name_tag}_cluster",
      }
      service_ipv4_cidr = "10.100.0.0/16"
      cluster_role = module.iam_role["thum_cluster_role"].iam_role
      cluster_version = 1.29
      sg_ids = [module.security_groups["thum_eks_cluster_sg"].id]
      node_group_role = module.iam_role["thum_node_group_role"].iam_role
      endpoint_private_access = true
      endpoint_public_access = true
    }
  }
}
#IAM OIDC
locals {
  THUM_IAM_OIDC = {
    iam_oidc = {
      url = module.eks_cluster["eks_thum_prod"].cluster_oidc
      client_id_list = ["sts.amazonaws.com"]
      thumbprint_list = [data.tls_certificate.eks_thum_prod_oidc.certificates.0.sha1_fingerprint]
    }
  }
}

#IAM_ROLE_IRSA
locals {
  THUM_IAM_ROLE_IRSA = {
    thum_irsa_aws_load_balancer_controller = {
      name = "thum_irsa_aws_load_balancer_controller"
      tags= {
        Name  = "${var.thum_name_tag}_irsa_aws_load_balancer_controller",
      }
      assume_role_policy = templatefile("${path.root}/template/EKS_IRSA_Trust_Policy.json",{
        OIDC = "${module.eks_cluster["eks_thum_prod"].cluster_oidc_without_url}"
        NAMESPACE = "kube-system"
        SERVICE_ACCOUNT = "aws-load-balancer-controller"
        ACCOUNT_ID="792931648816"
      })
      mgd_policies = [
        module.iam_policy["thum_irsa_elb_controller_policy"].policy_arn
      ]
    }
    thum_irsa_karpenter_controller = {
      name = "thum_irsa_karpenter_controller"
      tags= {
        Name  = "${var.thum_name_tag}_irsa_karpenter_controller",
      }
      assume_role_policy = templatefile("${path.root}/template/EKS_IRSA_Trust_Policy.json",{
        OIDC = "${module.eks_cluster["eks_thum_prod"].cluster_oidc_without_url}"
        NAMESPACE = "kube-system"
        SERVICE_ACCOUNT = "karpenter"
        ACCOUNT_ID="792931648816"
      })
      mgd_policies = [
        module.iam_policy["thum_irsa_karpenter_policy"].policy_arn
      ]
    }
    thum_irsa_efs_csi_driver = {
      name = "thum_irsa_efs_csi_driver"
      tags= {
        Name  = "${var.thum_name_tag}_irsa_efs_csi_driver",
      }
      assume_role_policy = templatefile("${path.root}/template/AWS_EFS_CSI_Driver_Trust_Policy.json",{
        OIDC = "${module.eks_cluster["eks_thum_prod"].cluster_oidc_without_url}"
        NAMESPACE = "kube-system"
        SERVICE_ACCOUNT = "efs-csi-*"
        ACCOUNT_ID="792931648816"
      })
      mgd_policies = [
        "arn:aws:iam::aws:policy/service-role/AmazonEFSCSIDriverPolicy"
      ]
    }
    thum_irsa_ebs_csi_driver = {
      name = "thum_irsa_ebs_csi_driver"
      tags= {
        Name  = "${var.thum_name_tag}_irsa_ebs_csi_driver",
      }
      assume_role_policy = templatefile("${path.root}/template/AWS_EBS_CSI_Driver_Trust_Policy.json",{
        OIDC = "${module.eks_cluster["eks_thum_prod"].cluster_oidc_without_url}"
        NAMESPACE = "kube-system"
        SERVICE_ACCOUNT = "ebs-csi-controller-sa"
        ACCOUNT_ID="792931648816"
      })
      mgd_policies = [
        "arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy"
      ]
    }
  }
}

#LAUNCH TEMPLATES
locals {
  THUM_LAUNCH_TEMPLATES = {
    thum_eks_node_groups_lt = {
      name = "thum_eks_node_groups_lt"
      image_id = "ami-0c970162f407cdfd0"
      instance_type = "t3.medium"
      update_default_version = false
      vpc_security_group_ids = [module.security_groups["thum_eks_node_sg"].id]
      user_data = base64encode(templatefile("${path.module}/user_data/eks_node.sh",
        {
        CLUSTER-NAME = module.eks_cluster["eks_thum_prod"].cluster_name,
        B64-CLUSTER-CA     = module.eks_cluster["eks_thum_prod"].kubeconfig-certificate-authority-data,
        APISERVER-ENDPOINT = module.eks_cluster["eks_thum_prod"].endpoint,
        DNS-CLUSTER-IP = cidrhost(local.THUM_EKS_CLUSTER.eks_thum_prod.service_ipv4_cidr, 10)
        }
      )
      )
    }
  }
}

#EKS NODE GROUP
locals {
  THUM_EKS_NODE_GROUP = {
    thum_node_group_private = {
      cluster_name = module.eks_cluster["eks_thum_prod"].cluster_name
      node_group_name = "thum_node_group_private"
      node_role_arn = module.iam_role["thum_node_group_role"].iam_role
      subnet_ids = [
        module.subnets["pri1"].subnet_id,
        module.subnets["pri2"].subnet_id
      ]
      scaling_config = [
        {
          desired_size = 1
          min_size     = 0
          max_size     = 2
        }
      ]
      launch_template = [
        {
          version = "$Default"
          id = module.launch_template["thum_eks_node_groups_lt"].id
        }
      ]
      tags= {
        Name  = "thum_node_group_private"
      }
    }
  }
}


# #EKS CLUSTER ADDONS
# locals {
#   THUM_EKS_CLUSTER_ADDONS = {
#     THUM_core_dns = {
#       cluster_name = "THUM_cluster_1"
#       addon_name = "coredns"
#       /*
#       check addon version
#       aws eks describe-addon-versions --kubernetes-version 1.26 --addon-name vpc-cni \ --query 'addons[].addonVersions[].{Version: addonVersion, Defaultversion: compatibilities[0].defaultVersion}' --output table
#       */
#       addon_version = "v1.9.3-eksbuild.15"
#       /*
#       NONE = Amazon EKS는 값을 변경하지 않습니다. 업데이트가 실패할 수 있습니다
#       OVERWRITE =  Amazon EKS는 변경된 값을 다시 Amazon EKS 기본값으로 덮어씁니다
#       PRESERVE = Amazon EKS가 값을 보존합니다. 이 옵션을 선택하는 경우 프로덕션 클러스터에서 추가 기능을 업데이트하기 전에 비프로덕션 클러스터에서 필드 및 값 변경 사항을 테스트하는 것이 좋습니다
#       */
#       resolve_conflicts_on_create = "NONE"
#       resolve_conflicts_on_update = "PRESERVE"
#     }
#   }
# }
#HELM RELEASE
locals {
  THUM_HELM = {
    THUM_karpenter_chart = {
      repository = "oci://public.ecr.aws/karpenter"
      values=[]
      chart = "karpenter"
      version=""
      namespace = "kube-system"
      name  = "karpenter"
      upgrade_install=true
      set   = [
        {
          name  = "serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"
          value = "arn:aws:iam::792931648816:role/thum_irsa_karpenter_controller"
        },
        {
          name  = "settings.clusterName"
          value = "eks_thum_prod"
        },
        {
          name  = "settings.interruptionQueue"
          value = "eks_thum_prod"
        },
        {
          name  = "controller.resources.requests.cpu"
          value = "0.5"
        },
        {
          name  = "controller.resources.requests.memory"
          value = "512Mi"
        },
        {
          name  = "controller.resources.limits.cpu"
          value = "0.5"
        },
        {
          name  = "controller.resources.limits.memory"
          value = "512Mi"
        },
        {
          name  = "settings.featureGates.spotToSpotConsolidation"
          value = "true"
        },
        {
          name  = "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[0].weight"
          value = "100"
        },
        {
          name  = "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[0].preference.matchExpressions[0].key"
          value = "karpenter.sh/nodepool"
        },
        {
          name  = "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[0].preference.matchExpressions[0].operator"
          value = "In"
        },
        {
          name  = "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[0].preference.matchExpressions[0].values[0]"
          value = "airflow"
        },
        {
          name  = "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[1].weight"
          value = "1"
        },
        {
          name  = "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[1].preference.matchExpressions[0].key"
          value = "eks.amazonaws.com/nodegroup"
        },
        {
          name  = "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[1].preference.matchExpressions[0].operator"
          value = "In"
        },
        {
          name  = "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[1].preference.matchExpressions[0].values[0]"
          value = "thum_node_group_private"
        },
        {
          name  = "affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution"
          value = "null"
        }
      ]
      create_namespace = true
    }
    THUM_load_balancer_controller_chart = {
      repository = "https://aws.github.io/eks-charts"
      chart = "aws-load-balancer-controller"
      version=""
      values=[]
      namespace = "kube-system"
      name  = "aws-load-balancer-controller"
      upgrade_install=true
      set   = [
        {
          name = "vpcId"
          value = module.vpc["thum_vpc_for_eks"].vpc_id
        },
        {
          name  = "serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"
          value = "arn:aws:iam::792931648816:role/thum_irsa_aws_load_balancer_controller"
        },
        {
          name  = "clusterName"
          value = "eks_thum_prod"
        },
        {
          name  = "affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].key"
          value = "karpenter.sh/nodepool"
        },
        {
          name  = "affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].operator"
          value = "In"
        },
        {
          name  = "affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].values[0]"
          value = "airflow"
        },
        {
          name  = "affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].matchExpressions[0].key"
          value = "app.kubernetes.io/name"
        },
        {
          name  = "affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].matchExpressions[0].operator"
          value = "In"
        },
        {
          name  = "affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].matchExpressions[0].values[0]"
          value = "aws-load-balancer-controller"
        },
        {
          name  = "affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].topologyKey"
          value = "kubernetes.io/hostname"
        }
      ]
      create_namespace = true
    }
  }
}


# #K8S MANIFEST
# locals {
#   THUM_K8S_MANIFEST = {
#     THUM_k8s_metrics = {
#       manifest = file("${path.root}/manifest/metrics-server.yaml")
#     }
#     THUM_karpenter_private_nodepool = {
#       manifest = file("${path.root}/manifest/PrivateNodePool.yml")
#     }
#   }
# }