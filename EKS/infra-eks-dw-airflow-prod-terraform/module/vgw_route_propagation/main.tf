resource "aws_vpn_gateway_route_propagation" "vgw" {
  vpn_gateway_id = var.vgw_route_config.vpn_gateway_id
  route_table_id = var.vgw_route_config.route_table_id
}