apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.deployment.name }}
  labels:
    app: {{ .Values.global.labels.app }}
  namespace: {{ .Values.namespace.name }}
spec:
  replicas: {{ .Values.deployment.replicas }}
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: {{ .Values.deployment.rollingUpdate.maxSurge }}
      maxUnavailable: {{ .Values.deployment.rollingUpdate.maxUnavailable }}
  selector:
    matchLabels:
      app: {{ .Values.global.labels.app }}
  template:
    metadata:
      labels:
        app: {{ .Values.global.labels.app }}
    spec:
      containers:
        - name: {{ .Values.deployment.containerName }}
          image: {{ .Values.deployment.image.name }}:{{ .Values.deployment.image.tag }}
          imagePullPolicy: {{ .Values.deployment.image.pullPolicy }}
          ports:
            - containerPort: {{ .Values.deployment.containerPort }}
          livenessProbe:
            httpGet:
              path: {{ .Values.deployment.livenessProbe.httpGet.path }}
              port: {{ .Values.deployment.livenessProbe.httpGet.port }}
            failureThreshold: {{ .Values.deployment.livenessProbe.failureThreshold }}
            periodSeconds: {{ .Values.deployment.livenessProbe.periodSeconds }}
          readinessProbe:
            httpGet:
              path: {{ .Values.deployment.readinessProbe.httpGet.path }}
              port: {{ .Values.deployment.readinessProbe.httpGet.port }}
            initialDelaySeconds: {{ .Values.deployment.readinessProbe.initialDelaySeconds }}
            periodSeconds: {{ .Values.deployment.readinessProbe.periodSeconds }}
            timeoutSeconds: {{ .Values.deployment.readinessProbe.timeoutSeconds }}
            successThreshold: {{ .Values.deployment.readinessProbe.successThreshold }}
            failureThreshold: {{ .Values.deployment.readinessProbe.failureThreshold }}
          resources:
            requests:
              memory: {{ .Values.deployment.resources.requests.mem }}
              cpu: {{ .Values.deployment.resources.requests.cpu }}
            limits:
              memory: {{ .Values.deployment.resources.limits.mem }}
              cpu: {{ .Values.deployment.resources.limits.cpu }}
          {{- if .Values.deployment.lifecycle }}
          lifecycle:
            {{- with .Values.deployment.lifecycle }}
            {{- toYaml . | nindent 12 }}
            {{- end}}
          {{- end }}
          env:
            - name: HOSTNAME
              value: "0.0.0.0"
        - name: {{ .Values.deployment.sidecarContainerName }}
          image: {{ .Values.deployment.image.sidecarName }}:{{ .Values.deployment.image.sidecarTag }}
          imagePullPolicy: {{ .Values.deployment.image.pullPolicy }}
          ports:
            - containerPort: {{ .Values.deployment.sidecarContainerPort }}
          livenessProbe:
            httpGet:
              path: {{ .Values.deployment.sidecarLivenessProbe.httpGet.path }}
              port: {{ .Values.deployment.sidecarLivenessProbe.httpGet.port }}
            failureThreshold: {{ .Values.deployment.sidecarLivenessProbe.failureThreshold }}
            periodSeconds: {{ .Values.deployment.sidecarLivenessProbe.periodSeconds }}
          readinessProbe:
            httpGet:
              path: {{ .Values.deployment.sidecarReadinessProbe.httpGet.path }}
              port: {{ .Values.deployment.sidecarReadinessProbe.httpGet.port }}
            initialDelaySeconds: {{ .Values.deployment.sidecarReadinessProbe.initialDelaySeconds }}
            periodSeconds: {{ .Values.deployment.sidecarReadinessProbe.periodSeconds }}
            timeoutSeconds: {{ .Values.deployment.sidecarReadinessProbe.timeoutSeconds }}
            successThreshold: {{ .Values.deployment.sidecarReadinessProbe.successThreshold }}
            failureThreshold: {{ .Values.deployment.sidecarReadinessProbe.failureThreshold }}
          resources:
            requests:
              memory: {{ .Values.deployment.sidecarResources.requests.mem }}
              cpu: {{ .Values.deployment.sidecarResources.requests.cpu }}
            limits:
              memory: {{ .Values.deployment.sidecarResources.limits.mem }}
              cpu: {{ .Values.deployment.sidecarResources.limits.cpu }}
          {{- if .Values.deployment.sidecarLifecycle }}
          lifecycle:
            {{- with .Values.deployment.sidecarLifecycle }}
            {{- toYaml . | nindent 12 }}
            {{- end}}
          {{- end }}
      terminationGracePeriodSeconds: 60
      affinity:
        {{- with .Values.deployment.affinity }}
        {{- toYaml . | nindent 8 }}
        {{- end}}