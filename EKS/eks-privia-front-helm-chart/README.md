# 프리비아 프론트 helm 차트 #

### 차트 구조 ###

- dev-chart
```text
  - common
  - flight
  - package
```
- stg-chart
```text 
  - common
```
- qa-chart
```text
  - package
```

- prod-chart
```text
  - common
```
## ArgoCD
* DEV/STG : https://dev-argocd.tidesquare.com/
* PRD : https://argocd.tidesquare.com/

## Kubernetes-Dashboard
* DEV/STG : https://eks-front-dev-dashboard.tidesquare.com/
* PROD : https://eks-front-prod-dashboard.tidesquare.com/

## Prometheus
* DEV/STG : https://dev-prometheus.tidesquare.com:9090/
* PROD : https://dev-prometheus.tidesquare.com:9090/

## Prometheus Alert
* NodeNotReady
* ContainerOOM
* MemoryPressure
* DiskPressure 
* OutOfDisk
* PodCrashLooping
* PodNotHealthy
* HPAScalingAbility
* ReadinessProbeFailed
* StatefulSetDown




## Grafana
* http://monitor.tidesquare.com/grafana/

### Main 차트
```text
메인 차트는 Namespace 생성 유무로 지정
```
- dev-chart : common
- stg-chart : common
- qa-chart : package
- prod-chart : common
