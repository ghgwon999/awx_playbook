# EKS LAB

## 사용법

### 환경 구성
- 인프라 테스트 계정 CodeBuild eks-lab 선택
- Start build with overrides 선택
- Buildspec name : env/main/buildspec.yml
- Start Build

### 환경 삭제
- EKS 내 생성한 리소스 삭제 (e.g. ingress )
- 인프라 테스트 계정 CodeBuild eks-lab 선택
- Start build with overrides 선택
- Buildspec name : env/main/buildspec_destroy.yml
- Start Build



## kubectl 환경 구성 (CloudShell)


```text
1. CloudShell 접속


2. 자격증명 설정
$aws eks update-kube-config --region ap-northeast-2 --name eks-lab

3. k9s 설치
$curl -sL https://github.com/derailed/k9s/releases/download/v0.25.18/k9s_Linux_x86_64.tar.gz | sudo tar \
  xfz - -C /usr/local/bin k9s


4. helm 설치
$ curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
$ chmod 700 get_helm.sh
$ ./get_helm.sh

5. Karpenter 노드풀 생성
$kubectl apply -f NodePool.yml 

6. kube-ops-view ALB 생성
$kubectl apply -f kube-ops-view.yml -n kube-system
```

#### Karpenter NodePool.yml (spot)
```yaml
apiVersion: karpenter.sh/v1beta1
kind: NodePool
metadata:
  name: system
spec:
  template:
    spec:
      kubelet:
        clusterDNS: ["10.200.0.10"]
      requirements:
        - key: karpenter.sh/capacity-type
          operator: In
          values: ["spot"]
        - key: karpenter.k8s.aws/instance-category
          operator: In
          values: ["t"]
        - key: karpenter.k8s.aws/instance-cpu
          operator: In
          values: ["2"]
        - key: kubernetes.io/arch
          operator: In
          values: ["arm64"]
      nodeClassRef:
        apiVersion: karpenter.k8s.aws/v1beta1
        kind: EC2NodeClass
        name: system
  limits:
    cpu: 1000
  disruption:
    consolidationPolicy: WhenUnderutilized
    expireAfter: Never
---
apiVersion: karpenter.k8s.aws/v1beta1
kind: EC2NodeClass
metadata:
  name: system
spec:
  amiFamily: AL2 # Amazon Linux 2
  userData: |
    #!/bin/bash
    Content-Type: multipart/mixed; boundary="//"
    MIME-Version: 1.0
    --//
    Content-Type: text/cloud-config; charset="us-ascii"
    MIME-Version: 1.0
    Content-Transfer-Encoding: 7bit
    Content-Disposition: attachment; filename="cloud-config.txt"
    #cloud-config
    cloud_final_modules:
    - [scripts-user, always]
    --//
    Content-Type: text/x-shellscript; charset="us-ascii"
    MIME-Version: 1.0
    Content-Transfer-Encoding: 7bit
    Content-Disposition: attachment; filename="userdata.txt"
    #!/bin/bash
    TOKEN=`curl -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600"` \
    && curl -H "X-aws-ec2-metadata-token: $TOKEN" http://169.254.169.254/latest/meta-data/   
    IP=$(curl -H "X-aws-ec2-metadata-token: $TOKEN" http://169.254.169.254/latest/meta-data/local-ipv4 | cut -d '.' -f 1,2,3,4 | sed 's/\./-/g')
    HOST=eks-lab-system-node
    INSTANCE_ID=$(curl -H "X-aws-ec2-metadata-token: $TOKEN" http://169.254.169.254/latest/meta-data/instance-id)
    hostnamectl set-hostname $HOST-$IP --static
    echo "hostname changed to $HOST-$IP"
    su ec2-user -c '/bin/aws ec2 create-tags --resources '$INSTANCE_ID' --tags Key=\"Name\",Value=$(hostname) --region ap-northeast-2'
  role: "eks_lab_node_group_role" # replace with your karpenter node role
  subnetSelectorTerms:
    - tags:
        Name: "poc-subnet-private*"
  securityGroupSelectorTerms:
    - tags:
        Name: "eks_lab_eks_node_sg"
  blockDeviceMappings:
    - deviceName: /dev/xvda
      ebs:
        volumeSize: 30Gi
        volumeType: gp3
        encrypted: true
---
apiVersion: karpenter.sh/v1beta1
kind: NodePool
metadata:
  name: eks-lab
spec:
  template:
    spec:
      kubelet:
        clusterDNS: ["10.200.0.10"]
      requirements:
        - key: karpenter.sh/capacity-type
          operator: In
          values: ["spot"]
        - key: karpenter.k8s.aws/instance-category
          operator: In
          values: ["t"]
        - key: karpenter.k8s.aws/instance-cpu
          operator: In
          values: ["2"]
        - key: kubernetes.io/arch
          operator: In
          values: ["amd64"]
      nodeClassRef:
        apiVersion: karpenter.k8s.aws/v1beta1
        kind: EC2NodeClass
        name: eks-lab
  limits:
    cpu: 1000
  disruption:
    consolidationPolicy: WhenUnderutilized
    expireAfter: Never
---
apiVersion: karpenter.k8s.aws/v1beta1
kind: EC2NodeClass
metadata:
  name: eks-lab
spec:
  amiFamily: AL2 # Amazon Linux 2
  userData: |
    #!/bin/bash
    Content-Type: multipart/mixed; boundary="//"
    MIME-Version: 1.0
    --//
    Content-Type: text/cloud-config; charset="us-ascii"
    MIME-Version: 1.0
    Content-Transfer-Encoding: 7bit
    Content-Disposition: attachment; filename="cloud-config.txt"
    #cloud-config
    cloud_final_modules:
    - [scripts-user, always]
    --//
    Content-Type: text/x-shellscript; charset="us-ascii"
    MIME-Version: 1.0
    Content-Transfer-Encoding: 7bit
    Content-Disposition: attachment; filename="userdata.txt"
    #!/bin/bash
    TOKEN=`curl -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600"` \
    && curl -H "X-aws-ec2-metadata-token: $TOKEN" http://169.254.169.254/latest/meta-data/   
    IP=$(curl -H "X-aws-ec2-metadata-token: $TOKEN" http://169.254.169.254/latest/meta-data/local-ipv4 | cut -d '.' -f 1,2,3,4 | sed 's/\./-/g')
    HOST=eks-lab-app-node
    INSTANCE_ID=$(curl -H "X-aws-ec2-metadata-token: $TOKEN" http://169.254.169.254/latest/meta-data/instance-id)
    hostnamectl set-hostname $HOST-$IP --static
    echo "hostname changed to $HOST-$IP"
    su ec2-user -c '/bin/aws ec2 create-tags --resources '$INSTANCE_ID' --tags Key=\"Name\",Value=$(hostname) --region ap-northeast-2'
  role: "eks_lab_node_group_role" # replace with your karpenter node role
  subnetSelectorTerms:
    - tags:
        Name: "poc-subnet-private*"
  securityGroupSelectorTerms:
    - tags:
        Name: "eks_lab_eks_node_sg"
  blockDeviceMappings:
    - deviceName: /dev/xvda
      ebs:
        volumeSize: 30Gi
        volumeType: gp3
        encrypted: true
```


### kube-ops-view ingress.yaml

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    alb.ingress.kubernetes.io/actions.kube-ops: |
      {"type":"forward","forwardConfig":{"targetGroups":[{"serviceName":"kube-ops-view","servicePort":"80"}]}}
    alb.ingress.kubernetes.io/healthcheck-path: /
    alb.ingress.kubernetes.io/healthcheck-protocol: HTTP
    alb.ingress.kubernetes.io/target-type: ip
    alb.ingress.kubernetes.io/scheme: internet-facing
    alb.ingress.kubernetes.io/backend-protocol: HTTP
    alb.ingress.kubernetes.io/backend-protocol-version: HTTP1
    alb.ingress.kubernetes.io/listen-ports: '[{"HTTP":80}]'
    alb.ingress.kubernetes.io/subnets: poc-subnet-public2-ap-northeast-2b, poc-subnet-public1-ap-northeast-2a
    alb.ingress.kubernetes.io/load-balancer-name: eks-lab-alb
    alb.ingress.kubernetes.io/group.name: eks-lab-alb
    alb.ingress.kubernetes.io/security-groups: eks_lab_eks_internal_ingress_sg
    alb.ingress.kubernetes.io/target-group-attributes: deregistration_delay.timeout_seconds=10
  name: kube-ops-view-ing
  namespace: kube-system
spec:
  ingressClassName: alb
  rules:
  - http:
      paths:
      - backend:
          service:
            name: kube-ops
            port:
              name: use-annotation
        pathType: ImplementationSpecific

```


#### Argocd 초기 패스워드
```
kubectl -n argo-cd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d

#argocd 액세스 토큰 생성
argocd account generate-token --account image-updater --id image-updater
kubectl create secret generic argocd-image-updater-secret \
	  --from-literal argocd.token=$YOUR_TOKEN --dry-run -o yaml |
kubectl -n argo-cd apply -f -
#ArgoCD Application Annotations 구성
#argocd-image-updater.argoproj.io/image-list = test-app=752478543649.dkr.ecr.ap-northeast-2.amazonaws.com/infra-team-tools
#argocd-image-updater.argoproj.io/test-app.helm.image-name=deployment.image.name <Helm 변수>
#argocd-image-updater.argoproj.io/test-app.helm.image-tag=deployment.image.tag <Helm 변수>
#argocd-image-updater.argoproj.io/test-app.update-strategy=newest-build
```

