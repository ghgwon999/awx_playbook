variable "account_id" {
  default = "752478543649"
}
variable "iam_roles"{
  type=map(object({
    name=any
    tags=any
    assume_role_policy = any
    mgd_policies = set(string)
  }))
  default = {}
  }
variable "iam_policies" {
  type = map(object({
    name = string
    policy = any
    description = string
    tags = any
  }))
  default = {}
}
variable "security_group" {
  type = map(object({
    name = string
    description = string
    vpc_id = string
    ingress = any
    egress = any
    tags = any
  }))
  default = {}
}
variable "launch_template" {
  type = map(object({
    name = string
    image_id = string
    instance_type = string
    vpc_security_group_ids = any
    user_data = any
    update_default_version = bool
  }))
  default = {}
}
variable "eks_cluster" {
  type = map(object({
    name = string
    subnets = any
    tags = any
    service_ipv4_cidr = string
    cluster_role = string
    cluster_version = number
    sg_ids= any
    node_group_role = string
    admin_role = string
    endpoint_private_access = bool
    endpoint_public_access = bool
    upload_kubeconfig = string
  }))
  default = {}
}
variable "eks_cluster_addons" {
  type = map(object({
    cluster_name =string
    addon_name = string
    /*
      check addon version
      aws eks describe-addon-versions --kubernetes-version 1.29 --addon-name vpc-cni \ --query 'addons[].addonVersions[].{Version: addonVersion, Defaultversion: compatibilities[0].defaultVersion}' --output table
    */
    addon_version = string
    /*
      NONE = Amazon EKS는 값을 변경하지 않습니다. 업데이트가 실패할 수 있습니다
      OVERWRITE =  Amazon EKS는 변경된 값을 다시 Amazon EKS 기본값으로 덮어씁니다
      PRESERVE = Amazon EKS가 값을 보존합니다. 이 옵션을 선택하는 경우 프로덕션 클러스터에서 추가 기능을 업데이트하기 전에 비프로덕션 클러스터에서 필드 및 값 변경 사항을 테스트하는 것이 좋습니다
    */
    resolve_conflicts_on_create = string
    resolve_conflicts_on_update = string
  }))
  default = {}
}
variable "eks_node_group" {
  type = map(object({
    cluster_name = string
    node_group_name = string
    node_group_role_arn = string
    subnet_ids = any
    scaling_config = any
    launch_template = any
    tags = any
  }))
  default = {}
}
variable "iam_oidc" {
  type = map(object({
    url = any
    client_id_list = any
    thumbprint_list = any
  }))
  default = {}
}
variable "helm_release" {
  type = map(object({
    chart = any
    name = string
    values = any
    version = any
    upgrade_install=bool
    set = any
    create_namespace = bool
  }))
  default = {}
}
variable "k8s_manifest" {
  type = map(object({
    manifest = any
  }))
  default = {}
}
variable "k8s_karpenter" {
  type = map(object({
    sqs = any
    event_rules = any
    instance_profile = string
  }))
  default = {}
}

variable "eks_lab_cluster_vpc_id" {
  default = "vpc-0f526a4d7c4ef8f5e"
}
variable "eks_lab_cluster_name" {
  default = "eks-lab"
}
variable "eks_lab_pri_subnet_a_zone" {
  default = "subnet-0da62124ac01ad1ca"

}
variable "eks_lab_pri_subnet_c_zone" {
  default = "subnet-0b3e02382636c85de"
}
variable "eks_lab_node_ami" {
  default = "ami-0d98ffebc6ff44bdb"
  description = "1.30"
}

#KARPENTER
locals {
  eks_lab_KARPENTER = {
    eks-lab = {
      sqs = {
        name = "eks-lab"
        message_retention_seconds = 300
        sqs_managed_sse_enabled = true
        policy = data.aws_iam_policy_document.karpenter_sqs_policy.json
      }
      event_rules = {
        ScheduledChangeRule = {
          name          = "eks-lab-ScheduledChangeRule"
          description   = "eks-lab-ScheduledChangeRule"
          event_pattern = jsonencode(
            {
              "source" : ["aws.health"],
              "detail-type" : ["AWS Health Event"]
            })
        }
        SpotInterruptionRule = {
          name          = "eks-lab-SpotInterruptionRule"
          description   = "eks-lab-SpotInterruptionRule"
          event_pattern = jsonencode(
            {
              "source" : ["aws.ec2"],
              "detail-type" : ["EC2 Spot Instance Interruption Warning"]
            })
        }
        RebalanceRule = {
          name          = "eks-lab-RebalanceRule"
          description   = "eks-lab-RebalanceRule"
          event_pattern = jsonencode(
            {
              "source" : ["aws.ec2"],
              "detail-type" : ["EC2 Instance Rebalance Recommendation"]
            })
        }
        InstanceStateChangeRule = {
          name          = "eks-lab-InstanceStateChangeRule"
          description   = "eks-lab-InstanceStateChangeRule"
          event_pattern = jsonencode(
            {
              "source" : ["aws.ec2"],
              "detail-type" : ["EC2 Instance State-change Notification"]
            })
        }
      }
      instance_profile = module.iam_role["eks_lab_node_group_role"].iam_role_name
    }
  }
}
#IAM ROLES
locals {
  eks_lab_IAM_ROLE = {
    eks_lab_cluster_role = {
      name               = "eks_lab_cluster_role"
      tags = {
        Name = "eks_lab_cluster_role"
      }
      assume_role_policy = data.aws_iam_policy_document.eks_cluster_role.json
      mgd_policies       = [
        "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy",
        "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
      ]
    }
    eks_lab_node_group_role = {
      name               = "eks_lab_node_group_role"
      tags = {
        Name = "eks_lab_node_group_role"
      }
      assume_role_policy = data.aws_iam_policy_document.eks_node_group_role.json
      mgd_policies       = [
        "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore",
        "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy",
        "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy",
        "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly",
        "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy",
        "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess",
        module.iam_policy["eks_lab_node_group_policy"].policy_arn
      ]
    }

  }
}
#IAM POLICY
locals {
  eks_lab_IAM_POLICY = {
    eks_lab_node_group_policy = {
      name = "eks_lab_node_group_policy"
      description = "ecr policy for node group"
      policy = templatefile("${path.root}/template/NodeECR_Policy.json",{} )
      tags = {
        Name = "eks_lab_node_group_policy"
      }
    }
    eks_lab_irsa_elb_controller_policy = {
      name = "eks_lab_irsa_elb_controller_policy"
      description = "irsa for elb controller"
      policy = templatefile("${path.root}/template/AWS_LB_Controller_Policy.json", {})
      tags = {
        Name = "eks_lab_irsa_elb_controller_policy"
      }
    }
    eks_lab_irsa_karpenter_policy = {
      name = "eks_lab_irsa_karpenter_policy"
      description = "irsa for karpenter controller"
      policy = templatefile("${path.root}/template/KarpenterControllerPolicy.json", { ClusterName = "eks-lab" , KarpenterNodeGroupRoleName = "eks_lab_node_group_role"})
      tags = {
        Name = "eks_lab_irsa_karpenter_policy"
      }
    }
    eks_lab_irsa_nth_policy = {
      name = "eks_lab_irsa_nth_policy"
      description = "irsa for nth controller"
      policy = templatefile("${path.root}/template/NTHPolicy.json", { ClusterName = "eks-lab" , SqsName = "eks-lab"})
      tags = {
        Name = "eks_lab_irsa_nth_policy"
      }
    }

  }
}
#IAM_ROLE_IRSA
locals {
  eks_lab_IAM_ROLE_IRSA = {
    eks_lab_irsa_aws_load_balancer_controller = {
      name = "eks_lab_irsa_aws_load_balancer_controller"
      tags = {
        Name  = "eks_lab_irsa_aws_load_balancer_controller"
      }
      assume_role_policy = templatefile("${path.root}/template/EKS_IRSA_Trust_Policy.json",{
        ACCOUNT_ID="752478543649"
        OIDC = "${module.eks_cluster["eks-lab"].cluster_oidc_without_url}"
        NAMESPACE = "kube-system"
        SERVICE_ACCOUNT = "aws-load-balancer-controller"
      })
      mgd_policies = [
        module.iam_policy["eks_lab_irsa_elb_controller_policy"].policy_arn
      ]
    }
    eks_lab_irsa_karpenter_controller = {
      name = "eks_lab_irsa_karpenter_controller"
      tags = {
        Name  = "eks_lab_irsa_karpenter_controller"
      }
      assume_role_policy = templatefile("${path.root}/template/EKS_IRSA_Trust_Policy.json",{
        ACCOUNT_ID="752478543649"
        OIDC = "${module.eks_cluster["eks-lab"].cluster_oidc_without_url}"
        NAMESPACE = "kube-system"
        SERVICE_ACCOUNT = "karpenter"
      })
      mgd_policies = [
        module.iam_policy["eks_lab_irsa_karpenter_policy"].policy_arn
      ]
    }
    eks_lab_irsa_nth = {
      name = "eks_lab_irsa_nth"
      tags = {
        Name  = "eks_lab_irsa_nth"
      }
      assume_role_policy = templatefile("${path.root}/template/EKS_IRSA_Trust_Policy.json",{
        ACCOUNT_ID="752478543649"
        OIDC = "${module.eks_cluster["eks-lab"].cluster_oidc_without_url}"
        NAMESPACE = "kube-system"
        SERVICE_ACCOUNT = "aws-node-termination-handler"
      })
      mgd_policies = [
        module.iam_policy["eks_lab_irsa_nth_policy"].policy_arn
      ]
    }
    eks_lab_irsa_efs_csi_driver = {
      name = "eks_lab_irsa_efs_csi_driver"
      tags = {
        Name  = "eks_lab_irsa_efs_csi_driver"
      }
      assume_role_policy = templatefile("${path.root}/template/AWS_EFS_CSI_Driver_Trust_Policy.json",{
        ACCOUNT_ID="752478543649"
        OIDC = "${module.eks_cluster["eks-lab"].cluster_oidc_without_url}"
        NAMESPACE = "kube-system"
        SERVICE_ACCOUNT = "efs-csi-*"
      })
      mgd_policies = [
        "arn:aws:iam::aws:policy/service-role/AmazonEFSCSIDriverPolicy"
      ]
    }
    eks_lab_irsa_ebs_csi_driver = {
      name = "eks_lab_irsa_ebs_csi_driver"
      tags = {
        Name  = "eks_lab_irsa_ebs_csi_driver"
      }
      assume_role_policy = templatefile("${path.root}/template/AWS_EBS_CSI_Driver_Trust_Policy.json",{
        ACCOUNT_ID="752478543649"
        OIDC = "${module.eks_cluster["eks-lab"].cluster_oidc_without_url}"
        NAMESPACE = "kube-system"
        SERVICE_ACCOUNT = "ebs-csi-controller-sa"
      })
      mgd_policies = [
        "arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy"
      ]
    }
  }
}

#SECURIT GROUPS
locals {
  PROD_SECURITY_GROUPS = {
    eks_lab_eks_node_sg = {
      name = "eks_lab_eks_node_sg"
      description = "eks_lab_eks_node_sg"
      vpc_id = var.eks_lab_cluster_vpc_id
      ingress = {
        inbound_inter_vpc = {
          cidr_ipv4   = "10.33.0.0/16"
          from_port   = 0
          ip_protocol = "tcp"
          to_port     = 65535
          description = "inter vpc"
        }

        inbound_dns = {
          cidr_ipv4   = "10.33.0.0/16"
          from_port   = 53
          ip_protocol = "udp"
          to_port     = 53
          description = "inbound dns"
        }
      }
      egress = {
        outbound_any = {
          cidr_ipv4   = "0.0.0.0/0"
          from_port   = 0
          ip_protocol = "tcp"
          to_port     = 65535
          description = "outbound_any"
        }
        outbound_dns = {
          cidr_ipv4   = "0.0.0.0/0"
          from_port   = 53
          ip_protocol = "udp"
          to_port     = 53
          description = "outbound_dns"
        }
      }
      tags= {
        Name  = "eks_lab_eks_node_sg"
      }
    }
    eks_lab_eks_cluster_sg = {
      name = "eks_lab_eks_cluster_sg"
      description = "eks_lab_eks_cluster_sg"
      vpc_id = var.eks_lab_cluster_vpc_id
      ingress = {
        inbound_443 = {
          cidr_ipv4   = "10.33.0.0/16"
          from_port   = 443
          ip_protocol = "tcp"
          to_port     = 443
          description = "inbound_443_inter_vpc"
        }
      }
      egress = {
        outbound_any = {
          cidr_ipv4   = "0.0.0.0/0"
          from_port   = 0
          ip_protocol = "tcp"
          to_port     = 65535
          description = "outbound_any"
        }
        outbound_dns = {
          cidr_ipv4   = "0.0.0.0/0"
          from_port   = 53
          ip_protocol = "udp"
          to_port     = 53
          description = "outbound_dns"
        }
      }
      tags= {
        Name  = "eks_lab_eks_cluster_sg"
      }
    }
    eks_lab_eks_ingress_sg = {
      name = "eks_lab_eks_ingress_sg"
      description = "eks_lab_eks_ingress_sg"
      vpc_id = var.eks_lab_cluster_vpc_id
      ingress = {
        inbound_argo_cli = {
          cidr_ipv4   = "52.79.202.93/32"
          from_port   = 3000
          ip_protocol = "tcp"
          to_port     = 3000
          description = "inbound_argo_cli"
        }
        inbound_80 = {
          cidr_ipv4   = "1.214.218.218/32"
          from_port   = 80
          ip_protocol = "tcp"
          to_port     = 80
          description = "inbound_80"
        }
        inbound_443 = {
          cidr_ipv4   = "1.214.218.218/32"
          from_port   = 443
          ip_protocol = "tcp"
          to_port     = 443
          description = "inbound_443"
        }
        inbound_443_gy = {
          cidr_ipv4   = "211.169.223.130/32"
          from_port   = 443
          ip_protocol = "tcp"
          to_port     = 443
          description = "inbound_443_gy"
        }
        inbound_443_gy2 = {
          cidr_ipv4   = "210.183.177.209/32"
          from_port   = 443
          ip_protocol = "tcp"
          to_port     = 443
          description = "inbound_443_gy2"
        }
        inbound_443_wifi = {
          cidr_ipv4   = "211.244.148.22/32"
          from_port   = 443
          ip_protocol = "tcp"
          to_port     = 443
          description = "inbound_443_wifi"
        }
        inbound_3000 = {
          cidr_ipv4   = "1.214.218.218/32"
          from_port   = 3000
          ip_protocol = "tcp"
          to_port     = 3000
          description = "inbound_argo_cd"
        }
      }
      egress = {
        outbound_any = {
          cidr_ipv4   = "0.0.0.0/0"
          from_port   = 0
          ip_protocol = "tcp"
          to_port     = 65535
          description = "outbound_any"
        }
      }
      tags= {
        Name  = "eks_lab_eks_ingress_sg"
      }
    }
    eks_lab_eks_internal_ingress_sg = {
      name = "eks_lab_eks_internal_ingress_sg"
      description = "eks_lab_eks_internal_ingress_sg"
      vpc_id = var.eks_lab_cluster_vpc_id
      ingress = {
        inbound_all = {
          cidr_ipv4   = "0.0.0.0/0"
          from_port   = 0
          ip_protocol = "tcp"
          to_port     = 65535
          description = "inbound_all"
        }
      }
      egress = {
        outbound_any = {
          cidr_ipv4   = "0.0.0.0/0"
          from_port   = 0
          ip_protocol = "tcp"
          to_port     = 65535
          description = "outbound_any"
        }
      }
      tags= {
        Name  = "eks_lab_eks_internal_ingress_sg"
      }
    }
  }
}

#LAUNCH TEMPLATES
locals {
  eks_lab_LAUNCH_TEMPLATES = {
    eks_lab_eks_node_groups_lt = {
      name = "eks_lab-eks-ng-lt"
      image_id = var.eks_lab_node_ami
      instance_type = "t3.medium"
      update_default_version = false
      vpc_security_group_ids = [module.security_groups["eks_lab_eks_node_sg"].id]
      user_data = base64encode(templatefile("${path.module}/user_data/eks_node.sh",
        {
        CLUSTER-NAME = module.eks_cluster["eks-lab"].cluster_name,
        B64-CLUSTER-CA     = module.eks_cluster["eks-lab"].kubeconfig-certificate-authority-data,
        APISERVER-ENDPOINT = module.eks_cluster["eks-lab"].endpoint,
        DNS-CLUSTER-IP = cidrhost(local.eks_lab_EKS_CLUSTER.eks-lab.service_ipv4_cidr, 10)
        }
      )
      )
    }
  }
}
#EKS CLUSTERS
locals {
  eks_lab_EKS_CLUSTER = {
    eks-lab = {
      name = "eks-lab"
      subnets = [
        var.eks_lab_pri_subnet_a_zone,
        var.eks_lab_pri_subnet_c_zone
      ]
      tags = {
        Name = "eks-lab"
      }
      service_ipv4_cidr = "10.200.0.0/16"
      cluster_role = module.iam_role["eks_lab_cluster_role"].iam_role
      cluster_version = "1.30"
      sg_ids = [module.security_groups["eks_lab_eks_cluster_sg"].id]
      node_group_role = module.iam_role["eks_lab_node_group_role"].iam_role
      admin_role = module.iam_role["eks_lab_node_group_role"].iam_role
      endpoint_private_access = true
      endpoint_public_access = true
    }
  }
}
#EKS NODE GROUP
locals {
  eks_lab_EKS_NODE_GROUP = {
    eks_lab_node_group_private = {
      cluster_name = module.eks_cluster["eks-lab"].cluster_name
      node_group_name = "eks_lab_node_group_private"
      node_role_arn = module.iam_role["eks_lab_node_group_role"].iam_role
      subnet_ids = [
        var.eks_lab_pri_subnet_a_zone,
        var.eks_lab_pri_subnet_c_zone
      ]
      scaling_config = [
        {
          desired_size = 1
          min_size     = 0
          max_size     = 2
        }
      ]
      launch_template = [
        {
          version = "$Default"
          id = module.launch_template["eks_lab_eks_node_groups_lt"].id
        }
      ]
      tags= {
        Name  = "eks_lab_node_group_private"
      }
    }
  }
}

#IAM OIDC
locals {
  eks_lab_IAM_OIDC = {
    iam_oidc = {
      url = module.eks_cluster["eks-lab"].cluster_oidc
      client_id_list = ["sts.amazonaws.com"]
      thumbprint_list = [data.tls_certificate.prod_eks_cluster_1_oidc.certificates.0.sha1_fingerprint]
    }
  }
}

#EKS CLUSTER ADDONS
locals {
  PROD_EKS_CLUSTER_ADDONS = {
    prod_core_dns = {
      cluster_name = "eks-lab"
      addon_name = "coredns"
      addon_version = "v1.9.3-eksbuild.15"
      resolve_conflicts_on_create = "NONE"
      resolve_conflicts_on_update = "PRESERVE"
    }
  }
}
#HELM RELEASE
locals {
  LAB_HELM = {
    metrics-server={
      repository = "https://kubernetes-sigs.github.io/metrics-server"
      upgrade_install=true
      version="3.12.0"
      values=[]
      chart = "metrics-server"
      namespace = "kube-system"
      name  = "metrics-server"
      set   = []
      create_namespace = true
    }
    prod_karpenter_chart = {
      repository = "oci://public.ecr.aws/karpenter"
      upgrade_install=true
      values=[]
      chart = "karpenter"
      version="0.37.0"
      namespace = "kube-system"
      name  = "karpenter"
      set   = [
        {
          name  = "serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"
          value = "arn:aws:iam::752478543649:role/eks_lab_irsa_karpenter_controller"
        },
        {
          name  = "settings.clusterName"
          value = "eks-lab"
        },
        {
          name  = "settings.interruptionQueue"
          value = "eks-lab"
        },
        {
          name  = "settings.featureGates.drift"
          value = "false"
        },
        {
          name  = "settings.featureGates.spotToSpotConsolidation"
          value = "true"
        },
        {
          name  = "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[0].weight"
          value = "100"
        },
        {
          name  = "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[0].preference.matchExpressions[0].key"
          value = "karpenter.sh/nodepool"
        },
        {
          name  = "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[0].preference.matchExpressions[0].operator"
          value = "In"
        },
        {
          name  = "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[0].preference.matchExpressions[0].values[0]"
          value = "system"
        },
        {
          name  = "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[1].weight"
          value = "1"
        },
        {
          name  = "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[1].preference.matchExpressions[0].key"
          value = "eks.amazonaws.com/nodegroup"
        },
        {
          name  = "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[1].preference.matchExpressions[0].operator"
          value = "In"
        },
        {
          name  = "affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution[1].preference.matchExpressions[0].values[0]"
          value = "eks_lab_node_group_private"
        },
        {
          name  = "affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution"
          value = "null"
        }
      ]
      create_namespace = true
    }
    prod_load_balancer_controller_chart = {
      repository = "https://aws.github.io/eks-charts"
      chart = "aws-load-balancer-controller"
      version="1.8.3"
      values=[]
      namespace = "kube-system"
      name  = "aws-load-balancer-controller"
      set   = [
        {
          name  = "serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"
          value = "arn:aws:iam::752478543649:role/eks_lab_irsa_aws_load_balancer_controller"
        },
        {
          name  = "clusterName"
          value = "eks-lab"
        },
        {
          name  = "affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].key"
          value = "karpenter.sh/nodepool"
        },
        {
          name  = "affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].operator"
          value = "In"
        },
        {
          name  = "affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].values[0]"
          value = "system"
        },
        {
          name  = "affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].matchExpressions[0].key"
          value = "app.kubernetes.io/name"
        },
        {
          name  = "affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].matchExpressions[0].operator"
          value = "In"
        },
        {
          name  = "affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].matchExpressions[0].values[0]"
          value = "aws-load-balancer-controller"
        },
        {
          name  = "affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].topologyKey"
          value = "kubernetes.io/hostname"
        }
      ]
      create_namespace = true
      upgrade_install=true
    }
#     prod_argo-cd = {
#       repository = "https://argoproj.github.io/argo-helm"
#       chart = "argo-cd"
#       version="7.6.2"
#       namespace = "argo-cd"
#       upgrade_install=true
#       name  = "argo-cd"
#       values=[file("${path.root}/manifest/argo-custom-values.yaml")]
#       create_namespace = true
#       set=[]
#     }
    kube-ops-view = {
      repository = "https://charts.christianhuth.de"
      chart = "kube-ops-view"
      version="3.6.0"
      namespace = "kube-system"
      upgrade_install=true
      name  = "kube-ops-view"
      values=[]
      create_namespace = true
      set=[]
    }
    nth = {
      repository = "https://aws.github.io/eks-charts/"
      chart = "aws-node-termination-handler"
      version="0.21.0"
      namespace = "kube-system"
      upgrade_install=true
      name  = "aws-node-termination-handler"
      values=[file("${path.root}/manifest/nth-values.yaml")]
      create_namespace = true
      set=[]
    }
#     prod_argocd-image-updater = {
#       repository = "https://argoproj.github.io/argo-helm"
#       chart = "argocd-image-updater"
#       version="0.11.0"
#       namespace = "argo-cd"
#       name  = "argocd-image-updater"
#       upgrade_install=true
#       values=[file("${path.root}/manifest/argocd-image-updater-custom-value.yml")]
#       create_namespace = true
#       set = []
#     }


  }
}