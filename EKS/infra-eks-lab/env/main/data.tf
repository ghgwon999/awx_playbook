data "aws_iam_policy_document" "eks_cluster_role" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      identifiers = ["eks.amazonaws.com"]
      type        = "Service"
    }
  }
}

data "aws_iam_policy_document" "eks_node_group_role" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      identifiers = ["ec2.amazonaws.com"]
      type        = "Service"
    }
  }
}

data "tls_certificate" "prod_eks_cluster_1_oidc" {
  url = module.eks_cluster["eks-lab"].cluster_oidc
}


data "aws_iam_policy_document" "karpenter_sqs_policy"{
  statement {
    actions = ["sqs:SendMessage"]
    principals {
      identifiers = ["events.amazonaws.com", "sqs.amazonaws.com"]
      type        = "Service"
    }
    resources =["arn:aws:sqs:ap-northeast-2:752478543649:eks-lab"]
  }
}
