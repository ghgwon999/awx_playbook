terraform {
  required_version = ">= 1.8.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.4"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "~> 2.28"
    }
  }
  backend "s3" {
    bucket  = "eks-lab-terraform"
    key     = "lab/terraform.tfstate"
    region  = "ap-northeast-2"
    encrypt = true
  }
}
provider "kubernetes" {
  host                   = module.eks_cluster["eks-lab"].endpoint
  cluster_ca_certificate = base64decode(module.eks_cluster["eks-lab"].kubeconfig-certificate-authority-data)
  config_path    = ""
  config_context = ""

  exec {
    api_version = "client.authentication.k8s.io/v1beta1"
    command     = "aws"
    args        = ["eks", "get-token", "--cluster-name", module.eks_cluster["eks-lab"].cluster_name]
  }
}

provider "helm" {
  kubernetes {
    host                   = module.eks_cluster["eks-lab"].endpoint
    cluster_ca_certificate = base64decode(module.eks_cluster["eks-lab"].kubeconfig-certificate-authority-data)
    config_path    = ""
    config_context = ""
    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      command     = "aws"
      args        = ["eks", "get-token", "--cluster-name", module.eks_cluster["eks-lab"].cluster_name]
    }
  }
}
