# EKS_Terraform

## 모듈 변수 구성
- 각 모듈의 구성의 변수는 map(object({})) 타입으로 구성

## metrics 서버 배포
```bash
cd ./env/main/manifest
kubectl apply -f metrics-server.yml
```

## ArgoCD Image Updater 설치
```bash
#argocd image updater
 
helm repo add argo https://argoproj.github.io/argo-helm
helm upgrade --install argocd-image-updater argo/argocd-image-updater  --namespace argo-cd --create-namespace -f argocd-image-updater-custom-value.yml
#configmap/argocd-cm 내 사용자 정의 추가
#		data:
#		 # …
# accounts.image-updater: apiKey
#configmap/argocd-rbac-cm 권한 추가
#data.plicy.csv: |
#p, role:image-updater, applications, get, */*, allow
#p, role:image-updater, applications, update, */*, allow
#g, image-updater, role:image-updater


#argocd 액세스 토큰 생성
argocd account generate-token --account image-updater --id image-updater
kubectl create secret generic argocd-image-updater-secret \
	  --from-literal argocd.token=$YOUR_TOKEN --dry-run -o yaml |
kubectl -n argo-cd apply -f -
#ArgoCD Application Annotations 구성
#argocd-image-updater.argoproj.io/image-list = test-app=751732153713.dkr.ecr.ap-northeast-2.amazonaws.com/infra-team-tools
#argocd-image-updater.argoproj.io/test-app.helm.image-name=deployment.image.name <Helm 변수>
#argocd-image-updater.argoproj.io/test-app.helm.image-tag=deployment.image.tag <Helm 변수>
#argocd-image-updater.argoproj.io/test-app.update-strategy=newest-build
```
## FluentBit Install
```bash
helm upgrade --install aws-for-fluent-bit --namespace kube-system eks/aws-for-fluent-bit
```