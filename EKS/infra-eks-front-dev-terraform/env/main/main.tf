module "iam_role" {
  source             = "../../module/iam_role"
  for_each = merge(var.iam_roles,local.EKS_FRONT_DEV_IAM_ROLE)
  iam_role_config = each.value
  depends_on = [module.iam_policy]
}
output iam_role {
  value = flatten([for iam_roles in module.iam_role : iam_roles.iam_role])
}
module "iam_policy" {
  source = "../../module/iam_policy"
  for_each = merge(var.iam_policies,local.EKS_FRONT_DEV_IAM_POLICY)
  iam_policy_config = each.value
}
output iam_policy {
  value = flatten([for iam_roles in module.iam_role : iam_roles.iam_role])
}
module "security_groups" {
  source = "../../module/security_groups"
  for_each = merge(var.security_group,local.DEV_SECURITY_GROUPS)
  sg_config = each.value
  #depends_on = [module.vpc]
}

module "eks_cluster" {
  source            = "../../module/eks_cluster"
  for_each = merge(var.eks_cluster,local.EKS_FRONT_DEV_EKS_CLUSTER)
  eks_cluster_config = each.value
  depends_on = [
    module.iam_role,
    module.security_groups
    #module.subnets
  ]
}
module "launch_template" {
  source = "../../module/launch_template"
  for_each = merge(var.launch_template,local.EKS_FRONT_DEV_LAUNCH_TEMPLATES)
  launch_template_config = each.value
  depends_on = [
  module.eks_cluster,module.security_groups
  ]
}

/*
module "eks_node_group" {
  source = "../../module/eks_node_groups"
  for_each = merge(var.eks_node_group,local.EKS_FRONT_DEV_EKS_NODE_GROUP)
  eks_node_group_config = each.value
#  depends_on = [module.launch_template, module.iam_role,module.subnets,module.eks_cluster]
}
*/
module "iam_oidc" {
  source = "../../module/iam_oidc"
  for_each = merge(var.iam_oidc,local.EKS_FRONT_DEV_IAM_OIDC)
  iam_oidc_config = each.value
  depends_on = [
  module.eks_cluster]
}


module "iam_irsa" {
  source = "../../module/iam_role"
  for_each = merge(var.iam_roles,local.EKS_FRONT_DEV_IAM_ROLE_IRSA)
  iam_role_config = each.value
  depends_on = [
  module.eks_cluster,module.iam_oidc]
}

output iam_irsa {
  value = flatten([for iam_roles in module.iam_irsa : iam_roles.iam_role])
}

module "k8s_karpenter" {
  source = "../../module/karpenter"
  for_each = merge(var.k8s_karpenter,local.EKS_FRONT_DEV_KARPENTER)
  karpenter_config = each.value
}

module "helm_release" {
  source = "../../module/helm"
  for_each = merge(var.helm_release,local.DEV_MON)
  helm_release_config = each.value
  depends_on = [module.k8s_karpenter]
}


# # ##K8S Resources
# module "k8s_manifest" {
#   source = "../../module/k8s_manifest"
#   for_each = merge(var.k8s_manifest,local.DEV_TEST)
#   k8s_manifest_config = each.value
# }
