# EKS 배포 파이프라인
![img_10.png](img_10.png)

```bash
source : 테스트 앱 소스
app-chart :  테스트 was helm 차트
web-chart : 테스트 web helm 차트

argocd 접속 주소 : https://dev-argocd.tidesquare.com:3000/
```

## 파이프라인 테스트

#### 1. Argocd 로그인 후 test-web 접속
![img_2.png](img_2.png)

#### 2. LOGIN 구문 밑 TEST 1025 변경으로 배포 테스트
![img_3.png](img_3.png)
#### 3. 소스 수정 
```bash
source/src/main/resources/templates/login.html 파일 20라인 수정으로 테스트 
```
###### 변경 전
![img_4.png](img_4.png)

###### 변경 후
![img_5.png](img_5.png)


#### 4. 소스 커밋 & 파이프라인 확인
![img_6.png](img_6.png)

#### 5. 파이프라인 완료 후 ArgoCD 접속하여 배포 확인
![img_7.png](img_7.png)


#### 6. test-web 접속 후 변경 사항 확인
![img_8.png](img_8.png)