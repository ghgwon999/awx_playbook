terraform {
  required_version = ">= 1.8.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.4"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "~> 2.28"
    }
  }
  backend "s3" {
    bucket  = "ts-infra-terraform-state"
    key     = "front-eks/terraform.tfstate"
    region  = "ap-northeast-2"
    encrypt = true
  }
}
