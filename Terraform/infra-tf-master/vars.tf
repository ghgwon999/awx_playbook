####################################################
#IAM USERS
####################################################
variable "iam_users" {
  type = map(object({
    user_name  = string
    group_name = set(string)
    tag_name = string
  }))
  default = {
  }
}
locals {
  IAM_USERS_HOMEPAGE = {
    TS0984 = {
      user_name  = "TS0984"
      group_name = ["ts_readonly"]
      tag_name = "TS0984"

    }
    TS1147 = {
      user_name  = "TS1147"
      group_name = ["ts_infra"]
      tag_name = "SHINJI"
    }
    TS0989 = {
      user_name  = "TS0989"
      group_name = ["ts_infra"]
      tag_name = "NAMHJ"
    }
    TS1164 = {
      user_name  = "TS1164"
      group_name = ["ts_infra"]
      tag_name = "CHOGB"
    }
    TS0991 = {
      user_name  = "TS0991"
      group_name = ["ts_infra"]
      tag_name = "KWONOS"
    }
    TS1198 = {
      user_name  = "TS1198"
      group_name = ["ts_infra"]
      tag_name = "KWONGH"
    }
    TS1520 = {
      user_name  = "TS1520"
      group_name = ["ts_dba"]
      tag_name = "WEEDH"
    }
    TS1405 = {
      user_name  = "TS1405"
      group_name = ["ts_dba"]
      tag_name = "KIMYS"
    }
    TS1258 = {
      user_name  = "TS1258"
      group_name = ["ts_readonly"]
      tag_name = "TS1258"
    }
    TS1309 = {
      user_name  = "TS1309"
      group_name = ["ts_readonly"]
      tag_name = "TS1309"
    }
    TS1466 = {
      user_name  = "TS1466"
      group_name = ["ts_readonly"]
      tag_name = "TS1466"
    }
  }
  IAM_USERS_AIR = {
    TS1466 = {
      user_name  = "TS1466"
      group_name = ["ts_readonly"]
      tag_name = "TS1466"
    }
    TS1309 = {
      user_name  = "TS1309"
      group_name = ["ts_readonly"]
      tag_name = "TS1309"
    }
    TS1258 = {
      user_name  = "TS1258"
      group_name = ["ts_readonly"]
      tag_name = "TS1258"
    }
    TS1248 = {
      user_name  = "TS1248"
      group_name = ["ts_developer"]
      tag_name = "BYEONRS"
    }
    TS1213 = {
      user_name  = "TS1213"
      group_name = ["ts_developer"]
      tag_name = "LEEYM"
    }
    TS1039 = {
    user_name  = "TS1039"
    group_name = ["ts_developer"]
    tag_name = "YOONJC"
    }
    TS1061 = {
      user_name  = "TS1061"
      group_name = ["ts_developer"]
      tag_name = "JANGJS"
    }
    TS1520 = {
      user_name  = "TS1520"
      group_name = ["ts_dba"]
      tag_name = "WEEDH"
    }
    TS1180 = {
      user_name  = "TS1180"
      group_name = ["ts_admin"]
      tag_name = "KWONMS"
    }
    TS1250 = {
      user_name  = "TS1250"
      group_name = ["ts_admin"]
      tag_name = "JKPARK"
    }
    TS1355 = {
      user_name  = "TS1355"
      group_name = ["ts_developer"]
      tag_name = "KWONYJ"
    }
    TS0994 = {
      user_name  = "TS0994"
      group_name = ["ts_developer"]
      tag_name = "TS0994"
    }
    bitbucket-pipeline-for-eks = {
      user_name  = "bitbucket-pipeline-for-eks"
      group_name = ["ts_codedeploy"]
      tag_name = "bitbucket-pipeline-for-eks"
    }
    code-deploy-user = {
      user_name  = "code-deploy-user"
      group_name = ["ts_codedeploy"]
      tag_name = "code-deploy-user"
    }
    deploy = {
      user_name  = "deploy"
      group_name = ["ts_codedeploy"]
      tag_name = "deploy"
    }
    eks-admin = {
      user_name  = "eks-admin"
      group_name = ["ts_readonly"]
      tag_name = "eks-admin"
    }
    eks-deploy = {
      user_name  = "eks-deploy"
      group_name = ["ts_readonly"]
      tag_name = "eks-deploy"
    }
    settlement-deploy = {
      user_name  = "settlement-deploy"
      group_name = ["ts_readonly"]
      tag_name = "settlement-deploy"
    }
    s3-upload = {
      user_name  = "s3-upload"
      group_name = ["ts_s3"]
      tag_name = "s3-upload"
    }
    TS0478 = {
      user_name  = "TS0478"
      group_name = ["ts_admin"]
      tag_name = "UMTH"
    }
    TS0964 = {
      user_name  = "TS0964"
      group_name = ["ts_admin"]
      tag_name = "KIMHJ"
    }
    TS0979 = {
      user_name  = "TS0979"
      group_name = ["ts_admin"]
      tag_name = "TS0979"
    }
    TS0981 = {
      user_name  = "TS0981"
      group_name = ["ts_admin"]
      tag_name = "TS0981"
    }
    TS0984 = {
      user_name  = "TS0984"
      group_name = ["ts_admin"]
      tag_name = "TS0984"
    }
    TS0986 = {
      user_name  = "TS0986"
      group_name = ["ts_developer"]
      tag_name = "TS0986"
    }
    TS0987 = {
      user_name  = "TS0987"
      group_name = ["ts_developer"]
      tag_name = "TS0987"
    }
    TS0988 = {
      user_name  = "TS0988"
      group_name = ["ts_developer"]
      tag_name = "TS0988"
    }
    TS0989 = {
      user_name  = "TS0989"
      group_name = ["ts_infra"]
      tag_name = "TS0989"
    }
    TS0991 = {
      user_name  = "TS0991"
      group_name = ["ts_infra"]
      tag_name = "TS0991"
    }
    TS0992 = {
      user_name  = "TS0992"
      group_name = ["ts_admin"]
      tag_name = "TS0992"
    }
    TS0997 = {
      user_name  = "TS0997"
      group_name = ["ts_developer"]
      tag_name = "TS0997"
    }
    TS1076 = {
      user_name  = "TS1076"
      group_name = ["ts_developer"]
      tag_name = "TS1076"
    }
    TS1091 = {
      user_name  = "TS1091"
      group_name = ["ts_admin"]
      tag_name = "TS1091"
    }
    TS1097 = {
      user_name  = "TS1097"
      group_name = ["ts_admin"]
      tag_name = "TS1097"
    }
    TS1147 = {
      user_name  = "TS1147"
      group_name = ["ts_infra"]
      tag_name = "TS1147"
    }
    TS1164 = {
      user_name  = "TS1164"
      group_name = ["ts_infra"]
      tag_name = "TS1164"
    }
    TS1168 = {
      user_name  = "TS1168"
      group_name = ["ts_developer"]
      tag_name = "TS1168"
    }
    TS1198 = {
      user_name  = "TS1198"
      group_name = ["ts_infra"]
      tag_name = "TS1198"
    }
    TS1217 = {
      user_name  = "TS1217"
      group_name = ["ts_admin"]
      tag_name = "TS1217"
    }
    TS1298 = {
      user_name  = "TS1298"
      group_name = ["ts_developer"]
      tag_name = "TS1298"
    }
    TS1350 = {
      user_name  = "TS1350"
      group_name = ["ts_admin"]
      tag_name = "TS1350"
    }
    TS1384 = {
      user_name  = "TS1384"
      group_name = ["ts_admin"]
      tag_name = "TS1384"
    }
    TS1405 = {
      user_name  = "TS1405"
      group_name = ["ts_dba"]
      tag_name = "TS1405"
    }
    TS1439 = {
      user_name  = "TS1439"
      group_name = ["ts_infra"]
      tag_name = "TS1439"
    }
    zabbix = {
      user_name  = "zabbix"
      group_name = ["ts_dba"]
      tag_name = "zabbix"
    }
    toursoft-s3 = {
      user_name  = "toursoft-s3"
      group_name = []
      tag_name = "toursoft-s3"
    }
  }
  IAM_USERS_HOT = {
    TS1466 = {
      user_name  = "TS1466"
      group_name = ["ts_readonly"]
      tag_name = "TS1466"
    }
    TS1309 = {
      user_name  = "TS1309"
      group_name = ["ts_readonly"]
      tag_name = "TS1309"
    }
    TS1258 = {
      user_name  = "TS1258"
      group_name = ["ts_readonly"]
      tag_name = "TS1258"
    }
    TS1520 = {
      user_name  = "TS1520"
      group_name = ["ts_dba"]
      tag_name = "WEEDH"

    }
    TS0981 = {
      user_name  = "TS0981"
      group_name = ["ts_developer"]
      tag_name = "JEONHJ"
    }
    TS0984 = {
      user_name  = "TS0984"
      group_name = ["ts_admin"]
      tag_name = "KIMJY"
    }
    TS1097 = {
      user_name  = "TS1097"
      group_name = ["ts_admin"]
      tag_name = "LEEJH"
    }
    TS1298 = {
      user_name  = "TS1298"
      group_name = ["ts_developer"]
      tag_name = "KIMHK"
    }

    TS0989 = {
      user_name  = "TS0989"
      group_name = ["ts_infra"]
      tag_name = "TS0989"
    }
    TS0991 = {
      user_name  = "TS0991"
      group_name = ["ts_infra"]
      tag_name = "TS0991"
    }
    TS0992 = {
      user_name  = "TS0992"
      group_name = ["ts_admin"]
      tag_name = "TS0992"
    }
    TS1003 = {
      user_name  = "TS1003"
      group_name = ["ts_admin"]
      tag_name = "TS1003"
    }
    TS1005 = {
      user_name  = "TS1005"
      group_name = ["ts_developer"]
      tag_name = "TS1005"
    }
    TS1170 = {
      user_name  = "TS1170"
      group_name = ["ts_developer"]
      tag_name = "TS1170"
    }
    TS1007 = {
      user_name  = "TS1007"
      group_name = ["ts_developer"]
      tag_name = "TS1007"
    }
    TS1008 = {
      user_name  = "TS1008"
      group_name = ["ts_developer"]
      tag_name = "TS1008"
    }
    TS1009 = {
      user_name  = "TS1009"
      group_name = ["ts_developer"]
      tag_name = "TS1009"
    }
    TS1040 = {
      user_name  = "TS1040"
      group_name = ["ts_developer"]
      tag_name = "TS1040"
    }
    TS1041 = {
      user_name  = "TS1041"
      group_name = ["ts_developer"]
      tag_name = "TS1041"
    }
    TS1147 = {
      user_name  = "TS1147"
      group_name = ["ts_infra"]
      tag_name = "TS1147"
    }
    TS1164 = {
      user_name  = "TS1164"
      group_name = ["ts_infra"]
      tag_name = "TS1164"
    }
    TS1179 = {
      user_name  = "TS1179"
      group_name = ["ts_developer"]
      tag_name = "TS1179"
    }
    TS1180 = {
      user_name  = "TS1180"
      group_name = ["ts_developer"]
      tag_name = "TS1180"
    }
    TS1198 = {
      user_name  = "TS1198"
      group_name = ["ts_infra"]
      tag_name = "TS1198"
    }
    TS1247 = {
      user_name  = "TS1247"
      group_name = ["ts_developer"]
      tag_name = "TS1247"
    }
    TS1405 = {
      user_name  = "TS1405"
      group_name = ["ts_dba"]
      tag_name = "TS1405"
    }
    TS1439 = {
      user_name  = "TS1439"
      group_name = ["ts_infra"]
      tag_name = "TS1439"
    }
    zabbix = {
      user_name  = "zabbix"
      group_name = ["ts_dba"]
      tag_name = "zabbix"
    }




  }
  IAM_USERS_COMM = {
    TS1466 = {
      user_name  = "TS1466"
      group_name = ["ts_readonly"]
      tag_name = "TS1466"
    }
    TS1309 = {
      user_name  = "TS1309"
      group_name = ["ts_readonly"]
      tag_name = "TS1309"
    }
    TS1258 = {
      user_name  = "TS1258"
      group_name = ["ts_readonly"]
      tag_name = "TS1258"
    }
    TS0991 = {
      user_name = "TS0991"
      group_name = ["ts_infra"]
      tag_name  = "KWONOS"
    }
    TS1164 = {
      user_name = "TS1164"
      group_name = ["ts_infra"]
      tag_name  = "CHOGB"
    }
    TS0989 = {
      user_name = "TS0989"
      group_name = ["ts_infra"]
      tag_name  = "NAMHJ"
    }
    TS1439 = {
      user_name = "TS1439"
      group_name = ["ts_infra"]
      tag_name  = "KWONSJ"
    }
    TS1147 = {
      user_name = "TS1147"
      group_name = ["ts_infra"]
      tag_name  = "SHINJI"
    }
    TS1198 = {
      user_name = "TS1198"
      group_name = ["ts_infra"]
      tag_name  = "KWONGH"
    }
    TS1520 = {
      user_name = "TS1520"
      group_name = ["ts_dba"]
      tag_name  = "WEEDH"
    }
    TS1168 = {
      user_name = "TS1168"
      group_name = ["ts_admin"]
      tag_name  = "LEECG"
    }
    TS1180 = {
      user_name = "TS1180"
      group_name = ["ts_admin"]
      tag_name  = "KWONMS"
    }
    TS1097 = {
      user_name = "TS1097"
      group_name = ["ts_admin"]
      tag_name  = "LEEJH"
    }
    TS0984 = {
      user_name = "TS0984"
      group_name = ["ts_admin"]
      tag_name  = "KIMJY"
    }
    TS1298 = {
      user_name = "TS1298"
      group_name = ["ts_admin"]
      tag_name  = "KIMHG"
    }
    TS0986 = {
      user_name = "TS0986"
      group_name = ["ts_admin"]
      tag_name  = "KIMYH"
    }
    TS1384 = {
      user_name = "TS1384"
      group_name = ["ts_admin"]
      tag_name  = "KANGHS"
    }
    TS0988 = {
      user_name = "TS0988"
      group_name = ["ts_admin"]
      tag_name  = "KIMGH"
    }
    TS0992 = {
      user_name = "TS0992"
      group_name = ["ts_admin"]
      tag_name  = "LEEDY"
    }
    TS0981 = {
      user_name = "TS0981"
      group_name = ["ts_admin"]
      tag_name  = "JEONHJ"
    }
    TS1350 = {
      user_name = "TS1350"
      group_name = ["ts_admin"]
      tag_name  = "LEEYS"
    }
    TS0964 = {
      user_name = "TS0964"
      group_name = ["ts_admin"]
      tag_name  = "KIMHJ"
    }
    zabbix = {
      user_name = "zabbix"
      group_name = ["ts_dba"]
      tag_name  = "zabbix"
    }
    TS1405 = {
      user_name = "TS1405"
      group_name = ["ts_dba"]
      tag_name  = "KIMYS"
    }
  }
  IAM_USERS_NDC = {
    #NDC 직접관리
    TS1520 = {
      user_name  = "TS1520"
      group_name = ["ts_dba"]
      tag_name = "WEEDH"
    }
  }
  IAM_USERS_TNA = {
    TS1466 = {
      user_name  = "TS1466"
      group_name = ["ts_readonly"]
      tag_name = "TS1466"
    }
    TS1309 = {
      user_name  = "TS1309"
      group_name = ["ts_readonly"]
      tag_name = "TS1309"
    }
    TS1258 = {
      user_name  = "TS1258"
      group_name = ["ts_readonly"]
      tag_name = "TS1258"
    }
    TS1520 = {
      user_name  = "TS1520"
      group_name = ["ts_dba"]
      tag_name = "WEEDH"
    }
    TS0989 = {
      #hj nam
      user_name  = "TS0989"
      group_name = ["ts_infra"]
      tag_name = "NAMHJ"
    }
    TS0991 = {
      #os kwon
      user_name  = "TS0991"
      group_name = ["ts_infra"]
      tag_name = "KWONOS"
    }
    TS1198 = {
      #gh kwon
      user_name  = "TS1198"
      group_name = ["ts_infra"]
      tag_name = "KWONGH"
    }
    TS1164 = {
      #gb cho
      user_name  = "TS1164"
      group_name = ["ts_infra"]
      tag_name = "CHOGB"
    }
    TS0981 = {
      #hj jeon
      user_name  = "TS0981"
      group_name = ["ts_admin"]
      tag_name = "JEONHJ"
    }
    TS0984 = {
      #KIMJY
      user_name  = "TS0984"
      group_name = ["ts_admin"]
      tag_name = "KIMJY"
    }
    TS1097 = {
      #LEEJH
      user_name  = "TS1097"
      group_name = ["ts_admin"]
      tag_name = "LEEJH"
    }
    TS0988 = {
      #KIMHG0815
      user_name  = "TS0988"
      group_name = ["ts_admin"]
      tag_name = "KIMHG0815"
    }
    codeartifact_reader = {
      #codeartifact_reader
      user_name  = "codeartifact_reader"
      group_name = ["ts_codeartifact"]
      tag_name = "codeartifact_reader"
    }
    benepia = {
      user_name  = "benepia"
      group_name = ["ts_s3"]
      tag_name = "benepia"
    }
    bkchoi = {
      user_name  = "bkchoi"
      group_name = ["ts_admin"]
      tag_name = "bkchoi"
    }
    cdc9308 = {
      user_name  = "cdc9308"
      group_name = ["ts_admin"]
      tag_name = "cdc9308"
    }
    dmlim = {
      user_name  = "dmlim"
      group_name = ["ts_admin"]
      tag_name = "dmlim"
    }
    dudals8937 = {
      user_name  = "dudals8937"
      group_name = ["ts_admin"]
      tag_name = "dudals8937"
    }
    fedev = {
      user_name  = "fedev"
      group_name = ["ts_developer"]
      tag_name = "fedev"
    }
    hdjeong = {
      user_name  = "hdjeong"
      group_name = ["ts_admin"]
      tag_name = "hdjeong"
    }
    hsmoon = {
      user_name  = "hsmoon"
      group_name = ["ts_admin"]
      tag_name = "hsmoon"
    }
    jcyoon = {
      user_name  = "jcyoon"
      group_name = ["ts_admin"]
      tag_name = "jcyoon"
    }
    jhjin = {
      user_name  = "jhjin"
      group_name = ["ts_admin"]
      tag_name = "jhjin"
    }
    jsjang = {
      user_name  = "jsjang"
      group_name = ["ts_admin"]
      tag_name = "jsjang"
    }
    jungkee = {
      user_name  = "jungkee"
      group_name = ["ts_admin"]
      tag_name = "jungkee"
    }
    khsgw = {
      user_name  = "khsgw"
      group_name = ["ts_admin"]
      tag_name = "khsgw"
    }
    kimhg = {
      user_name  = "kimhg"
      group_name = ["ts_admin"]
      tag_name = "kimhg"
    }
    leecg = {
      user_name  = "leecg"
      group_name = ["ts_admin"]
      tag_name = "leecg"
    }
    lkh = {
      user_name  = "lkh"
      group_name = ["ts_admin"]
      tag_name = "lkh"
    }
    OS0143 = {
      user_name  = "OS0143"
      group_name = ["ts_admin"]
      tag_name = "OS0143"
    }
    raynor = {
      user_name  = "raynor"
      group_name = ["ts_admin"]
      tag_name = "raynor"
    }
    taewonh = {
      user_name  = "taewonh"
      group_name = ["ts_admin"]
      tag_name = "taewonh"
    }
    thum = {
      user_name  = "thum"
      group_name = ["ts_admin"]
      tag_name = "thum"
    }
    tmembership = {
      user_name  = "tmembership"
      group_name = ["ts_s3"]
      tag_name = "tmembership"
    }
    TS0478 = {
      user_name  = "TS0478"
      group_name = ["ts_developer"]
      tag_name = "TS0478"
    }
    TS0979 = {
      user_name  = "TS0979"
      group_name = ["ts_admin"]
      tag_name = "TS0979"
    }
    TS0983 = {
      user_name  = "TS0983"
      group_name = ["ts_admin"]
      tag_name = "TS0983"
    }
    TS0994 = {
      user_name  = "TS0994"
      group_name = ["ts_admin"]
      tag_name = "TS0994"
    }
    TS1039 = {
      user_name  = "TS1039"
      group_name = ["ts_developer"]
      tag_name = "TS1039"
    }
    TS1057 = {
      user_name  = "TS1057"
      group_name = ["ts_developer"]
      tag_name = "TS1057"
    }
    TS1061 = {
      user_name  = "TS1061"
      group_name = ["ts_admin"]
      tag_name = "TS1061"
    }
    TS1061_key = {
      user_name  = "TS1061_key"
      group_name = ["ts_developer"]
      tag_name = "TS1061_key"
    }
    TS1129 = {
      user_name  = "TS1129"
      group_name = ["ts_admin"]
      tag_name = "TS1129"
    }
    TS1147 = {
      user_name  = "TS1147"
      group_name = ["ts_infra"]
      tag_name = "TS1147"
    }
    TS1153 = {
      user_name  = "TS1153"
      group_name = ["ts_admin"]
      tag_name = "TS1153"
    }
    ts1180 = {
      user_name  = "ts1180"
      group_name = ["ts_admin"]
      tag_name = "ts1180"
    }
    TS1213 = {
      user_name  = "TS1213"
      group_name = ["ts_developer"]
      tag_name = "TS1213"
    }
    TS1232 = {
      user_name  = "TS1232"
      group_name = ["ts_developer"]
      tag_name = "TS1232"
    }
    TS1239 = {
      user_name  = "TS1239"
      group_name = ["ts_developer"]
      tag_name = "TS1239"
    }
    TS1248 = {
      user_name  = "TS1248"
      group_name = ["ts_developer"]
      tag_name = "TS1248"
    }
    TS1249 = {
      user_name  = "TS1249"
      group_name = ["ts_developer"]
      tag_name = "TS1249"
    }
    TS1250 = {
      user_name  = "TS1250"
      group_name = ["ts_admin"]
      tag_name = "TS1250"
    }
    TS1272 = {
      user_name  = "TS1272"
      group_name = ["ts_s3"]
      tag_name = "TS1272"
    }
    TS1390 = {
      user_name  = "TS1390"
      group_name = ["ts_developer"]
      tag_name = "TS1390"
    }
    TS1403 = {
      user_name  = "TS1403"
      group_name = ["ts_developer"]
      tag_name = "TS1403"
    }
    TS1405 = {
      user_name  = "TS1405"
      group_name = ["ts_dba"]
      tag_name = "TS1405"
    }
    TS1439 = {
      user_name  = "TS1439"
      group_name = ["ts_infra"]
      tag_name = "TS1439"
    }
    TS1508 = {
      user_name  = "TS1508"
      group_name = ["ts_admin"]
      tag_name = "TS1508"
    }
    TS1515 = {
      user_name  = "TS1515"
      group_name = ["ts_admin"]
      tag_name = "TS1515"
    }
    TS1220 = {
      user_name  = "TS1220"
      group_name = ["ts_developer"]
      tag_name = "TS1220"
    }
    zabbix = {
      user_name  = "zabbix"
      group_name = ["ts_dba"]
      tag_name = "zabbix"
    }
    hjchoe = {
      user_name  = "hj.choe"
      group_name = ["ts_admin"]
      tag_name = "hjchoe"
    }
  }
  IAM_USERS_DW = {
    TS1466 = {
      user_name  = "TS1466"
      group_name = ["ts_readonly"]
      tag_name = "TS1466"
    }
    TS1309 = {
      user_name  = "TS1309"
      group_name = ["ts_readonly"]
      tag_name = "TS1309"
    }
    TS1258 = {
      user_name  = "TS1258"
      group_name = ["ts_readonly"]
      tag_name = "TS1258"
    }
    TS1520 = {
      user_name  = "TS1520"
      group_name = ["ts_dba"]
      tag_name = "WEEDH"
    }
    OS0337 = {
      user_name  = "OS0337"
      group_name = ["ts_data_analysis"]
      tag_name = "Jimmy_Hoang"
    }
    TS0968 = {
      user_name  = "TS0968"
      group_name = ["ts_admin"]
      tag_name = "TS0968"
    }
    TS0978 = {
      user_name  = "TS0978"
      group_name = ["ts_admin"]
      tag_name = "TS0978"
    }
    TS0981 = {
      user_name  = "TS0981"
      group_name = ["ts_admin"]
      tag_name = "TS0981"
    }
    TS0984 = {
      user_name  = "TS0984"
      group_name = ["ts_admin"]
      tag_name = "TS0984"
    }
    TS0989 = {
      user_name  = "TS0989"
      group_name = ["ts_infra"]
      tag_name = "TS0989"
    }
    TS0991 = {
      user_name  = "TS0991"
      group_name = ["ts_infra"]
      tag_name = "TS0991"
    }
    TS0992 = {
      user_name  = "TS0992"
      group_name = ["ts_admin"]
      tag_name = "TS0992"
    }
    TS1010 = {
      user_name  = "TS1010"
      group_name = ["ts_admin"]
      tag_name = "TS1010"
    }
    TS1057 = {
      user_name  = "TS1057"
      group_name = ["ts_developer"]
      tag_name = "TS1057"
    }
    TS1080 = {
      user_name  = "TS1080"
      group_name = ["ts_data_analysis"]
      tag_name = "TS1080"
    }
    TS1097 = {
      user_name  = "TS1097"
      group_name = ["ts_admin"]
      tag_name = "TS1097"
    }
    TS1115 = {
      user_name  = "TS1115"
      group_name = ["ts_data_analysis"]
      tag_name = "TS1115"
    }
    TS1147 = {
      user_name  = "TS1147"
      group_name = ["ts_infra"]
      tag_name = "TS1147"
    }
    TS1162 = {
      user_name  = "TS1162"
      group_name = ["ts_admin"]
      tag_name = "TS1162"
    }
    TS1164 = {
      user_name  = "TS1164"
      group_name = ["ts_infra"]
      tag_name = "TS1164"
    }
    TS1177 = {
      user_name  = "TS1177"
      group_name = ["ts_admin"]
      tag_name = "TS1177"
    }
    TS1180 = {
      user_name  = "TS1180"
      group_name = ["ts_developer"]
      tag_name = "TS1180"
    }
    TS1184 = {
      user_name  = "TS1184"
      group_name = ["ts_data_analysis"]
      tag_name = "TS1184"
    }
    TS1198 = {
      user_name  = "TS1198"
      group_name = ["ts_infra"]
      tag_name = "TS1198"
    }
    TS1203 = {
      user_name  = "TS1203"
      group_name = ["ts_data_analysis"]
      tag_name = "TS1203"
    }
    TS1205 = {
      user_name  = "TS1205"
      group_name = ["ts_data_analysis"]
      tag_name = "TS1205"
    }
    TS1217 = {
      user_name  = "TS1217"
      group_name = ["ts_admin"]
      tag_name = "TS1217"
    }
    TS1255 = {
      user_name  = "TS1255"
      group_name = ["ts_data_analysis"]
      tag_name = "TS1255"
    }
    TS1267 = {
      user_name  = "TS1267"
      group_name = ["ts_data_analysis"]
      tag_name = "TS1267"
    }
    TS1321 = {
      user_name  = "TS1321"
      group_name = ["ts_admin"]
      tag_name = "TS1321"
    }
    TS1350 = {
      user_name  = "TS1350"
      group_name = ["ts_admin"]
      tag_name = "TS1350"
    }
    TS1384 = {
      user_name  = "TS1384"
      group_name = ["ts_admin"]
      tag_name = "TS1384"
    }
    TS1405 = {
      user_name  = "TS1405"
      group_name = ["ts_dba"]
      tag_name = "TS1405"
    }
    TS1410 = {
      user_name  = "TS1410"
      group_name = ["ts_infra"]
      tag_name = "TS1410"
    }
    TS1413 = {
      user_name  = "TS1413"
      group_name = ["ts_infra"]
      tag_name = "TS1413"
    }
    TS1437 = {
      user_name  = "TS1437"
      group_name = ["ts_developer"]
      tag_name = "TS1437"
    }
    TS1439 = {
      user_name  = "TS1439"
      group_name = ["ts_infra"]
      tag_name = "TS1439"
    }

  }
  IAM_USERS_DMS = {
    TS1466 = {
      user_name  = "TS1466"
      group_name = ["ts_readonly"]
      tag_name = "TS1466"
    }
    TS1309 = {
      user_name  = "TS1309"
      group_name = ["ts_readonly"]
      tag_name = "TS1309"
    }
    TS1258 = {
      user_name  = "TS1258"
      group_name = ["ts_readonly"]
      tag_name = "TS1258"
    }
    TS1147 = {
      user_name = "TS1147"
      group_name = ["ts_infra"]
      tag_name  = "TS1147"
    }
    TS0991 = {
      user_name = "TS0991"
      group_name = ["ts_infra"]
      tag_name  = "TS0991"
    }
    TS0989 = {
      user_name = "TS0989"
      group_name = ["ts_infra"]
      tag_name  = "TS0989"
    }
    TS1198 = {
      user_name = "TS1198"
      group_name = ["ts_infra"]
      tag_name  = "KWONGH"
    }
    ts0984 = {
      user_name = "ts0984"
      group_name = ["ts_admin"]
      tag_name  = "KIMJY"
    }
    ts1097 = {
      user_name = "ts1097"
      group_name = ["ts_admin"]
      tag_name  = "LEEJH"
    }
    ts0981 = {
      user_name = "ts0981"
      group_name = ["ts_admin"]
      tag_name  = "JEONHJ"
    }
    zabbix = {
      user_name = "zabbix"
      group_name = ["ts_dba"]
      tag_name  = "zabbix"
    }
    TS1439 = {
      user_name = "TS1439"
      group_name = ["ts_infra"]
      tag_name  = "KWONSJ"
    }
    TS1164 = {
      user_name  = "TS1164"
      group_name = ["ts_infra"]
      tag_name = "CHOGB"
    }
    TS1520 = {
      user_name  = "TS1520"
      group_name = ["ts_dba"]
      tag_name = "WEEDH"
    }
    TS1405 = {
      user_name  = "TS1405"
      group_name = ["ts_dba"]
      tag_name = "KIMYS"
    }
  }
  IAM_USERS_VIET = {
    TS1466 = {
      user_name  = "TS1466"
      group_name = ["ts_readonly"]
      tag_name = "TS1466"
    }
    TS1309 = {
      user_name  = "TS1309"
      group_name = ["ts_readonly"]
      tag_name = "TS1309"
    }
    TS1258 = {
      user_name  = "TS1258"
      group_name = ["ts_readonly"]
      tag_name = "TS1258"
    }
    TS1198 = {
      user_name = "TS1198"
      group_name = ["ts_infra"]
      tag_name  = "TS1198"
    }
    TS1147 = {
      user_name = "TS1147"
      group_name = ["ts_infra"]
      tag_name  = "TS1147"
    }
    TS1164 = {
      user_name = "TS1164"
      group_name = ["ts_infra"]
      tag_name  = "TS1164"
    }
    TS0991 = {
      user_name = "TS0991"
      group_name = ["ts_infra"]
      tag_name  = "TS0991"
    }
    TS0989 = {
      user_name = "TS0989"
      group_name = ["ts_infra"]
      tag_name  = "TS0989"
    }
    TS1520 = {
      user_name  = "TS1520"
      group_name = ["ts_dba"]
      tag_name = "WEEDH"
    }
    hanoi05 = {
      user_name  = "hanoi05@tidesquare.com"
      group_name = ["ts_viet_dev_ops"]
      tag_name = "hanoi05"
    }
    hungpv94 = {
      user_name  = "hungpv94@tidesquare.com"
      group_name = ["ts_viet_dev_ops","ts_viet_dev"]
      tag_name = "hungpv94"
    }
    kyoonlee = {
      user_name  = "kyoonlee@tidesquare.com"
      group_name = ["ts_viet_eks"]
      tag_name = "kyoonlee"
    }
    osve08 = {
      user_name  = "osve08@tidesquare.com"
      group_name = ["ts_viet_dev_ops","ts_viet_dev"]
      tag_name = "osve08"
    }
    osve13 = {
      user_name  = "osve13@tidesquare.com"
      group_name = ["ts_viet_dev_ops","ts_viet_dev"]
      tag_name = "osve13"
    }
    thangvv = {
      user_name  = "thangvv@tidesquare.com"
      group_name = ["ts_admin"]
      tag_name = "thangvv"
    }
    trinhthevils = {
      user_name  = "trinhthevils@tidesquare.com"
      group_name = ["ts_viet_dev"]
      tag_name = "trinhthevils"
    }
    TS1350 = {
      user_name  = "TS1350"
      group_name = ["ts_admin"]
      tag_name = "TS1350"
    }
    TS1405 = {
      user_name  = "TS1405"
      group_name = ["ts_dba"]
      tag_name = "TS1405"
    }
    TS1439 = {
      user_name  = "TS1439"
      group_name = ["ts_infra"]
      tag_name = "TS1439"
    }
    zabbix = {
      user_name  = "zabbix"
      group_name = ["ts_dba"]
      tag_name = "zabbix"
    }
    nhan = {
      user_name  = "nhan.lq@tidesquare.com"
      group_name = ["ts_admin"]
      tag_name = "nhan"
    }
    dev-local-user = {
      user_name  = "dev-local-user"
      group_name = ["ts_viet_eks"]
      tag_name = "dev-local-user"
    }

  }
  IAM_USERS_BTMS = {
    TS1466 = {
      user_name  = "TS1466"
      group_name = ["ts_readonly"]
      tag_name = "TS1466"
    }
    TS1309 = {
      user_name  = "TS1309"
      group_name = ["ts_readonly"]
      tag_name = "TS1309"
    }
    TS1258 = {
      user_name  = "TS1258"
      group_name = ["ts_readonly"]
      tag_name = "TS1258"
    }
    TS1147 = {
      user_name = "TS1147"
      group_name = ["ts_infra"]
      tag_name  = "TS1147"
    }
    TS1164 = {
      user_name = "TS1164"
      group_name = ["ts_infra"]
      tag_name  = "TS1164"
    }
    TS0991 = {
      user_name = "TS0991"
      group_name = ["ts_infra"]
      tag_name  = "TS0991"
    }
    TS0989 = {
      user_name = "TS0989"
      group_name = ["ts_infra"]
      tag_name  = "TS0989"
    }
    TS1439 = {
      user_name  = "TS1439"
      group_name = ["ts_infra"]
      tag_name = "KWONSJ"
    }
    TS1198 = {
      user_name  = "TS1198"
      group_name = ["ts_infra"]
      tag_name = "KWONGH"
    }
    TS1520 = {
      user_name  = "TS1520"
      group_name = ["ts_dba"]
      tag_name = "weedh"
    }
  }
  IAM_USERS_KMS = {
    TS1466 = {
      user_name  = "TS1466"
      group_name = ["ts_readonly"]
      tag_name = "TS1466"
    }
    TS1309 = {
      user_name  = "TS1309"
      group_name = ["ts_readonly"]
      tag_name = "TS1309"
    }
    TS1258 = {
      user_name  = "TS1258"
      group_name = ["ts_readonly"]
      tag_name = "TS1258"
    }
    codeartifact_reader = {
      #codeartifact_reader
      user_name  = "codeartifact_reader"
      group_name = ["ts_codeartifact"]
      tag_name = "codeartifact_reader"
    }
    TS0979 = {
      user_name  = "TS0979"
      group_name = ["ts_admin"]
      tag_name = "LIMDM"
    }
    TS1180 = {
      user_name  = "TS1180"
      group_name = ["ts_admin"]
      tag_name = "KWONMS"
    }
    TS1439 = {
      user_name  = "TS1439"
      group_name = ["ts_infra"]
      tag_name = "KWONSJ"
    }
    TS1147 = {
      user_name  = "TS1147"
      group_name = ["ts_infra"]
      tag_name = "SHINJI"
    }
    TS0989 = {
      user_name  = "TS0989"
      group_name = ["ts_infra"]
      tag_name = "NAMHJ"
    }
    TS1164 = {
      user_name  = "TS1164"
      group_name = ["ts_infra"]
      tag_name = "CHOGB"
    }
    TS0991 = {
      user_name  = "TS0991"
      group_name = ["ts_infra"]
      tag_name = "KWONOS"
    }
    TS1198 = {
      user_name  = "TS1198"
      group_name = ["ts_infra"]
      tag_name = "KWONGH"
    }
    TS1520 = {
      user_name  = "TS1520"
      group_name = ["ts_dba"]
      tag_name = "WEEDH"
    }
    TS1405 = {
      user_name  = "TS1405"
      group_name = ["ts_dba"]
      tag_name = "KIMYS"
    }
  }
}

####################################################
#IAM GROUPS
####################################################
#HOMEPAGE IAM
variable "iam_groups_homepage" {
  type = map(object({
    group_name = string
    group_path = string
    mgd_policy = set(string)
  }))
  default = {
    ts_infra = {
      group_name = "ts_infra"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_admin = {
      group_name = "ts_admin"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_developer = {
      group_name = "ts_developer"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/AmazonAthenaFullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_s3 = {
      group_name = "ts_s3"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_codedeploy = {
      group_name = "ts_codedeploy"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AWSCodeDeployFullAccess","arn:aws:iam::aws:policy/AmazonEC2FullAccess","arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess"]
    }
    ts_readonly = {
      group_name = "ts_readonly"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_dba = {
      group_name = "ts_dba"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/job-function/DatabaseAdministrator","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
  }
}
#AIR IAM
variable "iam_groups_air" {
  type = map(object({
    group_name = string
    group_path = string
    mgd_policy = set(string)
  }))
  default = {
    ts_infra = {
      group_name = "ts_infra"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_admin = {
      group_name = "ts_admin"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_developer = {
      group_name = "ts_developer"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/CloudWatchLogsFullAccess","arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/AmazonAthenaFullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword","arn:aws:iam::aws:policy/AmazonEventBridgeFullAccess","arn:aws:iam::aws:policy/AmazonSNSFullAccess","arn:aws:iam::aws:policy/CloudWatchFullAccessV2"]}
    ts_s3 = {
      group_name = "ts_s3"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_codedeploy = {
      group_name = "ts_codedeploy"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AWSCodeDeployFullAccess","arn:aws:iam::aws:policy/AmazonEC2FullAccess","arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess"]
    }
    ts_readonly = {
      group_name = "ts_readonly"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_dba = {
      group_name = "ts_dba"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/job-function/DatabaseAdministrator","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
  }
}
#HOT IAM
variable "iam_groups_hot" {
  type = map(object({
    group_name = string
    group_path = string
    mgd_policy = set(string)
  }))

  default = {
    ts_infra = {
      group_name = "ts_infra"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_admin = {
      group_name = "ts_admin"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_developer = {
      group_name = "ts_developer"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/CloudWatchLogsFullAccess","arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/AmazonAthenaFullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword","arn:aws:iam::aws:policy/AmazonEventBridgeFullAccess","arn:aws:iam::aws:policy/AmazonSNSFullAccess","arn:aws:iam::aws:policy/CloudWatchFullAccessV2"]}
    ts_s3 = {
      group_name = "ts_s3"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_codedeploy = {
      group_name = "ts_codedeploy"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AWSCodeDeployFullAccess","arn:aws:iam::aws:policy/AmazonEC2FullAccess","arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess"]
    }
    ts_readonly = {
      group_name = "ts_readonly"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_dba = {
      group_name = "ts_dba"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/job-function/DatabaseAdministrator","arn:aws:iam::aws:policy/IAMUserChangePassword","arn:aws:iam::aws:policy/ReadOnlyAccess"]
    }
  }
}
#comm IAM
variable "iam_groups_comm" {
  type = map(object({
    group_name = string
    group_path = string
    mgd_policy = set(string)
  }))

  default = {
    ts_infra = {
      group_name = "ts_infra"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_admin = {
      group_name = "ts_admin"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_dba = {
      group_name = "ts_dba"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/job-function/DatabaseAdministrator","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_developer = {
      group_name = "ts_developer"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/CloudWatchLogsFullAccess","arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/AmazonAthenaFullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword","arn:aws:iam::aws:policy/AmazonEventBridgeFullAccess","arn:aws:iam::aws:policy/AmazonSNSFullAccess","arn:aws:iam::aws:policy/CloudWatchFullAccessV2"]}
    ts_s3 = {
      group_name = "ts_s3"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_codedeploy = {
      group_name = "ts_codedeploy"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AWSCodeDeployFullAccess","arn:aws:iam::aws:policy/AmazonEC2FullAccess","arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess"]
    }
    ts_readonly = {
      group_name = "ts_readonly"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
  }
}

#NDC IAM
variable "iam_groups_ndc" {
  type = map(object({
    group_name = string
    group_path = string
    mgd_policy = set(string)
  }))

  default = {
    ts_infra = {
      group_name = "ts_infra"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_admin = {
      group_name = "ts_admin"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_dba = {
      group_name = "ts_dba"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/job-function/DatabaseAdministrator","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_developer = {
      group_name = "ts_developer"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/CloudWatchLogsFullAccess","arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/AmazonAthenaFullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword","arn:aws:iam::aws:policy/AmazonEventBridgeFullAccess","arn:aws:iam::aws:policy/AmazonSNSFullAccess","arn:aws:iam::aws:policy/CloudWatchFullAccessV2"]}
    ts_s3 = {
      group_name = "ts_s3"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_codedeploy = {
      group_name = "ts_codedeploy"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AWSCodeDeployFullAccess","arn:aws:iam::aws:policy/AmazonEC2FullAccess","arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess"]
    }
    ts_readonly = {
      group_name = "ts_readonly"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
  }

}
#dw IAM
variable "iam_groups_dw" {
  type = map(object({
    group_name = string
    group_path = string
    mgd_policy = set(string)
  }))
  default = {
    ts_infra = {
      group_name = "ts_infra"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_admin = {
      group_name = "ts_admin"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_dba = {
      group_name = "ts_dba"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/job-function/DatabaseAdministrator","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_developer = {
      group_name = "ts_developer"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/CloudWatchLogsFullAccess","arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/AmazonAthenaFullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword","arn:aws:iam::aws:policy/AmazonEventBridgeFullAccess","arn:aws:iam::aws:policy/AmazonSNSFullAccess","arn:aws:iam::aws:policy/CloudWatchFullAccessV2"]}
    ts_s3 = {
      group_name = "ts_s3"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_codedeploy = {
      group_name = "ts_codedeploy"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AWSCodeDeployFullAccess","arn:aws:iam::aws:policy/AmazonEC2FullAccess","arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess"]
    }
    ts_readonly = {
      group_name = "ts_readonly"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_data_analysis = {
      group_name = "ts_data_analysis"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AWSKeyManagementServicePowerUser","arn:aws:iam::aws:policy/AWSGlueConsoleFullAccess","arn:aws:iam::aws:policy/AmazonSQSFullAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/AmazonAthenaFullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword","arn:aws:iam::aws:policy/AmazonRDSFullAccess"]

    }
  }
}
#tna IAM
variable "iam_groups_tna" {
  type = map(object({
    group_name = string
    group_path = string
    mgd_policy = set(string)
  }))
  default = {
    ts_infra = {
      group_name = "ts_infra"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_admin = {
      group_name = "ts_admin"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_dba = {
      group_name = "ts_dba"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/job-function/DatabaseAdministrator",
        "arn:aws:iam::aws:policy/IAMUserChangePassword","arn:aws:iam::aws:policy/ReadOnlyAccess",
        "arn:aws:iam::aws:policy/service-role/AmazonDMSVPCManagementRole"
      ]
    }
    ts_developer = {
      group_name = "ts_developer"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/CloudWatchLogsFullAccess","arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/AmazonAthenaFullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword","arn:aws:iam::aws:policy/AmazonEventBridgeFullAccess","arn:aws:iam::aws:policy/AmazonSNSFullAccess","arn:aws:iam::aws:policy/CloudWatchFullAccessV2"]}
    ts_s3 = {
      group_name = "ts_s3"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_codedeploy = {
      group_name = "ts_codedeploy"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AWSCodeDeployFullAccess","arn:aws:iam::aws:policy/AmazonEC2FullAccess","arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess"]
    }
    ts_readonly = {
      group_name = "ts_readonly"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_codeartifact = {
      group_name = "ts_codeartifact"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AWSCodeArtifactReadOnlyAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_program = {
      group_name = "ts_program"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/ElasticLoadBalancingFullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword","arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/AWSCloudFormationFullAccess",
        "arn:aws:iam::aws:policy/AmazonSSMFullAccess","arn:aws:iam::aws:policy/AmazonSNSFullAccess","arn:aws:iam::aws:policy/AmazonECS_FullAccess","arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"]
    }
  }
}
#viet IAM
variable "iam_groups_viet" {
  type = map(object({
    group_name = string
    group_path = string
    mgd_policy = set(string)
  }))

  default = {
    ts_infra = {
      group_name = "ts_infra"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_admin = {
      group_name = "ts_admin"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_dba = {
      group_name = "ts_dba"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/job-function/DatabaseAdministrator","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_developer = {
      group_name = "ts_developer"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/CloudWatchLogsFullAccess","arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/AmazonAthenaFullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword","arn:aws:iam::aws:policy/AmazonEventBridgeFullAccess","arn:aws:iam::aws:policy/AmazonSNSFullAccess","arn:aws:iam::aws:policy/CloudWatchFullAccessV2"]}
    ts_s3 = {
      group_name = "ts_s3"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_codedeploy = {
      group_name = "ts_codedeploy"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AWSCodeDeployFullAccess","arn:aws:iam::aws:policy/AmazonEC2FullAccess","arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess"]
    }
    ts_readonly = {
      group_name = "ts_readonly"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_viet_dev = {
      group_name = "ts_viet_dev"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AmazonECS_FullAccess",
        "arn:aws:iam::aws:policy/AmazonRDSFullAccess",
        "arn:aws:iam::aws:policy/AmazonS3FullAccess",
        "arn:aws:iam::aws:policy/AmazonSQSFullAccess",
        "arn:aws:iam::aws:policy/CloudWatchFullAccessV2",
        "arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_viet_dev_ops = {
      group_name = "ts_viet_dev_ops"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess-Amplify",
        "arn:aws:iam::aws:policy/AmazonRoute53FullAccess",
        "arn:aws:iam::aws:policy/AmazonSNSFullAccess",
        "arn:aws:iam::aws:policy/AWSCertificateManagerFullAccess",
        "arn:aws:iam::aws:policy/AWSCertificateManagerPrivateCAFullAccess",
        "arn:aws:iam::aws:policy/AWSCloudFormationFullAccess",
        "arn:aws:iam::aws:policy/AWSCodeBuildAdminAccess",
        "arn:aws:iam::aws:policy/AWSCodeDeployFullAccess",
        "arn:aws:iam::aws:policy/AWSCodePipeline_FullAccess",
        "arn:aws:iam::aws:policy/AWSLambda_FullAccess"
      ]
    }
    ts_viet_eks = {
      group_name = "ts_viet_eks"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/IAMUserChangePassword",
      "arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy",
      "arn:aws:iam::aws:policy/AmazonEC2FullAccess",
      "arn:aws:iam::aws:policy/AWSCloudFormationFullAccess"]
    }



  }
}
#dms IAM
variable "iam_groups_dms" {
  type = map(object({
    group_name = string
    group_path = string
    mgd_policy = set(string)
  }))

  default = {
    ts_infra = {
      group_name = "ts_infra"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_admin = {
      group_name = "ts_admin"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_dba = {
      group_name = "ts_dba"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/job-function/DatabaseAdministrator","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_developer = {
      group_name = "ts_developer"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/CloudWatchLogsFullAccess","arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/AmazonAthenaFullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword","arn:aws:iam::aws:policy/AmazonEventBridgeFullAccess","arn:aws:iam::aws:policy/AmazonSNSFullAccess","arn:aws:iam::aws:policy/CloudWatchFullAccessV2"]}
    ts_s3 = {
      group_name = "ts_s3"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_codedeploy = {
      group_name = "ts_codedeploy"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AWSCodeDeployFullAccess","arn:aws:iam::aws:policy/AmazonEC2FullAccess","arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess"]
    }
    ts_readonly = {
      group_name = "ts_readonly"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
  }
}

#btms IAM
variable "iam_groups_btms" {
  type = map(object({
    group_name = string
    group_path = string
    mgd_policy = set(string)
  }))

  default = {
    ts_infra = {
      group_name = "ts_infra"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_admin = {
      group_name = "ts_admin"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_dba = {
      group_name = "ts_dba"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/job-function/DatabaseAdministrator","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_developer = {
      group_name = "ts_developer"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/CloudWatchLogsFullAccess","arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/AmazonAthenaFullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword","arn:aws:iam::aws:policy/AmazonEventBridgeFullAccess","arn:aws:iam::aws:policy/AmazonSNSFullAccess","arn:aws:iam::aws:policy/CloudWatchFullAccessV2"]}
    ts_s3 = {
      group_name = "ts_s3"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_codedeploy = {
      group_name = "ts_codedeploy"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AWSCodeDeployFullAccess","arn:aws:iam::aws:policy/AmazonEC2FullAccess","arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess","arn:aws:iam::aws:policy/AmazonS3FullAccess"]
    }
    ts_readonly = {
      group_name = "ts_readonly"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
  }
}

#kms IAM
variable "iam_groups_kms" {
  type = map(object({
    group_name = string
    group_path = string
    mgd_policy = set(string)
  }))

  default = {
    ts_infra = {
      group_name = "ts_infra"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_admin = {
      group_name = "ts_admin"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AdministratorAccess"]
    }
    ts_dba = {
      group_name = "ts_dba"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/job-function/DatabaseAdministrator","arn:aws:iam::aws:policy/IAMUserChangePassword","arn:aws:iam::aws:policy/ReadOnlyAccess"]
    }
    ts_codeartifact = {
      group_name = "ts_codeartifact"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/AWSCodeArtifactReadOnlyAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
    ts_readonly = {
      group_name = "ts_readonly"
      group_path = "/"
      mgd_policy = ["arn:aws:iam::aws:policy/ReadOnlyAccess","arn:aws:iam::aws:policy/IAMUserChangePassword"]
    }
  }
}


####################################################
#IAM POLICIES
####################################################
variable "iam_policies" {
  type = map(object({
    name = string
    policy = any
    description = string
    tags = any
  }))
  default = {}
}
locals {
  IAM_CUSTOM_POLICY = {
    IAM_FORCE_MFA_FOR_ACCESS_KEY = {
      name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      policy = "${path.root}/template/mfa_policy.json"
      description = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      tags = {
        Name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      }
    }
    IAM_POLICY_RDS_SCHEDULER = {
      name = "IAM_POLICY_RDS_SCHEDULER"
      policy = "${path.root}/template/RDS_Scheduler_policy.json"
      description = "IAM_POLICY_RDS_SCHEDULER"
      tags = {
        Name = "IAM_POLICY_RDS_SCHEDULER"
      }
    }
    IAM_COMPUTE_OPTIMIZER = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy = "${path.root}/template/compute_optimizer_policy.json"
      description = "IAM_COMPUTE_OPTIMIZER"
      tags = {
        Name = "IAM_COMPUTE_OPTIMIZER"
      }
    }
    IAM_DENY_POLICY = {
      name = "IAM_DENY_POLICY"
      policy = "${path.root}/template/iam_deny_policy.json"
      description = "IAM_DENY_POLICY"
      tags = {
        Name = "IAM_DENY_POLICY"
      }
    }
    IAM_DBA_POLICY = {
      name = "IAM_DBA_POLICY"
      policy = "${path.root}/template/dba_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_DBA_POLICY"
      }
    }
    IAM_SECUI_POLICY = {
      name = "IAM_SECUI_POLICY"
      policy = "${path.root}/template/secui_policy.json"
      description = "IAM_SECUI_POLICY"
      tags = {
        Name = "IAM_SECUI_POLICY"
      }
    }
    IAM_MFA_ALLOW_POLICY = {
      name = "IAM_MFA_ALLOW_POLICY"
      policy = "${path.root}/template/mfa_allow_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_MFA_ALLOW_POLICY"
      }
    }
  }
  IAM_CUSTOM_POLICY_AIR = {
    IAM_FORCE_MFA_FOR_ACCESS_KEY = {
      name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      policy = "${path.root}/template/mfa_policy.json"
      description = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      tags = {
        Name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      }
    }
    IAM_POLICY_RDS_SCHEDULER = {
      name = "IAM_POLICY_RDS_SCHEDULER"
      policy = "${path.root}/template/RDS_Scheduler_policy.json"
      description = "IAM_POLICY_RDS_SCHEDULER"
      tags = {
        Name = "IAM_POLICY_RDS_SCHEDULER"
      }
    }
    IAM_COMPUTE_OPTIMIZER = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy = "${path.root}/template/compute_optimizer_policy.json"
      description = "IAM_COMPUTE_OPTIMIZER"
      tags = {
        Name = "IAM_COMPUTE_OPTIMIZER"
      }
    }
    IAM_DENY_POLICY = {
      name = "IAM_DENY_POLICY"
      policy = "${path.root}/template/iam_deny_policy.json"
      description = "IAM_DENY_POLICY"
      tags = {
        Name = "IAM_DENY_POLICY"
      }
    }
    IAM_DBA_POLICY = {
      name = "IAM_DBA_POLICY"
      policy = "${path.root}/template/dba_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_DBA_POLICY"
      }
    }
    IAM_SECUI_POLICY = {
      name = "IAM_SECUI_POLICY"
      policy = "${path.root}/template/secui_policy.json"
      description = "IAM_SECUI_POLICY"
      tags = {
        Name = "IAM_SECUI_POLICY"
      }
    }
    IAM_TOURSOFT_S3_POLICY = {
      name = "IAM_TOURSOFT_S3_POLICY"
      policy = "${path.root}/template/toursoft-s3-access.json"
      description = "IAM_TOURSOFT_S3_POLICY"
      tags = {
        Name = "IAM_TOURSOFT_S3_POLICY"
      }
    }
    IAM_MFA_ALLOW_POLICY = {
      name = "IAM_MFA_ALLOW_POLICY"
      policy = "${path.root}/template/mfa_allow_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_MFA_ALLOW_POLICY"
      }
    }
  }
  IAM_CUSTOM_POLICY_HOT = {
    IAM_MFA_ALLOW_POLICY = {
      name = "IAM_MFA_ALLOW_POLICY"
      policy = "${path.root}/template/mfa_allow_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_MFA_ALLOW_POLICY"
      }
    }
    IAM_FORCE_MFA_FOR_ACCESS_KEY = {
      name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      policy = "${path.root}/template/mfa_policy.json"
      description = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      tags = {
        Name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      }
    }
    IAM_POLICY_RDS_SCHEDULER = {
      name = "IAM_POLICY_RDS_SCHEDULER"
      policy = "${path.root}/template/RDS_Scheduler_policy.json"
      description = "IAM_POLICY_RDS_SCHEDULER"
      tags = {
        Name = "IAM_POLICY_RDS_SCHEDULER"
      }
    }
    IAM_COMPUTE_OPTIMIZER = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy = "${path.root}/template/compute_optimizer_policy.json"
      description = "IAM_COMPUTE_OPTIMIZER"
      tags = {
        Name = "IAM_COMPUTE_OPTIMIZER"
      }
    }
    IAM_DENY_POLICY = {
      name = "IAM_DENY_POLICY"
      policy = "${path.root}/template/iam_deny_policy.json"
      description = "IAM_DENY_POLICY"
      tags = {
        Name = "IAM_DENY_POLICY"
      }
    }
    IAM_DBA_POLICY = {
      name = "IAM_DBA_POLICY"
      policy = "${path.root}/template/dba_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_DBA_POLICY"
      }
    }
    IAM_SECUI_POLICY = {
      name = "IAM_SECUI_POLICY"
      policy = "${path.root}/template/secui_policy.json"
      description = "IAM_SECUI_POLICY"
      tags = {
        Name = "IAM_SECUI_POLICY"
      }
    }
  }
  IAM_CUSTOM_POLICY_KONG = {
    IAM_MFA_ALLOW_POLICY = {
      name = "IAM_MFA_ALLOW_POLICY"
      policy = "${path.root}/template/mfa_allow_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_MFA_ALLOW_POLICY"
      }
    }
    IAM_FORCE_MFA_FOR_ACCESS_KEY = {
      name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      policy = "${path.root}/template/mfa_policy.json"
      description = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      tags = {
        Name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      }
    }
    IAM_POLICY_RDS_SCHEDULER = {
      name = "IAM_POLICY_RDS_SCHEDULER"
      policy = "${path.root}/template/RDS_Scheduler_policy.json"
      description = "IAM_POLICY_RDS_SCHEDULER"
      tags = {
        Name = "IAM_POLICY_RDS_SCHEDULER"
      }
    }
    IAM_COMPUTE_OPTIMIZER = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy = "${path.root}/template/compute_optimizer_policy.json"
      description = "IAM_COMPUTE_OPTIMIZER"
      tags = {
        Name = "IAM_COMPUTE_OPTIMIZER"
      }
    }
    IAM_DENY_POLICY = {
      name = "IAM_DENY_POLICY"
      policy = "${path.root}/template/iam_deny_policy.json"
      description = "IAM_DENY_POLICY"
      tags = {
        Name = "IAM_DENY_POLICY"
      }
    }
    IAM_DBA_POLICY = {
      name = "IAM_DBA_POLICY"
      policy = "${path.root}/template/dba_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_DBA_POLICY"
      }
    }
    IAM_SECUI_POLICY = {
      name = "IAM_SECUI_POLICY"
      policy = "${path.root}/template/secui_policy.json"
      description = "IAM_SECUI_POLICY"
      tags = {
        Name = "IAM_SECUI_POLICY"
      }
    }
  }
  IAM_CUSTOM_POLICY_TNA = {
    IAM_MFA_ALLOW_POLICY = {
      name = "IAM_MFA_ALLOW_POLICY"
      policy = "${path.root}/template/mfa_allow_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_MFA_ALLOW_POLICY"
      }
    }
    IAM_FORCE_MFA_FOR_ACCESS_KEY = {
      name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      policy = "${path.root}/template/mfa_policy.json"
      description = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      tags = {
        Name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      }
    }
    IAM_POLICY_RDS_SCHEDULER = {
      name = "IAM_POLICY_RDS_SCHEDULER"
      policy = "${path.root}/template/RDS_Scheduler_policy.json"
      description = "IAM_POLICY_RDS_SCHEDULER"
      tags = {
        Name = "IAM_POLICY_RDS_SCHEDULER"
      }
    }
    IAM_COMPUTE_OPTIMIZER = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy = "${path.root}/template/compute_optimizer_policy.json"
      description = "IAM_COMPUTE_OPTIMIZER"
      tags = {
        Name = "IAM_COMPUTE_OPTIMIZER"
      }
    }
    IAM_DENY_POLICY = {
      name = "IAM_DENY_POLICY"
      policy = "${path.root}/template/iam_deny_policy.json"
      description = "IAM_DENY_POLICY"
      tags = {
        Name = "IAM_DENY_POLICY"
      }
    }
    IAM_DBA_POLICY = {
      name = "IAM_DBA_POLICY"
      policy = "${path.root}/template/dba_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_DBA_POLICY"
      }
    }
    IAM_SECUI_POLICY = {
      name = "IAM_SECUI_POLICY"
      policy = "${path.root}/template/secui_policy.json"
      description = "IAM_SECUI_POLICY"
      tags = {
        Name = "IAM_SECUI_POLICY"
      }
    }
    IAM_FEDEV2_POLICY = {
      name = "IAM_FEDEV2_POLICY"
      policy = "${path.root}/template/fedev2-codecommit-policy.json"
      description = "IAM_FEDEV2_POLICY"
      tags = {
        Name = "IAM_FEDEV2_POLICY"
      }
    }
    IAM_COMMON_API_USER_POLICY = {
      #using common-api-user
      name = "IAM_COMMON_API_USER_POLICY"
      policy = "${path.root}/template/tna-common-api-user-policy.json"
      description = "IAM_COMMON_API_USER_POLICY"
      tags = {
        Name = "IAM_COMMON_API_USER_POLICY"
      }
    }
    IAM_TNA_RESELL_USER_POLICY = {
      #using tna-resell-user
      name = "IAM_TNA_RESELL_USER_POLICY"
      policy = "${path.root}/template/tna-resell-user-policy.json"
      description = "IAM_TNA_RESELL_USER_POLICY"
      tags = {
        Name = "IAM_TNA_RESELL_USER_POLICY"
      }
    }
    IAM_TOACT_SEARCH_S3_POLICY = {
      #using toact-search-s3
      name = "IAM_TOACT_SEARCH_S3_POLICY"
      policy = "${path.root}/template/tna-toact-search-s3-policy.json"
      description = "IAM_TOACT_SEARCH_S3_POLICY"
      tags = {
        Name = "IAM_TOACT_SEARCH_S3_POLICY"
      }
    }
    IAN_TNA_TOURVIS_ALB_ERR_LOG_API_POLICY = {
      #using tourvis-alb-err-log-api
      name = "IAN_TNA_TOURVIS_ALB_ERR_LOG_API_POLICY"
      policy = "${path.root}/template/tna-tourvis-alb-err-log-api-policy.json"
      description = "IAN_TNA_TOURVIS_ALB_ERR_LOG_API_POLICY"
      tags = {
        Name = "IAN_TNA_TOURVIS_ALB_ERR_LOG_API_POLICY"
      }
    }
    IAM_TNA_PIPELINE_ALB_LOG_PARSER = {
      #using pipeline alb-log-parser
      name = "IAM_TNA_PIPELINE_ALB_LOG_PARSER"
      policy = "${path.root}/template/tna-pipeline-alb-log-parser.json"
      description = "IAM_TNA_PIPELINE_ALB_LOG_PARSER"
      tags = {
        Name = "IAM_TNA_PIPELINE_ALB_LOG_PARSER"
      }
    }
    IAM_TNA_PIPELINE_ECR_POLICY = {
      #using pipeline ecr policy
      name = "IAM_TNA_PIPELINE_ECR_POLICY"
      policy = "${path.root}/template/tna-pipeline-ecr-policy.json"
      description = "IAM_TNA_PIPELINE_ECR_POLICY"
      tags = {
        Name = "IAM_TNA_PIPELINE_ECR_POLICY"
      }
    }
    IAM_TNA_TS_PROGRAM_CUSTOM_POLICY = {
      #using ts-program group custom policy
      name = "IAM_TNA_TS_PROGRAM_CUSTOM_POLICY"
      policy = "${path.root}/template/tna-ts-program-custom-policy.json"
      description = "IAM_TNA_TS_PROGRAM_CUSTOM_POLICY"
      tags = {
        Name = "IAM_TNA_TS_PROGRAM_CUSTOM_POLICY"
      }
    }

  }
  IAM_CUSTOM_POLICY_NDC = {
    IAM_FORCE_MFA_FOR_ACCESS_KEY = {
      name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      policy = "${path.root}/template/mfa_policy.json"
      description = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      tags = {
        Name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      }
    }
    IAM_POLICY_RDS_SCHEDULER = {
      name = "IAM_POLICY_RDS_SCHEDULER"
      policy = "${path.root}/template/RDS_Scheduler_policy.json"
      description = "IAM_POLICY_RDS_SCHEDULER"
      tags = {
        Name = "IAM_POLICY_RDS_SCHEDULER"
      }
    }
    IAM_COMPUTE_OPTIMIZER = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy = "${path.root}/template/compute_optimizer_policy.json"
      description = "IAM_COMPUTE_OPTIMIZER"
      tags = {
        Name = "IAM_COMPUTE_OPTIMIZER"
      }
    }
    IAM_DENY_POLICY = {
      name = "IAM_DENY_POLICY"
      policy = "${path.root}/template/iam_deny_policy.json"
      description = "IAM_DENY_POLICY"
      tags = {
        Name = "IAM_DENY_POLICY"
      }
    }
    IAM_DBA_POLICY = {
      name = "IAM_DBA_POLICY"
      policy = "${path.root}/template/dba_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_DBA_POLICY"
      }
    }
    IAM_SECUI_POLICY = {
      name = "IAM_SECUI_POLICY"
      policy = "${path.root}/template/secui_policy.json"
      description = "IAM_SECUI_POLICY"
      tags = {
        Name = "IAM_SECUI_POLICY"
      }
    }

  }
  IAM_CUSTOM_POLICY_VIET = {
    IAM_MFA_ALLOW_POLICY = {
      name = "IAM_MFA_ALLOW_POLICY"
      policy = "${path.root}/template/mfa_allow_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_MFA_ALLOW_POLICY"
      }
    }
    IAM_FORCE_MFA_FOR_ACCESS_KEY = {
      name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      policy = "${path.root}/template/mfa_policy.json"
      description = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      tags = {
        Name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      }
    }
    IAM_POLICY_RDS_SCHEDULER = {
      name = "IAM_POLICY_RDS_SCHEDULER"
      policy = "${path.root}/template/RDS_Scheduler_policy.json"
      description = "IAM_POLICY_RDS_SCHEDULER"
      tags = {
        Name = "IAM_POLICY_RDS_SCHEDULER"
      }
    }
    IAM_COMPUTE_OPTIMIZER = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy = "${path.root}/template/compute_optimizer_policy.json"
      description = "IAM_COMPUTE_OPTIMIZER"
      tags = {
        Name = "IAM_COMPUTE_OPTIMIZER"
      }
    }
    IAM_DENY_POLICY = {
      name = "IAM_DENY_POLICY"
      policy = "${path.root}/template/iam_deny_policy.json"
      description = "IAM_DENY_POLICY"
      tags = {
        Name = "IAM_DENY_POLICY"
      }
    }
    IAM_DBA_POLICY = {
      name = "IAM_DBA_POLICY"
      policy = "${path.root}/template/dba_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_DBA_POLICY"
      }
    }
    IAM_VIET_EKS_POLICY = {
      name = "IAM_VIET_EKS_POLICY"
      policy = "${path.root}/template/ts_viet_eks_policy.json"
      description = "IAM_VIET_EKS_POLICY"
      tags = {
        Name = "IAM_VIET_EKS_POLICY"
      }
    }
  }
  IAM_CUSTOM_POLICY_DMS = {
    IAM_MFA_ALLOW_POLICY = {
      name = "IAM_MFA_ALLOW_POLICY"
      policy = "${path.root}/template/mfa_allow_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_MFA_ALLOW_POLICY"
      }
    }
    IAM_FORCE_MFA_FOR_ACCESS_KEY = {
      name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      policy = "${path.root}/template/mfa_policy.json"
      description = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      tags = {
        Name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      }
    }
    IAM_POLICY_RDS_SCHEDULER = {
      name = "IAM_POLICY_RDS_SCHEDULER"
      policy = "${path.root}/template/RDS_Scheduler_policy.json"
      description = "IAM_POLICY_RDS_SCHEDULER"
      tags = {
        Name = "IAM_POLICY_RDS_SCHEDULER"
      }
    }
    IAM_COMPUTE_OPTIMIZER = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy = "${path.root}/template/compute_optimizer_policy.json"
      description = "IAM_COMPUTE_OPTIMIZER"
      tags = {
        Name = "IAM_COMPUTE_OPTIMIZER"
      }
    }
    IAM_DENY_POLICY = {
      name = "IAM_DENY_POLICY"
      policy = "${path.root}/template/iam_deny_policy.json"
      description = "IAM_DENY_POLICY"
      tags = {
        Name = "IAM_DENY_POLICY"
      }
    }
    IAM_DBA_POLICY = {
      name = "IAM_DBA_POLICY"
      policy = "${path.root}/template/dba_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_DBA_POLICY"
      }
    }
  }
  IAM_CUSTOM_POLICY_DW = {
    IAM_MFA_ALLOW_POLICY = {
      name = "IAM_MFA_ALLOW_POLICY"
      policy = "${path.root}/template/mfa_allow_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_MFA_ALLOW_POLICY"
      }
    }
    IAM_FORCE_MFA_FOR_ACCESS_KEY = {
      name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      policy = "${path.root}/template/mfa_policy.json"
      description = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      tags = {
        Name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      }
    }
    IAM_POLICY_RDS_SCHEDULER = {
      name = "IAM_POLICY_RDS_SCHEDULER"
      policy = "${path.root}/template/RDS_Scheduler_policy.json"
      description = "IAM_POLICY_RDS_SCHEDULER"
      tags = {
        Name = "IAM_POLICY_RDS_SCHEDULER"
      }
    }
    IAM_COMPUTE_OPTIMIZER = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy = "${path.root}/template/compute_optimizer_policy.json"
      description = "IAM_COMPUTE_OPTIMIZER"
      tags = {
        Name = "IAM_COMPUTE_OPTIMIZER"
      }
    }
    IAM_DENY_POLICY = {
      name = "IAM_DENY_POLICY"
      policy = "${path.root}/template/iam_deny_policy.json"
      description = "IAM_DENY_POLICY"
      tags = {
        Name = "IAM_DENY_POLICY"
      }
    }
    IAM_DBA_POLICY = {
      name = "IAM_DBA_POLICY"
      policy = "${path.root}/template/dba_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_DBA_POLICY"
      }
    }
    IAM_SECUI_POLICY = {
      name = "IAM_SECUI_POLICY"
      policy = "${path.root}/template/secui_policy.json"
      description = "IAM_SECUI_POLICY"
      tags = {
        Name = "IAM_SECUI_POLICY"
      }
    }
  }

  IAM_CUSTOM_POLICY_BTMS = {
    IAM_MFA_ALLOW_POLICY = {
      name = "IAM_MFA_ALLOW_POLICY"
      policy = "${path.root}/template/mfa_allow_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_MFA_ALLOW_POLICY"
      }
    }
    IAM_FORCE_MFA_FOR_ACCESS_KEY = {
      name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      policy = "${path.root}/template/mfa_policy.json"
      description = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      tags = {
        Name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      }
    }
    IAM_POLICY_RDS_SCHEDULER = {
      name = "IAM_POLICY_RDS_SCHEDULER"
      policy = "${path.root}/template/RDS_Scheduler_policy.json"
      description = "IAM_POLICY_RDS_SCHEDULER"
      tags = {
        Name = "IAM_POLICY_RDS_SCHEDULER"
      }
    }
    IAM_COMPUTE_OPTIMIZER = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy = "${path.root}/template/compute_optimizer_policy.json"
      description = "IAM_COMPUTE_OPTIMIZER"
      tags = {
        Name = "IAM_COMPUTE_OPTIMIZER"
      }
    }
    IAM_DENY_POLICY = {
      name = "IAM_DENY_POLICY"
      policy = "${path.root}/template/iam_deny_policy.json"
      description = "IAM_DENY_POLICY"
      tags = {
        Name = "IAM_DENY_POLICY"
      }
    }
    IAM_DBA_POLICY = {
      name = "IAM_DBA_POLICY"
      policy = "${path.root}/template/dba_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_DBA_POLICY"
      }
    }
  }
  IAM_CUSTOM_POLICY_KMS = {
    IAM_MFA_ALLOW_POLICY = {
      name = "IAM_MFA_ALLOW_POLICY"
      policy = "${path.root}/template/mfa_allow_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_MFA_ALLOW_POLICY"
      }
    }
    IAM_FORCE_MFA_FOR_ACCESS_KEY = {
      name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      policy = "${path.root}/template/mfa_policy.json"
      description = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      tags = {
        Name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      }
    }
    IAM_DENY_POLICY = {
      name = "IAM_DENY_POLICY"
      policy = "${path.root}/template/iam_deny_policy.json"
      description = "IAM_DENY_POLICY"
      tags = {
        Name = "IAM_DENY_POLICY"
      }
    }
    IAM_DBA_POLICY = {
      name = "IAM_DBA_POLICY"
      policy = "${path.root}/template/dba_policy.json"
      description = "IAM_DBA_POLICY"
      tags = {
        Name = "IAM_DBA_POLICY"
      }
    }
  }
}

####################################################
#IAM USER POLICY ATTACH
####################################################
variable "iam_user_policy_attach" {
  type = map(object({
    name = string
    policy_arn = string
    roles = set(string)
    users = set(string)
    groups = set(string)
  }))
  default = {}
}
locals {
  IAM_CUSTOM_POLICY_ATTACH = {
    IAM_MFA_ALLOW_POLICY_ATTACH = {
      name = "IAM_MFA_ALLOW_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy["IAM_MFA_ALLOW_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_developer"]
    }
    IAM_FORCE_MFA_FOR_ACCESS_KEY_ATTACH = {
      name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      policy_arn = module.iam_custom_policy["IAM_FORCE_MFA_FOR_ACCESS_KEY"].policy_arn
      roles= []
      users = []
      groups = ["ts_infra","ts_admin"]
    }
    IAM_COMPUTE_OPTIMIZER_ATTACH = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy_arn = module.iam_custom_policy["IAM_COMPUTE_OPTIMIZER"].policy_arn
      roles= ["IAM_ROLE_FOR_COMPUTE_OPTIMIZER"]
      users = []
      groups = []
    }
    IAM_DENY_POLICY_ATTACH = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy_arn = module.iam_custom_policy["IAM_DENY_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_admin"]
    }
    IAM_DBA_POLICY_ATTACH = {
      name = "IAM_DBA_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy["IAM_DBA_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_dba"]
    }
    IAM_SECUI_POLICY_ATTACH = {
      name = "IAM_DBA_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy["IAM_SECUI_POLICY"].policy_arn
      roles= ["TIDESQUARE_IAM_ROLE_FOR_SECUI"]
      users = ["TIDESQUARE_IAM_USER_FOR_SECUI"]
      groups = []
    }
  }
  IAM_CUSTOM_POLICY_ATTACH_AIR = {
    IAM_MFA_ALLOW_POLICY_ATTACH = {
      name = "IAM_MFA_ALLOW_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_air["IAM_MFA_ALLOW_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_developer"]
    }
    IAM_FORCE_MFA_FOR_ACCESS_KEY_ATTACH = {
      name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      policy_arn = module.iam_custom_policy_air["IAM_FORCE_MFA_FOR_ACCESS_KEY"].policy_arn
      roles= []
      users = []
      groups = ["ts_infra","ts_admin"]
    }
    IAM_COMPUTE_OPTIMIZER_ATTACH = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy_arn = module.iam_custom_policy_air["IAM_COMPUTE_OPTIMIZER"].policy_arn
      roles= ["IAM_ROLE_FOR_COMPUTE_OPTIMIZER"]
      users = []
      groups = []
    }
    IAM_DENY_POLICY_ATTACH = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy_arn = module.iam_custom_policy_air["IAM_DENY_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_admin"]
    }
    IAM_DBA_POLICY_ATTACH = {
      name = "IAM_DBA_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_air["IAM_DBA_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_dba"]
    }
    IAM_SECUI_POLICY_ATTACH = {
      name = "IAM_DBA_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_air["IAM_SECUI_POLICY"].policy_arn
      roles= ["TIDESQUARE_IAM_ROLE_FOR_SECUI"]
      users = ["TIDESQUARE_IAM_USER_FOR_SECUI"]
      groups = []
    }
  }
  IAM_CUSTOM_POLICY_ATTACH_HOT = {
    IAM_MFA_ALLOW_POLICY_ATTACH = {
      name = "IAM_MFA_ALLOW_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_hot["IAM_MFA_ALLOW_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_developer"]
    }
    IAM_FORCE_MFA_FOR_ACCESS_KEY_ATTACH = {
      name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      policy_arn = module.iam_custom_policy_hot["IAM_FORCE_MFA_FOR_ACCESS_KEY"].policy_arn
      roles= []
      users = []
      groups = ["ts_infra","ts_admin"]
    }
    IAM_COMPUTE_OPTIMIZER_ATTACH = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy_arn = module.iam_custom_policy_hot["IAM_COMPUTE_OPTIMIZER"].policy_arn
      roles= ["IAM_ROLE_FOR_COMPUTE_OPTIMIZER"]
      users = []
      groups = []
    }
    IAM_DENY_POLICY_ATTACH = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy_arn = module.iam_custom_policy_hot["IAM_DENY_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_admin"]
    }
    IAM_DBA_POLICY_ATTACH = {
      name = "IAM_DBA_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_hot["IAM_DBA_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_dba"]
    }
    IAM_SECUI_POLICY_ATTACH = {
      name = "IAM_DBA_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_hot["IAM_SECUI_POLICY"].policy_arn
      roles= ["TIDESQUARE_IAM_ROLE_FOR_SECUI"]
      users = ["TIDESQUARE_IAM_USER_FOR_SECUI"]
      groups = []
    }
  }
  IAM_CUSTOM_POLICY_ATTACH_KONG = {
    IAM_MFA_ALLOW_POLICY_ATTACH = {
      name = "IAM_MFA_ALLOW_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_comm["IAM_MFA_ALLOW_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_developer"]
    }
    IAM_FORCE_MFA_FOR_ACCESS_KEY_ATTACH = {
      name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      policy_arn = module.iam_custom_policy_comm["IAM_FORCE_MFA_FOR_ACCESS_KEY"].policy_arn
      roles= []
      users = []
      groups = ["ts_admin","ts_infra"]
    }
    IAM_COMPUTE_OPTIMIZER_ATTACH = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy_arn = module.iam_custom_policy_comm["IAM_COMPUTE_OPTIMIZER"].policy_arn
      roles= ["IAM_ROLE_FOR_COMPUTE_OPTIMIZER"]
      users = []
      groups = []
    }
    IAM_DENY_POLICY_ATTACH = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy_arn = module.iam_custom_policy_comm["IAM_DENY_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_admin"]
    }
    IAM_DBA_POLICY_ATTACH = {
      name = "IAM_DBA_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_comm["IAM_DBA_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_dba"]
    }
    IAM_SECUI_POLICY_ATTACH = {
      name = "IAM_DBA_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_comm["IAM_SECUI_POLICY"].policy_arn
      roles= ["TIDESQUARE_IAM_ROLE_FOR_SECUI"]
      users = ["TIDESQUARE_IAM_USER_FOR_SECUI"]
      groups = []
    }
  }
  IAM_CUSTOM_POLICY_ATTACH_TNA = {
    IAM_MFA_ALLOW_POLICY_ATTACH = {
      name = "IAM_MFA_ALLOW_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_tna["IAM_MFA_ALLOW_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_developer"]
    }
    IAM_FORCE_MFA_FOR_ACCESS_KEY_ATTACH = {
      name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      policy_arn = module.iam_custom_policy_tna["IAM_FORCE_MFA_FOR_ACCESS_KEY"].policy_arn
      roles= []
      users = []
      groups = ["ts_admin","ts_infra"]
    }
    IAM_COMPUTE_OPTIMIZER_ATTACH = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy_arn = module.iam_custom_policy_tna["IAM_COMPUTE_OPTIMIZER"].policy_arn
      roles= ["IAM_ROLE_FOR_COMPUTE_OPTIMIZER"]
      users = []
      groups = []
    }
    IAM_DENY_POLICY_ATTACH = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy_arn = module.iam_custom_policy_tna["IAM_DENY_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_admin"]
    }
    IAM_DBA_POLICY_ATTACH = {
      name = "IAM_DBA_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_tna["IAM_DBA_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_dba"]
    }
    IAM_SECUI_POLICY_ATTACH = {
      name = "IAM_DBA_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_tna["IAM_SECUI_POLICY"].policy_arn
      roles= ["TIDESQUARE_IAM_ROLE_FOR_SECUI"]
      users = ["TIDESQUARE_IAM_USER_FOR_SECUI"]
      groups = []
    }
    IAM_FEDEV2_POLICY = {
      name = "IAM_FEDEV2_POLICY"
      policy_arn = module.iam_custom_policy_tna["IAM_FEDEV2_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_developer"]
    }
    IAM_TNA_TS_PROGRAM_CUSTOM_POLICY_ATTACH = {
      name = "IAM_TNA_TS_PROGRAM_CUSTOM_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_tna["IAM_TNA_TS_PROGRAM_CUSTOM_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_program"]
    }

    #Custom Policy for program users
    IAM_COMMON_API_USER_POLICY_ATTACH = {
      name = "IAM_COMMON_API_USER_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_tna["IAM_COMMON_API_USER_POLICY"].policy_arn
      roles= []
      users = ["common-api-user"]
      groups = []
    }
    IAM_TNA_RESELL_USER_POLICY_ATTACH = {
      name = "IAM_TNA_RESELL_USER_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_tna["IAM_TNA_RESELL_USER_POLICY"].policy_arn
      roles= []
      users = ["tna-resell-user"]
      groups = []
    }
    IAM_TOACT_SEARCH_S3_POLICY_ATTACH = {
      name = "IAM_TOACT_SEARCH_S3_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_tna["IAM_TOACT_SEARCH_S3_POLICY"].policy_arn
      roles= []
      users = ["toact-search-s3"]
      groups = []
    }
    IAN_TNA_TOURVIS_ALB_ERR_LOG_API_POLICY_ATTACH = {
      name = "IAN_TNA_TOURVIS_ALB_ERR_LOG_API_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_tna["IAN_TNA_TOURVIS_ALB_ERR_LOG_API_POLICY"].policy_arn
      roles= []
      users = ["tourvis-alb-err-log-api"]
      groups = []
    }
    IAM_TNA_PIPELINE_ALB_LOG_PARSER_ATTACH = {
      name = "IAM_TNA_PIPELINE_ALB_LOG_PARSER_ATTACH"
      policy_arn = module.iam_custom_policy_tna["IAM_TNA_PIPELINE_ALB_LOG_PARSER"].policy_arn
      roles= []
      users = ["pipeline"]
      groups = []
    }
    IAM_TNA_PIPELINE_ECR_POLICY_ATTACH = {
      name = "IAM_TNA_PIPELINE_ECR_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_tna["IAM_TNA_PIPELINE_ECR_POLICY"].policy_arn
      roles= []
      users = ["pipeline"]
      groups = []
    }
  }
  IAM_CUSTOM_POLICY_ATTACH_NDC = {
    IAM_FORCE_MFA_FOR_ACCESS_KEY_ATTACH = {
      name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      policy_arn = module.iam_custom_policy_ndc["IAM_FORCE_MFA_FOR_ACCESS_KEY"].policy_arn
      roles= []
      users = ["TS1010","TS1321.Terraform","TS1484.Terraform"]
      groups = ["ts_infra","ts_admin"]
    }
    IAM_DBA_POLICY_ATTACH = {
      name = "IAM_DBA_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_ndc["IAM_DBA_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_dba"]
    }
    IAM_COMPUTE_OPTIMIZER_ATTACH = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy_arn = module.iam_custom_policy_ndc["IAM_COMPUTE_OPTIMIZER"].policy_arn
      roles= ["IAM_ROLE_FOR_COMPUTE_OPTIMIZER"]
      users = []
      groups = []
    }
    IAM_DENY_POLICY_ATTACH = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy_arn = module.iam_custom_policy_ndc["IAM_DENY_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_admin"]
    }
    IAM_SECUI_POLICY_ATTACH = {
      name = "IAM_DBA_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_ndc["IAM_SECUI_POLICY"].policy_arn
      roles= ["TIDESQUARE_IAM_ROLE_FOR_SECUI"]
      users = ["TIDESQUARE_IAM_USER_FOR_SECUI"]
      groups = []
    }
  }
  IAM_CUSTOM_POLICY_ATTACH_DW = {
    IAM_MFA_ALLOW_POLICY_ATTACH = {
      name = "IAM_MFA_ALLOW_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_dw["IAM_MFA_ALLOW_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_developer"]
    }
    IAM_FORCE_MFA_FOR_ACCESS_KEY_ATTACH = {
      name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      policy_arn = module.iam_custom_policy_dw["IAM_FORCE_MFA_FOR_ACCESS_KEY"].policy_arn
      roles= []
      users = []
      groups = ["ts_infra","ts_admin"]
    }
    IAM_COMPUTE_OPTIMIZER_ATTACH = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy_arn = module.iam_custom_policy_dw["IAM_COMPUTE_OPTIMIZER"].policy_arn
      roles= ["IAM_ROLE_FOR_COMPUTE_OPTIMIZER"]
      users = []
      groups = []
    }
    IAM_DENY_POLICY_ATTACH = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy_arn = module.iam_custom_policy_dw["IAM_DENY_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_admin"]
    }
    IAM_DBA_POLICY_ATTACH = {
      name = "IAM_DBA_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_dw["IAM_DBA_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_dba"]
    }
    IAM_SECUI_POLICY_ATTACH = {
      name = "IAM_DBA_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_dw["IAM_SECUI_POLICY"].policy_arn
      roles= ["TIDESQUARE_IAM_ROLE_FOR_SECUI"]
      users = ["TIDESQUARE_IAM_USER_FOR_SECUI"]
      groups = []
    }
  }
  IAM_CUSTOM_POLICY_ATTACH_VIET = {
    IAM_MFA_ALLOW_POLICY_ATTACH = {
      name = "IAM_MFA_ALLOW_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_viet["IAM_MFA_ALLOW_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_developer"]
    }
    IAM_VIET_EKS_POLICY = {
    name = "IAM_VIET_EKS_POLICY"
    policy_arn = module.iam_custom_policy_viet["IAM_VIET_EKS_POLICY"].policy_arn
    roles= []
    users = []
    groups = ["ts_viet_eks"]
  }
    IAM_DBA_POLICY_ATTACH = {
      name = "IAM_DBA_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_viet["IAM_DBA_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_dba"]
    }
    IAM_COMPUTE_OPTIMIZER_ATTACH = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy_arn = module.iam_custom_policy_viet["IAM_COMPUTE_OPTIMIZER"].policy_arn
      roles= ["IAM_ROLE_FOR_COMPUTE_OPTIMIZER"]
      users = []
      groups = []
    }
    IAM_DENY_POLICY_ATTACH = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy_arn = module.iam_custom_policy_viet["IAM_DENY_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_admin"]
    }
  }
  IAM_CUSTOM_POLICY_ATTACH_DMS = {
    IAM_MFA_ALLOW_POLICY_ATTACH = {
      name = "IAM_MFA_ALLOW_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_dms["IAM_MFA_ALLOW_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_developer"]
    }
    IAM_COMPUTE_OPTIMIZER_ATTACH = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy_arn = module.iam_custom_policy_dms["IAM_COMPUTE_OPTIMIZER"].policy_arn
      roles= ["IAM_ROLE_FOR_COMPUTE_OPTIMIZER"]
      users = []
      groups = []
    }
    IAM_DENY_POLICY_ATTACH = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy_arn = module.iam_custom_policy_dms["IAM_DENY_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_admin"]
    }
    IAM_DBA_POLICY_ATTACH = {
      name = "IAM_DBA_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_dms["IAM_DBA_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_dba"]
    }
  }
  IAM_CUSTOM_POLICY_ATTACH_BTMS = {
    IAM_MFA_ALLOW_POLICY_ATTACH = {
      name = "IAM_MFA_ALLOW_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_btms["IAM_MFA_ALLOW_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_developer"]
    }
    IAM_FORCE_MFA_FOR_ACCESS_KEY_ATTACH = {
      name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      policy_arn = module.iam_custom_policy_btms["IAM_FORCE_MFA_FOR_ACCESS_KEY"].policy_arn
      roles= []
      users = []
      groups = ["ts_infra","ts_admin"]
    }
    IAM_DENY_POLICY_ATTACH = {
      name = "IAM_COMPUTE_OPTIMIZER"
      policy_arn = module.iam_custom_policy_btms["IAM_DENY_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_admin"]
    }
    IAM_DBA_POLICY_ATTACH = {
      name = "IAM_DBA_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_btms["IAM_DBA_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_dba"]
    }

}
  IAM_CUSTOM_POLICY_ATTACH_KMS = {
    IAM_FORCE_MFA_FOR_ACCESS_KEY_ATTACH = {
      name = "IAM_FORCE_MFA_FOR_ACCESS_KEY"
      policy_arn = module.iam_custom_policy_kms["IAM_FORCE_MFA_FOR_ACCESS_KEY"].policy_arn
      roles= []
      users = []
      groups = ["ts_infra","ts_admin"]
    }
    IAM_DBA_POLICY_ATTACH = {
      name = "IAM_DBA_POLICY_ATTACH"
      policy_arn = module.iam_custom_policy_kms["IAM_DBA_POLICY"].policy_arn
      roles= []
      users = []
      groups = ["ts_dba"]
    }
#     IAM_DENY_POLICY_ATTACH = {
#       name = "IAM_COMPUTE_OPTIMIZER"
#       policy_arn = module.iam_custom_policy_kms["IAM_DENY_POLICY"].policy_arn
#       roles= []
#       users = []
#       groups = ["ts_admin"]
#     }
  }
}
####################################################
#IAM ROLES
####################################################
variable "iam_roles"{
  type=map(object({
    name=any
    tags=any
    assume_role_policy = any
    mgd_policies = set(string)
  }))
  default = {}
}
locals {
  IAM_ROLE_HOMEPAGE = {
    IAM_ROLE_SCHEDULER = {
      name = "IAM_ROLE_SCHEDULER"
      tags = {
        Name = "IAM_ROLE_SCHEDULER"
      }
      assume_role_policy = data.aws_iam_policy_document.ec2_scheduler_role.json
      mgd_policies       = ["arn:aws:iam::aws:policy/AmazonEC2FullAccess","arn:aws:iam::aws:policy/ReadOnlyAccess"]
    }
    IAM_ROLE_FOR_READ_ASSUME = {
      name = "IAM_ROLE_FOR_READ_ASSUME"
      tags = {
        Name = "IAM_ROLE_FOR_READ_ASSUME"
      }
      assume_role_policy = data.aws_iam_policy_document.assume_role_for_read_only.json
      mgd_policies       = ["arn:aws:iam::aws:policy/ReadOnlyAccess"]
    }
    IAM_ROLE_FOR_COMPUTE_OPTIMIZER = {
      name = "IAM_ROLE_FOR_COMPUTE_OPTIMIZER"
      tags = {
        Name = "IAM_ROLE_FOR_COMPUTE_OPTIMIZER"
      }
      assume_role_policy = data.aws_iam_policy_document.assume_role_for_lambda.json
      mgd_policies       = ["arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/ComputeOptimizerReadOnlyAccess"]
    }
  }
  IAM_ROLE_AIR = {
    IAM_ROLE_SCHEDULER = {
      name = "IAM_ROLE_SCHEDULER"
      tags = {
        Name = "IAM_ROLE_SCHEDULER"
      }
      assume_role_policy = data.aws_iam_policy_document.ec2_scheduler_role.json
      mgd_policies       = ["arn:aws:iam::aws:policy/AmazonEC2FullAccess","arn:aws:iam::aws:policy/ReadOnlyAccess"]
    }
    IAM_ROLE_FOR_READ_ASSUME = {
      name = "IAM_ROLE_FOR_READ_ASSUME"
      tags = {
        Name = "IAM_ROLE_FOR_READ_ASSUME"
      }
      assume_role_policy = data.aws_iam_policy_document.assume_role_for_read_only.json
      mgd_policies       = ["arn:aws:iam::aws:policy/ReadOnlyAccess"]
    }
    IAM_ROLE_FOR_COMPUTE_OPTIMIZER = {
      name = "IAM_ROLE_FOR_COMPUTE_OPTIMIZER"
      tags = {
        Name = "IAM_ROLE_FOR_COMPUTE_OPTIMIZER"
      }
      assume_role_policy = data.aws_iam_policy_document.assume_role_for_lambda.json
      mgd_policies       = ["arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/ComputeOptimizerReadOnlyAccess"]
    }
  }
  IAM_ROLE_HOT = {
    IAM_ROLE_SCHEDULER = {
      name = "IAM_ROLE_SCHEDULER"
      tags = {
        Name = "IAM_ROLE_SCHEDULER"
      }
      assume_role_policy = data.aws_iam_policy_document.ec2_scheduler_role.json
      mgd_policies       = ["arn:aws:iam::aws:policy/AmazonEC2FullAccess","arn:aws:iam::aws:policy/ReadOnlyAccess"]
    }
    IAM_ROLE_FOR_READ_ASSUME = {
      name = "IAM_ROLE_FOR_READ_ASSUME"
      tags = {
        Name = "IAM_ROLE_FOR_READ_ASSUME"
      }
      assume_role_policy = data.aws_iam_policy_document.assume_role_for_read_only.json
      mgd_policies       = ["arn:aws:iam::aws:policy/ReadOnlyAccess"]
    }
    IAM_ROLE_FOR_COMPUTE_OPTIMIZER = {
      name = "IAM_ROLE_FOR_COMPUTE_OPTIMIZER"
      tags = {
        Name = "IAM_ROLE_FOR_COMPUTE_OPTIMIZER"
      }
      assume_role_policy = data.aws_iam_policy_document.assume_role_for_lambda.json
      mgd_policies       = ["arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/ComputeOptimizerReadOnlyAccess"]
    }
  }
  IAM_ROLE_KONG = {
    IAM_ROLE_SCHEDULER = {
      name = "IAM_ROLE_SCHEDULER"
      tags = {
        Name = "IAM_ROLE_SCHEDULER"
      }
      assume_role_policy = data.aws_iam_policy_document.ec2_scheduler_role.json
      mgd_policies       = ["arn:aws:iam::aws:policy/AmazonEC2FullAccess","arn:aws:iam::aws:policy/ReadOnlyAccess"
      ,module.iam_custom_policy_comm["IAM_POLICY_RDS_SCHEDULER"].policy_arn]
    }
    IAM_ROLE_FOR_READ_ASSUME = {
      name = "IAM_ROLE_FOR_READ_ASSUME"
      tags = {
        Name = "IAM_ROLE_FOR_READ_ASSUME"
      }
      assume_role_policy = data.aws_iam_policy_document.assume_role_for_read_only.json
      mgd_policies       = ["arn:aws:iam::aws:policy/ReadOnlyAccess"]
    }
    IAM_ROLE_FOR_COMPUTE_OPTIMIZER = {
      name = "IAM_ROLE_FOR_COMPUTE_OPTIMIZER"
      tags = {
        Name = "IAM_ROLE_FOR_COMPUTE_OPTIMIZER"
      }
      assume_role_policy = data.aws_iam_policy_document.assume_role_for_lambda.json
      mgd_policies       = ["arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/ComputeOptimizerReadOnlyAccess"]
    }
  }
  IAM_ROLE_NDC = {
    IAM_ROLE_SCHEDULER = {
      name = "IAM_ROLE_SCHEDULER"
      tags = {
        Name = "IAM_ROLE_SCHEDULER"
      }
      assume_role_policy = data.aws_iam_policy_document.ec2_scheduler_role.json
      mgd_policies       = ["arn:aws:iam::aws:policy/AmazonEC2FullAccess","arn:aws:iam::aws:policy/ReadOnlyAccess"]
    }
    IAM_ROLE_FOR_READ_ASSUME = {
      name = "IAM_ROLE_FOR_READ_ASSUME"
      tags = {
        Name = "IAM_ROLE_FOR_READ_ASSUME"
      }
      assume_role_policy = data.aws_iam_policy_document.assume_role_for_read_only.json
      mgd_policies       = ["arn:aws:iam::aws:policy/ReadOnlyAccess"]
    }
    IAM_ROLE_FOR_COMPUTE_OPTIMIZER = {
      name = "IAM_ROLE_FOR_COMPUTE_OPTIMIZER"
      tags = {
        Name = "IAM_ROLE_FOR_COMPUTE_OPTIMIZER"
      }
      assume_role_policy = data.aws_iam_policy_document.assume_role_for_lambda.json
      mgd_policies       = ["arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/ComputeOptimizerReadOnlyAccess"]
    }
  }
  IAM_ROLE_TNA = {
    IAM_ROLE_SCHEDULER = {
      name = "IAM_ROLE_SCHEDULER"
      tags = {
        Name = "IAM_ROLE_SCHEDULER"
      }
      assume_role_policy = data.aws_iam_policy_document.ec2_scheduler_role.json
      mgd_policies       = ["arn:aws:iam::aws:policy/AmazonEC2FullAccess","arn:aws:iam::aws:policy/ReadOnlyAccess"]
    }
    IAM_ROLE_FOR_READ_ASSUME = {
      name = "IAM_ROLE_FOR_READ_ASSUME"
      tags = {
        Name = "IAM_ROLE_FOR_READ_ASSUME"
      }
      assume_role_policy = data.aws_iam_policy_document.assume_role_for_read_only.json
      mgd_policies       = ["arn:aws:iam::aws:policy/ReadOnlyAccess"]
    }
    IAM_ROLE_FOR_COMPUTE_OPTIMIZER = {
      name = "IAM_ROLE_FOR_COMPUTE_OPTIMIZER"
      tags = {
        Name = "IAM_ROLE_FOR_COMPUTE_OPTIMIZER"
      }
      assume_role_policy = data.aws_iam_policy_document.assume_role_for_lambda.json
      mgd_policies       = ["arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/ComputeOptimizerReadOnlyAccess"]
    }
  }

  IAM_ROLE_DW = {
    IAM_ROLE_SCHEDULER = {
      name = "IAM_ROLE_SCHEDULER"
      tags = {
        Name = "IAM_ROLE_SCHEDULER"
      }
      assume_role_policy = data.aws_iam_policy_document.ec2_scheduler_role.json
      mgd_policies       = ["arn:aws:iam::aws:policy/AmazonEC2FullAccess","arn:aws:iam::aws:policy/ReadOnlyAccess"]
    }
    IAM_ROLE_FOR_READ_ASSUME = {
      name = "IAM_ROLE_FOR_READ_ASSUME"
      tags = {
        Name = "IAM_ROLE_FOR_READ_ASSUME"
      }
      assume_role_policy = data.aws_iam_policy_document.assume_role_for_read_only.json
      mgd_policies       = ["arn:aws:iam::aws:policy/ReadOnlyAccess"]
    }
    IAM_ROLE_FOR_COMPUTE_OPTIMIZER = {
      name = "IAM_ROLE_FOR_COMPUTE_OPTIMIZER"
      tags = {
        Name = "IAM_ROLE_FOR_COMPUTE_OPTIMIZER"
      }
      assume_role_policy = data.aws_iam_policy_document.assume_role_for_lambda.json
      mgd_policies       = ["arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/ComputeOptimizerReadOnlyAccess"]
    }
  }
  IAM_ROLE_DMS = {
    IAM_ROLE_SCHEDULER = {
      name = "IAM_ROLE_SCHEDULER"
      tags = {
        Name = "IAM_ROLE_SCHEDULER"
      }
      assume_role_policy = data.aws_iam_policy_document.ec2_scheduler_role.json
      mgd_policies       = ["arn:aws:iam::aws:policy/AmazonEC2FullAccess","arn:aws:iam::aws:policy/ReadOnlyAccess"]
    }
    IAM_ROLE_FOR_READ_ASSUME = {
      name = "IAM_ROLE_FOR_READ_ASSUME"
      tags = {
        Name = "IAM_ROLE_FOR_READ_ASSUME"
      }
      assume_role_policy = data.aws_iam_policy_document.assume_role_for_read_only.json
      mgd_policies       = ["arn:aws:iam::aws:policy/ReadOnlyAccess"]
    }
    IAM_ROLE_FOR_COMPUTE_OPTIMIZER = {
      name = "IAM_ROLE_FOR_COMPUTE_OPTIMIZER"
      tags = {
        Name = "IAM_ROLE_FOR_COMPUTE_OPTIMIZER"
      }
      assume_role_policy = data.aws_iam_policy_document.assume_role_for_lambda.json
      mgd_policies       = ["arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/ComputeOptimizerReadOnlyAccess"]
    }
  }
  IAM_ROLE_VIET = {
    IAM_ROLE_SCHEDULER = {
      name = "IAM_ROLE_SCHEDULER"
      tags = {
        Name = "IAM_ROLE_SCHEDULER"
      }
      assume_role_policy = data.aws_iam_policy_document.ec2_scheduler_role.json
      mgd_policies       = ["arn:aws:iam::aws:policy/AmazonEC2FullAccess","arn:aws:iam::aws:policy/ReadOnlyAccess"]
    }
    IAM_ROLE_FOR_READ_ASSUME = {
      name = "IAM_ROLE_FOR_READ_ASSUME"
      tags = {
        Name = "IAM_ROLE_FOR_READ_ASSUME"
      }
      assume_role_policy = data.aws_iam_policy_document.assume_role_for_read_only.json
      mgd_policies       = ["arn:aws:iam::aws:policy/ReadOnlyAccess"]
    }
    IAM_ROLE_FOR_COMPUTE_OPTIMIZER = {
      name = "IAM_ROLE_FOR_COMPUTE_OPTIMIZER"
      tags = {
        Name = "IAM_ROLE_FOR_COMPUTE_OPTIMIZER"
      }
      assume_role_policy = data.aws_iam_policy_document.assume_role_for_lambda.json
      mgd_policies       = ["arn:aws:iam::aws:policy/AmazonS3FullAccess","arn:aws:iam::aws:policy/ComputeOptimizerReadOnlyAccess"]
    }
  }
}
