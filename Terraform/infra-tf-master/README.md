
# Terraform IAM 관리


- 사용 IDE : InteliJ
- 소스 관리 : Bitbucket
- CodeBuild 


## IAM Group 정책

- ts_infra
  - Admin 권한
  - MFA 필수
- ts_admin 
  - Admin 권한
  - MFA 필수
- ts_developer
  - 전체 읽기 권한
  - S3 전체 권한
  - Athena 전체 권한
- ts_s3
  - S3 전체 권한
- ts_codedeploy
  - CodeDeploy 전체 권한
  - EC2 전체 권한
  - ECR 전체 권한
- ts_readonly
  - 전체 읽기 권한



## 연동 계정
```terraform
- homepage 589566835476 (https://589566835476.signin.aws.amazon.com/console)
- air 751732153713 (https://751732153713.signin.aws.amazon.com/console)
- hot 298385711329 (https://298385711329.signin.aws.amazon.com/console)
- comm 300846112004 (https://300846112004.signin.aws.amazon.com/console)
- kms 145023110150  (https://145023110150 .signin.aws.amazon.com/console)
- dw 792931648816 (https://792931648816.signin.aws.amazon.com/console)
- tna 339927058960 (https://339927058960.signin.aws.amazon.com/console)
- viet 836881754257 (https://836881754257.signin.aws.amazon.com/console)
- dms 097345172411 (https://097345172411.signin.aws.amazon.com/console)
- btms 503561428620 (https://503561428620.signin.aws.amazon.com/console)
```



## Terraform 모듈 구성

- iam_groups : groups 생성 후 관리형 정책 추가
  ```terraform
   type = map(object({
    group_name = string
    group_path = string
    mgd_policy = set(string)
  }))
  ```
- iam_policy : 사용자 지정 정책 생성 후 원하는 groups 또는 user 에 추가
  ```terraform
    type = map(object({
    policy_name = string
    policy_path = string
    policy_desc = string
    policy = any
    policy_attach_roles = set(string)
    policy_attach_users = set(string)
    policy_attach_groups = set(string)
  }))
  ```
- iam_users : user 생성 후 group 매핑
  ```terraform
    type = map(object({
    user_name  = string
    group_name = set(string)
  }))
    
  ```