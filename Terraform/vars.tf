variable "vpc_cidr" {
  description = "VPC CIDR BLOCK : x.x.x.x/x"
  type = map(object({
    vpc_cidr=string
    tags = any
  }))
  default = {}
}
variable "nat_gw" {
  type = map(object({
    public_subnet=string
    tags=any
  }))
  default = {}
}
variable "subnets" {
  type=map(object({
    vpc_id=any
    subnet_cidr=any
    subnet_az=any
    is_public=bool
    tags=any
  }))
  default = {}
}
variable "route_tables" {
  type = map(object({
    vpc_id = string
    route = any
    tags = any
    subnets = any
  }))
  default = {}
}

#VPC CIDR
locals {
  DEV_VPC={
    dev_vpc_1 = {
      vpc_cidr = ".168.0.0/16"
      tags= {
        Name  = "${var.dev_name_tag}-vpc",
        Owner = "ksj"
      }
    }
  }
}
#NAT GW
locals {
  DEV_NAT_GW ={
    dev_nat_gw_1 = {
      public_subnet = module.subnets["pub1"].subnet_id
      tags = {
        Name = "dev_nat_gw"
        Owner = "ksj"
      }
    }
  }
}
#SUBNETS
locals {
  DEV_SUBNETS= {
    pub1 = {
      vpc_id      = module.vpc["dev_vpc_1"].vpc_id
      subnet_cidr = "192.168.0.0/24"
      subnet_az   = data.aws_availability_zones.available.names[0]
      is_public   = true
      tags= {
        Name  = "${var.dev_name_tag}-public-1",
        Owner = "ksj"
      }
    }
    pub2 = {
      vpc_id      = module.vpc["dev_vpc_1"].vpc_id
      subnet_cidr = "192.168.1.0/24"
      subnet_az   = data.aws_availability_zones.available.names[2]
      is_public   = true
      tags= {
        Name  = "${var.dev_name_tag}-public-2",
        Owner = "ksj"
      }
    }
    pri1 = {
      vpc_id      = module.vpc["dev_vpc_1"].vpc_id
      subnet_cidr = "192.168.2.0/24"
      subnet_az   = data.aws_availability_zones.available.names[0]
      is_public   = false
      tags= {
        Name  = "${var.dev_name_tag}-private-1",
        Owner = "ksj"
      }
    }
    pri2 = {
      vpc_id      = module.vpc["dev_vpc_1"].vpc_id
      subnet_cidr = "192.168.3.0/24"
      subnet_az   = data.aws_availability_zones.available.names[2]
      is_public   = false
      tags= {
        Name  = "${var.dev_name_tag}-private-2",
        Owner = "ksj"
      }
    }
  }
}
#ROUTE TABLES
locals {
  DEV_ROUTE_TABLE = {
    dev_public_route_table = {
      vpc_id = module.vpc["dev_vpc_1"].vpc_id
      tags = {
        Name = "public-route-table"
        Owner = "ksj"
      }
      route =[
        {
          cidr_block = "0.0.0.0/0"
          gateway_id = module.vpc["dev_vpc_1"].igw_id
        }
      ]
      subnets = [module.subnets["pub1"].subnet_id,module.subnets["pub2"].subnet_id]
    }

    dev_private_route_table = {
      vpc_id = module.vpc["dev_vpc_1"].vpc_id
      tags = {
        Name = "private-route-table"
        Owner = "ksj"
      }
      route =[
        {
          cidr_block = "0.0.0.0/0"
          gateway_id = module.nat_gw["dev_nat_gw_1"].nat_gw
        }
      ]
      subnets = [module.subnets["pri1"].subnet_id,module.subnets["pri2"].subnet_id]
    }
  }
}