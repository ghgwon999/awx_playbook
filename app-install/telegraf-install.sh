#!/bin/bash

DATE=$(date '+%Y%m%d')

cp /etc/hosts /etc/hosts-$DATE

# 추가할 내용
comment="# telegraf influxDB"
entry="172.16.201.150 influxdb"

# 루트 권한 확인
if [[ $EUID -ne 0 ]]; then
    echo "이 스크립트는 루트 권한으로 실행해야 합니다."
    exit 1
fi

# sed 명령어로 3/4번째줄에 새 라인 삽입
sed -i "3i $comment" /etc/hosts
sed -i "4i $entry" /etc/hosts

echo "추가 완료: $comment"
echo "추가 완료: $entry"

# telegraf install 
wget --no-check-certificate https://dl.influxdata.com/telegraf/releases/telegraf-1.21.2-1.x86_64.rpm

yum install -y telegraf-1.21.2-1.x86_64.rpm

mv /etc/telegraf/telegraf.conf /etc/telegraf/telegraf.conf-$DATE

cp /root/app-install/telegraf.conf /etc/telegraf/telegraf.conf

cp /root/app-install/telegraf /etc/default/telegraf

# 수정할 파일 경로
FILE_PATH="/etc/default/telegraf"

# 사용자 입력 받기
read -p "Enter the Service value for Service(e.g TOURVIS): " service_value
read -p "Enter the IP value for Service(e.g 172.16.201.x): " ip_value
read -p "Enter the Product value for Service(e.g PROD,STG..): " product_value
read -p "Enter the Project value for Service(e.g Stella-API): " project_value

# 입력 값이 비어있지 않은지 확인
if [[ -n "$service_value" && -n "$ip_value" && -n "$product_value" && -n "$project_value" ]]; then
    echo "Updating the configuration file with the provided values..."

    # 파일 수정 (변경 내용 저장)
    sed -i "s|Service=.*|Service=$service_value|" $FILE_PATH
    sed -i "s|IP=.*|IP=$ip_value|" $FILE_PATH
    sed -i "s|Product=.*|Product=$product_value|" $FILE_PATH
    sed -i "s|Project=.*|Project=$project_value|" $FILE_PATH

    echo "Configuration updated successfully in $FILE_PATH"
else
    echo "One or more input values are missing. Please try again."
    exit 1
fi

systemctl restart telegraf

rm -rf /root/app-install/telegraf-1.21.2-1.x86_64.rpm
