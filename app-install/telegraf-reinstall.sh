#!/bin/bash

# 수정할 파일 경로
FILE_PATH="/etc/default/telegraf"

# 사용자 입력 받기
read -p "Enter the Service value for Service(e.g TOURVIS): " service_value
read -p "Enter the IP value for Service(e.g 172.16.201.x): " ip_value
read -p "Enter the Product value for Service(e.g PROD,STG..): " product_value
read -p "Enter the Project value for Service(e.g Stella-API): " project_value

# 입력 값이 비어있지 않은지 확인
if [[ -n "$service_value" && -n "$ip_value" && -n "$product_value" && -n "$project_value" ]]; then
    echo "Updating the configuration file with the provided values..."

    # 파일 수정 (변경 내용 저장)
    sed -i "s|Service=.*|Service=$service_value|" $FILE_PATH
    sed -i "s|IP=.*|IP=$ip_value|" $FILE_PATH
    sed -i "s|Product=.*|Product=$product_value|" $FILE_PATH
    sed -i "s|Project=.*|Project=$project_value|" $FILE_PATH

    echo "Configuration updated successfully in $FILE_PATH"
else
    echo "One or more input values are missing. Please try again."
    exit 1
fi

systemctl restart telegraf

rm -rf /root/app-install/telegraf-1.21.2-1.x86_64.rpm
